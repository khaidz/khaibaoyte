define('jquery', [], function () {
    return jQuery;
});

require.config({
    baseUrl: static_url + '/js/lib',
    waitSeconds: 200,
    paths: {
        app: '../app',
        tpl: '../tpl',
        vendor: '../../vendor',
        schema: '../schemas',
        js: '../',
    },
    shim: {
        'gonrin': {
            deps: ['underscore', 'jquery', 'backbone'],
            exports: 'Gonrin'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});


require(['jquery', 'gonrin', 'app/router',
    'app/bases/Nav/NavbarView',
    'app/notifications/NotifyView',
    'text!app/bases/tpl/mainlayout.html',
    'i18n!app/nls/app',
    'vendor/store'
],
    function ($, Gonrin, Router, Nav, NotifyView, layout, lang, storejs, UserDonViDialogView) {
        $.ajaxSetup({
            headers: {
                'content-type': 'application/json'
            }
        });

        var app = new Gonrin.Application({
            serviceURL: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
            staticURL: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/static/',
            router: new Router(),
            lang: lang,
            //layout: layout,
            initialize: function () {
                this.getRouter().registerAppRoute();
                this.nav = new Nav();
                this.nav.render();

                this.getCurrentUser();
                $(document).on('show.bs.modal', '.modal', function (event) {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function () {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                });
                $(document).on('hidden.bs.modal', '.modal', function () {
                    $('.modal:visible').length && $(document.body).addClass('modal-open');
                });
                this.renderHomePage(gonrinApp().currentUser)
            },
            parseDate: function (val) {
                var result = null;
                if (val === null || val === undefined || val === "" || val === 0) {
                    result = null;
                } else {
                    var date = null;
                    if (val instanceof Date) {
                        result = moment.utc([val.getFullYear(), val.getMonth(), val.getDate()]);
                    } else {
                        if ($.isNumeric(val)) {
                            date = new Date(val * 1000);
                        } else if (typeof val === "string") {
                            date = new Date(val);
                        } else {
                            result = moment.utc();
                        }
                        if (date != null && date instanceof Date) {
                            result = moment.utc([date.getFullYear(), date.getMonth(), date.getDate()]);
                        }
                    }
                    return result;
                }
            },
            getParameterUrl: function (parameter, url) {
                if (!url) url = window.location.href;
                var reg = new RegExp('[?&]' + parameter + '=([^&#]*)', 'i');
                var string = reg.exec(url);
                return string ? string[1] : undefined;
            },
            showloading: function (content = null) {
                $('body .loader').addClass('active');
                if (content) {
                    $('body .loader').find(".loader-content").html(content);
                }
            },
            hideloading: function () {
                //			$("#loading").addClass("d-none");
                $('body .loader').removeClass('active');
                $('body .loader').find(".loader-content").empty();
            },
            getCurrentUser: function () {
                var self = this;
                var token = storejs.get('X-USER-TOKEN');
                var uid = storejs.get('UID-TOKHAI-YTE');
                if(!uid){
                    uid = this.default_uuid();
                    storejs.set('UID-TOKHAI-YTE', uid);
                }
                $.ajaxSetup({
                    headers: {
                        'X-USER-TOKEN': token
                    }
                });
                self.showloading();
                $.ajax({
                    url: self.serviceURL + '/api/v1/current_user',
                    dataType: "json",
                    success: function (data) {
                        self.hideloading();
                        if (data.active === 0 || data.active === '0') {
                            gonrinApp().getRouter().navigate('confirm-active?uid=' + data.id);
                            return false;
                        } else if (data.active === 1 || data.active === '1') {
                            $.ajaxSetup({
                                headers: {
                                    'X-USER-TOKEN': data.token
                                }
                            });
                            storejs.set('X-USER-TOKEN', data.token);
                            gonrinApp().postLogin(data);
                        } else {
                            gonrinApp().notify("Tài khoản của bạn đã bị khóa");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        self.hideloading();
                        
                        var currentRoute = self.getRouter().currentRoute().fragment;
                        if (currentRoute.indexOf('login') >= 0) {
                            gonrinApp().getRouter().refresh();
                            
                        }else if(currentRoute.indexOf('ketqua')>=0 || currentRoute.indexOf('lichsu')>=0 || currentRoute.indexOf('tokhai_yte')>=0){
                            
                        }
                         else {
                            gonrinApp().getRouter().navigate('tokhai_yte/model');
                        }
                    }
                });
            },
            hasRole: function (role) {
                return (gonrinApp().currentUser != null && gonrinApp().currentUser.roles != null) && gonrinApp().currentUser.roles.indexOf(role) >= 0;
            },
           
            postLogin: function (data) {
                var self = this;

                // self.showloading();
                self.currentUser = new Gonrin.User(data);
                var url_path = window.location.href;
                this.renderHomePage(data);
          
            },
            autoHeightArea: function (element, class_auto) {
                var self = this;
                element.$el.find(class_auto).each(function (indexcao, itemcao) {
                    var chieucao = itemcao.scrollHeight;
                    $(itemcao).keyup(function () {
                        if (itemcao.scrollHeight > chieucao) {
                            chieucao = itemcao.scrollHeight;
                            itemcao.style.height = chieucao + 'px';
                        } else if (itemcao.scrollHeight < chieucao) {
                            if (itemcao.scrollHeight < 60) {
                                itemcao.style.height = 60 + 'px';
                            }
                            else {
                                itemcao.style.height = itemcao.scrollHeight + 'px';
                            }
                        }
                    });
                });
            },
            renderHomePage(data, sw_menu = null) {
                var self = this;
                $('#contaner-template').html(layout);
                $('body').attr('data-layout-mode', 'horizontal').attr('data-sidebar-size', 'default');
                this.$header = $('body').find(".navbar-custom");
                
                if(!!self.currentUser){
                    if(!!self.currentUser.email){
                        this.$header.find('.user-name').text(self.currentUser.email);
                    }else if(!!self.currentUser.phone){
                        this.$header.find('.user-name').text(self.currentUser.phone);
                    }
                }
                this.$content = $('body').find(".content-page");
                this.$navbar = $('body').find("#topnav-menu-content");
                self.hideloading();

                var currentRoute = self.getRouter().currentRoute().fragment;
                if (sw_menu == true) {
                    gonrinApp().getRouter().refresh();
                } else if (this.hasRole('user') || this.hasRole('admin')) {
                    self.$header.find('.menu-admin').removeClass('d-none');
                    self.$header.find('.menu-none-login').addClass('d-none');
                    if (currentRoute.indexOf('login') >= 0 || currentRoute === "" || currentRoute === null) {
                        self.router.navigate('tokhai_yte/collection');
                    }
                }

                if(this.hasRole('admin')){
                    self.$header.find('.quanly_taikhoan').removeClass('d-none');
                }
                if(this.hasRole('user')){
                    self.$header.find('.thongtin_taikhoan').removeClass('d-none');
                    self.$header.find('.thongtin_taikhoan').unbind('click').bind('click', function(){
                        self.router.navigate('quan_ly_nguoi_dung/model?id='+gonrinApp().currentUser.id);
                    })
                    
                }
            },
            getInfoUser: function () {
                var current_user = gonrinApp().currentUser;
                var url = gonrinApp().serviceURL + '/api/v1/profile/' + current_user.id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    headers: {
                        'content-type': 'application/json'
                    },
                    dataType: 'json',
                    success: function (data) {
                        var dialogUserDonViView = new UserDonViDialogView({ "viewData": { "donvi": current_user.donvi, "data": data } });
                        $(".content-page").empty();
                        dialogUserDonViView.render();
                        $(".content-page").html(dialogUserDonViView.el);
                    },
                    error: function (xhr, status, error) {
                        try {
                            if (($.parseJSON(xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                self.getApp().getRouter().navigate("login");
                            } else {
                                self.getApp().notify({ message: $.parseJSON(xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                            }
                        } catch (err) {
                            self.getApp().notify({ message: "Có lỗi xảy ra, vui lòng thử lại sau" }, { type: "danger", delay: 1000 });
                        }
                    }
                });
            },

            openPhotoSwipe: function (index, galleryElement, class_file, disableAnimation, fromURL) {
                // var self = this;
                var pswpElement = document.querySelectorAll('.pswp')[0];
                var items = gonrinApp().parseThumbnailElements(galleryElement, class_file);
                var options = { index: index };
                // Pass data to PhotoSwipe and initialize it
                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            },
            parseThumbnailElements: function (el, class_file = "list-image") {
                var thumbElements = el.find("." + class_file);
                var items = [];
                for (var i = 0; i < thumbElements.length; i++) {
                    // create slide object
                    var item = {
                        src: thumbElements[i].getAttribute('src'),
                        w: parseInt(thumbElements[i].naturalWidth, 10),
                        h: parseInt(thumbElements[i].naturalHeight, 10)
                    };
                    item.el = thumbElements[i]; // save link to element for getThumbBoundsFn
                    items.push(item);
                }
                return items;
            },
            /**
             * Format datetime
             */
            datetimeFormat: function (datetime, formatString, align = null) {
                var format = (formatString != null) ? formatString : "DD-MM-YYYY HH:mm:ss";
                //			console.log(moment(datetime, ["MM-DD-YYYY", "YYYY-MM-DD"]).format(format),"======");
                if (align == null) {
                    return moment(datetime, ["MM-DD-YYYY", "YYYY-MM-DD"]).isValid() ? moment(datetime, ["MM-DD-YYYY", "YYYY-MM-DD"]).format(format) : "";
                }
                return moment(datetime).isValid() ? `<div style="text-align: ${align}">${moment(datetime).format(format)}</div>` : "";

            },
            timestampFormat: function (utcTime, format = "YYYY-MM-DD HH:mm:ss") {
                return moment(utcTime).local().format(format);
            },
            set_text_header: function (text) {
                var self = this;
                // $('.title_header_layout_web').text(text);
            },
            change_avatar: function (url_image) {
                var self = this;
                if (self.hasRole("nguoidan")) {
                    if (url_image == "default") {
                        $(".user-avatar").attr({ "src": "static/images/default_image.png" });
                    } else {
                        var url = self.check_image(url_image);
                        $(".user-avatar").attr({ "src": url });
                    }
                }
            },
            scanQRCode: function () {
                gonrinApp().notify("Chức năng quét mã QRCode chỉ hỗ trợ trên phiên bản cài đặt ứng dụng mobile");
            },
            initNotify: function () {
                var self = this;
                // Your web app's Firebase configuration

                const messaging = firebase.messaging();
                messaging.usePublicVapidKey("BBIWZ46a-CxH1ExEEg-Tm-OPsmYbXnJPVwQkeo4s6baqryBhyhFvHmdZYk1N7IMRTVMeRnFbGvzNahztRSo82-k");
                messaging.getToken().then((currentToken) => {
                    if (currentToken) {
                        gonrinApp().set_token_firebase_to_server(currentToken);
                    } else {
                        // Show permission request.
                        console.log('No Instance ID token available. Request permission to generate one.');
                        // Show permission UI.

                        gonrinApp().check_permission_notify();


                    }
                }).catch((err) => {
                    console.log('An error occurred while retrieving token. ', err);
                });
                messaging.onMessage(function (notification) {
                    console.log('Message received. ', notification);
                    // [START_EXCLUDE]
                    // Update the UI to include the received message.
                    console.log(notification);
                    var notifyView = new NotifyView({ viewData: notification });
                    notifyView.dialog();
                    setTimeout(() => {
                        notifyView.close();
                        // notifyView.$el.find("#myModalNotify").modal("hide");
                    }, 5000);
                    // [END_EXCLUDE]
                });
            },
            check_permission_notify: function () {
                if (!('Notification' in window)) {
                    console.log("This browser does not support notifications.");
                } else {
                    if (Notification.permission === 'denied' || Notification.permission === 'default') {
                        try {
                            Notification.requestPermission(function (permission) {
                                if (permission === 'granted') {
                                    console.log('Notification permission granted.');
                                } else {
                                    console.log('Unable to get permission to notify.');
                                }
                            });
                        } catch (error) {
                            Notification.requestPermission().then((permission) => {
                                if (permission === 'granted') {
                                    console.log('Notification permission granted.');
                                } else {
                                    console.log('Unable to get permission to notify.');
                                }
                            });
                        }
                    }
                }
            },
            set_token_firebase_to_server: function (currentToken) {
                if (currentToken) {
                    // var params = JSON.stringify({
                    //     "type_notify": "web",
                    //     "data": currentToken
                    // });
                    $.ajax({
                        url: gonrinApp().serviceURL + '/api/v1/set_notify_token',
                        dataType: "json",
                        type: "POST",
                        data: JSON.stringify({ "type_notify": "web", "data": currentToken }),
                        headers: {
                            'content-type': 'application/json'
                        },
                        success: function (data) {
                            console.log("set token firebase success!!!");
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log("set notify firebase failed");
                        }
                    });


                } else {
                    console.log('No Instance ID token available. Request permission to generate one.');
                }
            },
            validateEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            },
            validatePhone: function (inputPhone) {
                if (inputPhone == null || inputPhone == undefined) {
                    return false;
                }
                var phoneno = /(09|08|07|05|03|02)+[0-9]{8}/g;
                const result = inputPhone.match(phoneno);
                if (result && result == inputPhone) {
                    return true;
                } else {
                    return false;
                }
            },
            checkNotificationPromise: function () {
                try {
                    Notification.requestPermission().then();
                } catch (e) {
                    return false;
                }
                return true;
            },
            delete_token_firebase: function () {
                var currentToken = storejs.get('TOKEN-FIREBASE');
                if (!!currentToken) {
                    try {
                        const messaging = firebase.messaging();
                        messaging.getToken().then((currentToken) => {
                            messaging.deleteToken(currentToken).then(() => {
                                console.log('Token deleted.');
                            }).catch((err) => {
                                console.log('Unable to delete token. ', err);
                            });
                        }).catch((err) => {
                            console.log('Error retrieving Instance ID token. ', err);
                        });
                    } catch (error) {

                    }

                }

            },
            render_notify: function (data) {
                var self = this;
                var html_menu = $("#menu_notify").html("");

                if (data.length === 0) {
                    html_menu.append(`<a class="dropdown-item" href="javascript:;">
	                      <div class="notification__icon-wrapper">
	                        <div class="notification__icon">
	                          <i class="material-icons"></i>
	                        </div>
	                      </div>
	                      <div class="notification__content">
	                        <span class="notification__category">Không có thông báo mới</span>
	                      </div>
	                    </a>`);
                } else {
                    for (var i = 0; i < data.length; i++) {
                        if (i <= 10) {
                            var url = data[i].url;
                            if (url !== null && url !== "" && url.split('#').length > 0) {
                                url = url.split('#')[1];
                            }
                            var class_unread = "";
                            if (data.read_at === null || data.read_at <= 0) {
                                class_unread = "unread";
                            }
                            html_menu.append(`<a class="dropdown-item ` + class_unread + `" href="javascript:;" onClick="gonrinApp().getRouter().navigate(\'` + url + `\',{trigger: true});">
			                      <div class="notification__icon-wrapper">
			                        <div class="notification__icon">
			                          <i class="material-icons"></i>
			                        </div>
			                      </div>
			                      <div class="notification__content">
			                        <span class="notification__category">` + (data[i].title || "") + `</span>
			                        <p>` + (data[i].content || "") + `</p>
			                      </div>
			                    </a>`);
                        }
                    }

                }
                $("#menu_notify").attr({ "style": "max-height:400px" });
            },
            get_list_notify: function () {
                var self = this;
                //data: {"q": JSON.stringify({"filters": filters, "order_by":[{"field": "thoigian", "direction": "desc"}], "limit":1})},
                $.ajax({
                    url: self.serviceURL + '/api/v1/notify/read',
                    dataType: "json",
                    type: "POST",
                    //       		    data: {"q": JSON.stringify({"order_by":[{"field": "updated_at", "direction": "desc"}], "limit":100})},
                    headers: {
                        'content-type': 'application/json'
                    },
                    success: function (data) {
                        $(".notify .badge").addClass("d-none").html("");
                        self.render_notify(data.objects);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("get_list_notify failed", XMLHttpRequest);
                    }
                });
            },
            check_notify: function () {
                var self = this;
                $.ajax({
                    url: self.serviceURL + '/api/v1/notify/check',
                    dataType: "json",
                    type: "GET",
                    headers: {
                        'content-type': 'application/json'
                    },
                    success: function (data) {
                        // console.log("abc====", $(".menu_thongbao .badge"));
                        // $("#menu_thongbao .badge").removeClass("d-none").html(6);
                        if (data !== null && data.data > 0) {
                            $("#menu_thongbao .badge").removeClass("d-none").html(data.data);
                        } else {
                            $("#menu_thongbao .badge").addClass("d-none");
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) { }
                });
            },
            responsive_table: function () {
                // inspired by http://jsfiddle.net/arunpjohny/564Lxosz/1/

                if ($(window).width() < 768) {
                    $('.table-mobile').find("th").each(function (i) {
                        var el = $('.table td:nth-child(' + (i + 1) + ')');
                        if ($(this).text() === " " || $(this).text() === "") {
                            el.prepend('<span class="table-thead"></span> ');
                        } else {
                            el.prepend('<span class="table-thead">' + $(this).text() + ':</span> ');
                        }

                        $('.table-thead').hide();
                    });
                    $('.table-mobile').each(function () {
                        var thCount = $(this).find("th").length;
                        var rowGrow = 100 / thCount + '%';
                        $(this).find("th, td").css('flex-basis', rowGrow);
                        $(this).find("tr").addClass("shadow-sm p-2 mb-1 bg-white rounded");
                    });

                    function flexTable() {
                        $(".table-mobile").each(function (i) {
                            $(this).find('.table-thead').show();
                            $(this).find('thead').hide();
                        });
                    }
                    flexTable();
                }
            },
            check_image: function (image) {
                if (!image) {
                    return "";
                } else if (typeof image === "string") {
                    var url_image = "";
                    if (image.includes("static.somevabe.com")) {
                        url_image = image;
                    }
                    else {
                        for (var i = 0; i < 3; i++) {
                            image = image.replace("\"", "");
                        }
                        image = image.replace("\"", "");
                        var url_image = static_url + image;
                        return url_image;
                    }
                    return url_image;
                } else {
                    var url_image = image.link;
                    if (!url_image) {
                        return "";
                    }
                    if (url_image.startsWith("https://somevabe.com/")) {
                        url_image = url_image;
                    } else {
                        url_image = static_url + url_image;
                    }
                    return url_image;
                }
            },
            toInt: function (x) {
                return parseInt(x) ? parseInt(x) : 0;
            },
            convert_khongdau: function (strdata) {
                var kituA = ["á", "à", "ạ", "ã", "ả", "â", "ấ", "ầ", "ậ", "ẫ", "ă", "ằ", "ắ", "ẳ"],
                    kituE = ["é", "è", "ẹ", "ẻ", "ẽ", "ê", "ế", "ề", "ệ", "ễ", "ể", "ệ"],
                    kituI = ["í", "ì", "ị", "ỉ", "ĩ"],
                    kituO = ["ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ"],
                    kituU = ["ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ"],
                    kituY = ["ỳ", "ý", "ỵ", "ỷ", "ỹ"];

                var str2 = strdata.toLowerCase();
                for (var i = 0; i < kituA.length; i++) {
                    str2 = str2.replace(kituA[i], "a");
                }
                for (var i = 0; i < kituE.length; i++) {
                    str2 = str2.replace(kituE[i], "e");
                }
                for (var i = 0; i < kituI.length; i++) {
                    str2 = str2.replace(kituI[i], "i");
                }
                for (var i = 0; i < kituO.length; i++) {
                    str2 = str2.replace(kituO[i], "o");
                }
                for (var i = 0; i < kituU.length; i++) {
                    str2 = str2.replace(kituU[i], "u");
                }
                for (var i = 0; i < kituY.length; i++) {
                    str2 = str2.replace(kituY[i], "y");
                }
                str2 = str2.replace("đ", "d");
                // if(upper === true){
                // 	return str2.toUpperCase();
                // }
                return str2;
            },
            convert_string_to_number: function (string) {
                var number = 0;
                if (Number.isNaN(Number(string))) {
                    number = 0
                } else {
                    number = Number(string);
                }
                return number;
            },
            convert_diachi: function (diachi = "", tinhthanh = null, quanhuyen = null, xaphuong = null) {
                var self = this;
                var address = (!!diachi) ? diachi : "";
                if (!!xaphuong) {
                    if (address !== "") {
                        address = address + ", " + xaphuong.ten;
                    } else {
                        address = address + xaphuong.ten;
                    }

                }
                if (!!quanhuyen) {
                    if (address !== "") {
                        address = address + ", ";
                    }
                    address = address + quanhuyen.ten;
                }
                if (!!tinhthanh) {
                    if (address !== "") {
                        address = address + ", ";
                    }
                    address = address + tinhthanh.ten;
                }
                return address;
            },
            convert_number_to_vietnamese: function (number) {
                var string = String(to_vietnamese(number)).trim();
                var text = this.capitalizeFirstLetter(string);
                return text;
            },
            capitalizeFirstLetter: function (string) {
                console.log("string.charAt(0).toUpperCase()", string.charAt(0).toUpperCase())
                return string.charAt().toUpperCase() + string.slice(1);
            },
            format_timestamp_toDDMMYYY: function (timestamp) {
                if (timestamp == null || timestamp == undefined || timestamp == '') {
                    return '';
                }
                var text = moment.unix(timestamp).local().format("DD/MM/YYYY");
                return text;
            },
            default_uuid: function () {
                var dt = new Date().getTime();
                var uuid = 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = (dt + Math.random() * 16) % 16 | 0;
                    dt = Math.floor(dt / 16);
                    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
                return uuid;
            },
            utf8ToHex: function (str) {
                return Array.from(str).map(c =>
                    c.charCodeAt(0) < 128 ? c.charCodeAt(0).toString(16) :
                        encodeURIComponent(c).replace(/\%/g, '').toLowerCase()
                ).join('');
            },
            hexToUtf8: function (hex) {
                return decodeURIComponent('%' + hex.match(/.{1,2}/g).join('%'));
            },
            hex_to_ascii: function (str1) {
                var hex = str1.trim();
                hex = hex.replaceAll("â", 'aa');
                hex = hex.replaceAll("ă", 'aw');
                hex = hex.replaceAll("ô", 'oo');
                hex = hex.replaceAll("ơ", 'ow');
                hex = hex.replaceAll("ê", 'ee');
                hex = hex.replaceAll("ư", 'uw');
                // â-aa, ô-oo, ư-uw , ơ-ow, ă-aw, ê -ee,
                return decodeURIComponent(hex.replace(/\s+/g, '').replace(/[0-9a-f]{2}/g, '%$&'));
            },

        });
        app.isMobile = false;
        app.registerScrollToolbar = function (view) {
            if (!!app.isMobile) {
                Backbone.off("window:scroll").on("window:scroll", function (evt) {
                    var toolwrap = view.$el.find(".toolbar-wraper");
                    if (evt.direction === "up") {
                        if ($(window).scrollTop() > 30) {
                            toolwrap.addClass("autofix fix");
                        } else {
                            toolwrap.removeClass("autofix fix");
                        }
                    } else if (evt.direction === "down") {
                        toolwrap.removeClass("autofix fix");
                    }
                });
            }

        };

    });
