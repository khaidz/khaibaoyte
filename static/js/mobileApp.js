define('jquery', [], function() {
    return jQuery;
});

require.config({
    baseUrl: 'static/js/lib',
    paths: {
        app: '../app',
        tpl: '../tpl',
        vendor: '../../vendor',
        schema: '../schema',
    },
    /*map: {
        '*': {
            'app/models/nhanvien': 'app/models/memory/nhanvien'
        }
    },*/
    shim: {
        'gonrin': {
            deps: ['underscore', 'jquery', 'backbone'],
            exports: 'Gonrin'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});



var cordovaApp = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        //        document.addEventListener('touchmove', this.preventDefault, {passive: false});
        //        document.addEventListener('touchforcechange', this.preventDefault, {passive: false});
    },
    onDeviceReady: function() {


        require(['jquery', 'gonrin', 'app/router',
                'app/bases/Nav/NavbarView',
                'app/splash/View',
                'app/notifications/NotifyView',
                'text!tpl/base/mainlayout.html',
                'i18n!app/nls/app',
                'vendor/store'
            ],
            function($, Gonrin, Router, Nav, splashView, NotifyView, layout, lang, storejs) {
                $.ajaxSetup({
                    headers: {
                        'content-type': 'application/json'
                    }
                });
                var mobileapp = new Gonrin.Application({
                    serviceURL: 'https://somevabe.com/chuyengiatuvan',
                    // serviceURL: location.protocol+'//'+location.hostname+(location.port ? ':'+location.port : ''),
                    router: new Router(),
                    lang: lang,
                    version: 1.0,
                    platform:'IOS',
                    // version: 1.0,
//                     platform: 'Android',
                    //layout: layout,
                    initialize: function() {
                        //                   FastClick.attach(document.body);


                        this.getRouter().registerAppRoute();

                        this.nav = new Nav();
                        this.nav.render();
                        this.getCurrentUser();
                    },
                    parseDate: function(val) {
                        var result = null;
                        if (val === null || val === undefined || val === "" || val === 0) {
                            //        				return moment.utc();
                            result = null;
                        } else {
                            var date = null;
                            if (val instanceof Date) {
                                result = moment.utc([date.getFullYear(), date.getMonth(), date.getDate()]);
                            } else {
                                if ($.isNumeric(val) && parseInt(val) > 0) {
                                    date = new Date(val * 1000);
                                } else if (typeof val === "string") {
                                    date = new Date(val);
                                } else {
                                    result = moment.utc();
                                }
                                if (date != null && date instanceof Date) {
                                    result = moment.utc([date.getFullYear(), date.getMonth(), date.getDate()]);
                                }
                            }

                            return result;
                        }
                    },
                    getParameterUrl: function(parameter, url) {
                        if (!url) url = window.location.href;
                        var reg = new RegExp('[?&]' + parameter + '=([^&#]*)', 'i');
                        var string = reg.exec(url);
                        return string ? string[1] : undefined;
                    },
                    showloading: function(content = null) {
                        //        			$("#loading").removeClass("d-none");
                        $('body .loader').addClass('active');
                        if (content) {
                            $('body .loader').find(".loader-content").html(content);
                        }
                    },
                    hideloading: function() {
                        //        			$("#loading").addClass("d-none");
                        $('body .loader').removeClass('active');
                        $('body .loader').find(".loader-content").empty();
                    },
                    hasRole: function(role) {
                        return (gonrinApp().currentUser != null && gonrinApp().currentUser.roles != null) && gonrinApp().currentUser.roles.indexOf(role) >= 0;
                    },
                    getCurrentUser: function() {
                        var self = this;
                        var token = storejs.get('X-USER-TOKEN');
                        $.ajaxSetup({
                            headers: {
                                'X-USER-TOKEN': token
                            }
                        });
                        self.showloading();
                        $.ajax({
                            url: self.serviceURL + '/api/v1/current_user',
                            dataType: "json",
                            success: function(data) {
                                if (data.active === 0 || data.active === '0') {
                                    gonrinApp().getRouter().navigate('confirm-active?uid=' + data.id);
                                    return false;
                                } else if (data.active === 1 || data.active === '1') {
                                    $.ajaxSetup({
                                        headers: {
                                            'X-USER-TOKEN': data.token
                                        }
                                    });
                                    storejs.set('X-USER-TOKEN', data.token);
                                    gonrinApp().postLogin(data);
                                } else {
                                    gonrinApp().notify("Tài khoản của bạn đã bị khóa");
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                self.hideloading();
                                var view = new splashView({ el: $('.content-contain') });
                                view.render();
                                try {
                                    codePush.sync();
                                } catch (error) {
                                    console.log("codepush sync error", error);
                                }
                                // self.router.navigate("login");
                            }
                        });
                    },
                    hasRole: function(role) {
                        return (gonrinApp().currentUser != null && gonrinApp().currentUser.roles != null) && gonrinApp().currentUser.roles.indexOf(role) >= 0;
                    },
                    updateCurrentUser: function(hoten) {
                        var self = this;
                        var currUser = self.currentUser;
                        if (!!hoten && hoten !== "") {
                            currUser.name = hoten;
                            $("span.username").html(currUser.name);
                        }
                    },
                    postLogin: function(data) {
						var self = this;
						
                        self.currentUser = new Gonrin.User(data);

                        if (this.hasRole('admin')) {
                            $('div.content-contain').html(layout);
                            this.$header = $('body').find(".main-sidebar");
                            this.$content = $('body').find(".content-area");
                            this.$navbar = $('body').find(".main-sidebar .nav-wrapper");



                            if (!data.name || data.name === "") {
                                data.name = data.email;
                            }
                            $("span.username").html(data.name);
                            this.nav = new Nav({ el: this.$navbar });
                            self.nav.render();
                            self.hideloading();


                        } else if (self.hasRole('admin_tyt') || self.hasRole('admin_benhvien') ||
                            self.hasRole('canbo_benhvien') || self.hasRole('canbo_tyt')) {

                            if (self.router.currentRoute().route === "ticket/collection") {
                                self.router.refresh();
                            } else {
                                gonrinApp().getRouter().navigate("ticket/collection");
                            }
                        } else {
                            //role nguoidan
                            if (self.router.currentRoute().route === "home") {
                                self.router.refresh();
                            } else {
                                gonrinApp().getRouter().navigate("home");
                            }
                        }

                        self.hideloading();

                        self.initNotify();
                        $.extend($.easing, {
                            easeOutSine: function easeOutSine(x, t, b, c, d) {
                                return c * Math.sin(t / d * (Math.PI / 2)) + b;
                            }
                        });

                        var slideConfig = {
                            duration: 270,
                            easing: 'easeOutSine'
                        };

                        // Add dropdown animations when toggled.
                        $(':not(.main-sidebar--icons-only) .dropdown').on('show.bs.dropdown', function() {
                            $(this).find('.dropdown-menu').first().stop(true, true).slideDown(slideConfig);
                        });

                        $(':not(.main-sidebar--icons-only) .dropdown').on('hide.bs.dropdown', function() {
                            $(this).find('.dropdown-menu').first().stop(true, true).slideUp(slideConfig);
                        });

                        /**
                         * Sidebar toggles
                         */
                        $('.toggle-sidebar').unbind("click").bind('click', function(e) {
                            $('.main-sidebar').toggleClass('open');
                        });
                        $('.notifications').on('show.bs.dropdown', function() {
                            gonrinApp().get_list_notify();
                        });

                        // setInterval(function () { gonrinApp().check_notify(); }, 3 * 60000);
                        try {
                            if (navigator.userAgent.match(/(iPad|iPhone|iPod)/i)) {
                                var fastClick = new FastClick(document.body);
                            }
                            codePush.sync();
                        } catch (error) {
                            console.log("codepush sync error", error);
                        }
                    },
                    change_avatar: function(url_image) {
                        var self = this;
                        if (self.hasRole("nguoidan")) {
                            if (url_image == "default") {
                                $(".user-avatar").attr({ "src": "static/images/default_image.png" });
                            } else {
                                var url = self.check_image(url_image);
                                $(".user-avatar").attr({ "src": url });
                            }
                        }
                    },
                    hex_to_ascii: function(str1) {
                        var hex = str1.toString();
                        return decodeURIComponent(hex.replace(/\s+/g, '').replace(/[0-9a-f]{2}/g, '%$&'));
                    },
                    scanQRCode: function(model) {
                        /*
                         * Theo thứ tự là: số thẻ 0| họ tên 1| ngày sinh 2| giới tinh 3| địa chỉ 4| mã
                         * tỉnh/thành phố và mã cskcb của bệnh viện đăng ký ban đầu 5| giá trị sử dụng từ 6
                         * | bỏ 7| ngày ký 8| bỏ 9| bỏ 10| ngày bắt đầu (liên tục 5 năm từ) 11
                         */
                        cordova.plugins.barcodeScanner.scan(
                            function(result) {
                                if (!!result.cancelled && (result.cancelled === true || result.cancelled === 1)) {
                                    return false;
                                }
                                var url_get = gonrinApp().serviceURL;
                                console.log(result.text);
                                var arrData = result.text.split('|');
                                var hoten = gonrinApp().hex_to_ascii(arrData[1]);
                                var diachi = gonrinApp().hex_to_ascii(arrData[4]);
                                if (!!diachi && diachi.length > 0) {
                                    diachi = diachi.split('_')[0];
                                }
                                var birthday = gonrinApp().parseDate(arrData[2]);

                                model.$el.find('[name=ma_bhyt]').val(arrData[0]),
                                    model.$el.find('[name=name_input]').val(hoten),
                                    model.$el.find('#birthday').data("gonrin").setValue(birthday),
                                    model.$el.find('[name=address]').val(diachi);
                                model.$el.find('#gioitinh').val(arrData[3]);


                            },
                            function(error) {
                                console.log(error);
                                //  	                          gonrinApp().notify("Có lỗi xảy ra, vui lòng thực hiện lại sau, \n " + error);
                            }, {
                                prompt: "Di chuyển camera đến vùng chứa mã QR để quét",
                            }
                        );
                    },
                    change_avatar: function(url_image) {
                        var self = this;
                        if (self.hasRole("thongtin_canhan")) {
                            if (url_image == "default") {
                                $(".user-avatar").attr({ "src": "static/images/icon_giadinh_48x48.png" });
                            } else {
                                var url = self.check_image(url_image);
                                $(".user-avatar").attr({ "src": url });
                            }
                        }
                    },
                    toInt: function(x) {
                        return parseInt(x) ? parseInt(x) : 0;
                    },
                    initNotify: function() {
                        var self = this;
                        window.FirebasePlugin.setUserId(self.currentUser.id);
                        window.FirebasePlugin.hasPermission(function(data) {
                            
                            if (data.isEnabled !== true && navigator.userAgent.match(/(iPad|iPhone|iPod)/i)) {
                                window.FirebasePlugin.grantPermission();
                            }
                        });
                        window.FirebasePlugin.getToken(function(token) {
                            // save this server-side and use it to push notifications to this device
                            console.log("FirebasePlugin.getToken==", token);
                            gonrinApp().set_token_firebase_to_server(token);
                        }, function(error) {
                            console.error(error);
                        });
                        //    				window.FirebasePlugin.onTokenRefresh(function(token) {
                        //    				    console.log("FirebasePlugin.onTokenRefresh==",token);
                        //    				    gonrinApp().set_token_firebase_to_server(token);
                        //    				}, function(error) {
                        //    				    console.error(error);
                        //    				});
                        //        				{"id":"f0bbe501-dde0-4445-8db2-5c667ddfd5de","created_at":"1557849532","from":"637676599535","notification":{"body":"Đăng ký khám mới","e":"1","sound2":"bell","sound":"default"},"title":"Đăng ký khám mới","action":"{\"datkham_id\":\"100000144\"}","deleted":"false","collapse_key":"com.sosuckhoe.datkham","type":"text","updated_at":"1557849532","url":"https://somevabe.com/datkham/#dangkykham/model?id=100000144","content":"Yêu cầu đặt khám mới từ Nguyen Van B với mã số là 100000144"}
                        window.FirebasePlugin.onMessageReceived(function(notification) {
							console.log("onMessageReceived===");
                            console.log(notification);
                            var notifyView = new NotifyView({ viewData: notification });
                                notifyView.dialog();
                                setTimeout(() => {
                                    notifyView.close();
                                    // notifyView.$el.find("#myModalNotify").modal("hide");
                                }, 5000);
                            //android====
                            // messageType: "notification"
                            // id: "0:1586839743283781%27d7064d27d7064d"
                            // ttl: "3600"
                            // uid: "d4917016-8cf4-4b28-8057-c5fc9484bbf5"
                            // body: "day la noi dung notify"
                            // url: "https://drlinks.yte360.com#ticket/collection"
                            // from: "752607942662"
                            // icon: "https://drlinks.yte360.com/static/images/ic_launcher.png"
                            // sound: "bell"
                            // title: "test title"
                            // sent_time: "1586839743279"
                            // content: "day la noi dung notify"
                            // collapse_key: "com.yte360.drlinks"
                            // show_notification: "false"

                            //ios:
                            // {"content":"day la noi dung notify",
                            // "uid":"d4917016-8cf4-4b28-8057-c5fc9484bbf5",
                            // "google.c.a.e":"1",
                            // "aps":{"alert":{"title":"test title","body":"day la noi dung notify"},"mutable-content":1,"badge":1},
                            // "title":"test title",
                            // "fcm_options":{"image":"https://drlinks.yte360.com/static/images/ic_launcher.png"},
                            // "gcm.message_id":"1586840350695644",
                            // "messageType":"notification",
                            // "url":"https://drlinks.yte360.com#ticket/collection"}
							
                            // var check_url = notification.url;
                            // if (!!check_url  && check_url.indexOf("drlinks.yte360.com") >= 0) {
                            //     if (check_url.indexOf("#") >= 0) {
                            //         var navigate_url = check_url.split("#")[1];
                            //         gonrinApp().getRouter().navigate(navigate_url);
                            //     } else {
                            //         window.open(check_url, "_self");
                            //     }
                            // } else {
                            //     gonrinApp().notify(notification.title+"<br>"+notification.content);
							// 	// if(navigator.userAgent.match(/(iPad|iPhone|iPod)/i)){
							// 	// }
                                
                            // }
                            //    					gonrinApp().check_notify();

                            //    					gonrinApp().notify({"message":notification.content,"url":notification.url,"title":notification.title},{ type: "info", delay: 3000 },{"url_target":"_self"});
                        }, function(error) {
                            console.error(error);
                        });

                    },
                    set_token_firebase_to_server: function(currentToken) {
                        if (currentToken) {
                            var params = JSON.stringify({
                                data: currentToken
                            });
                            $.ajax({
                                url: gonrinApp().serviceURL + '/api/v1/set_notify_token',
                                dataType: "json",
                                type: "POST",
                                data: params,
                                headers: {
                                    'content-type': 'application/json'
                                },
                                success: function(data) {
                                    console.log("set token firebase success!!!");
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    console.log("set notify firebase failed");
                                }
                            });


                        } else {
                            console.log('No Instance ID token available. Request permission to generate one.');
                        }
                    },
                    get_list_notify: function() {
                        var self = this;
                        $.ajax({
                            url: self.serviceURL + '/api/v1/notify/read',
                            dataType: "json",
                            type: "POST",
                            //               		    data: {"q": JSON.stringify({"order_by":[{"field": "updated_at", "direction": "desc"}], "limit":100})},
                            headers: {
                                'content-type': 'application/json'
                            },
                            success: function(data) {
                                $(".badge-pill").addClass("d-none").html("");
                                self.render_notify(data.objects);

                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log("set notify firebase failed");
                            }
                        });
                    },
                    check_notify: function() {
                        var self = this;
                        $.ajax({
                            url: self.serviceURL + '/api/v1/notify/check',
                            dataType: "json",
                            type: "GET",
                            headers: {
                                'content-type': 'application/json'
                            },
                            success: function(data) {
                                if (data !== null && data.data > 0) {
                                    $(".badge-pill").removeClass("d-none").html(data.data);
                                } else {
                                    $(".badge-pill").addClass("d-none");
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log("check_notify error====", XMLHttpRequest);
                            }
                        });
                    },
                    render_notify: function(data) {
                        var self = this;
                        var html_menu = $("#menu_notify").html("");

                        if (data.length === 0) {
                            html_menu.append(`<a class="dropdown-item" href="javascript:;">
        	                      <div class="notification__icon-wrapper">
        	                        <div class="notification__icon">
        	                          <i class="material-icons"></i>
        	                        </div>
        	                      </div>
        	                      <div class="notification__content">
        	                        <span class="notification__category">Không có thông báo mới</span>
        	                      </div>
        	                    </a>`);
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                if (i <= 10) {
                                    var class_unread = "";
                                    if (data[i].read_at === null || data[i].read_at <= 0) {
                                        class_unread = "unread";
                                    }
                                    var url = data[i].url;
                                    if (url !== null && url !== "" && url.split('#').length > 0) {
                                        url = url.split('#')[1];
                                    }
                                    html_menu.append(`<a class="dropdown-item ` + class_unread + `" href="javascript:;" onClick="gonrinApp().getRouter().navigate(\'` + url + `\',{trigger: true});">
        			                      <div class="notification__icon-wrapper">
        			                        <div class="notification__icon">
        			                          <i class="material-icons"></i>
        			                        </div>
        			                      </div>
        			                      <div class="notification__content">
        			                        <span class="notification__category">` + (data[i].title || "") + `</span>
        			                        <p>` + (data[i].content || "") + `</p>
        			                      </div>
        			                    </a>`);
                                }
                            }
                        }
                        var window_h = "max-height:" + ($(window).height() - 110) + "px";
                        $("#menu_notify").attr({ "style": window_h });
                    },
                    responsive_table: function() {
                        // inspired by http://jsfiddle.net/arunpjohny/564Lxosz/1/
                        if ($(window).width() < 768) {
                            $('.table-mobile').find("td").each(function(i) {
                                if ($(this).text() === " " || $(this).text() === "" || $(this).text() === null || $(this).text() === undefined || $(this).text() === 'none') {
                                    $(this).hide();
                                }
                            });
                            $('.table-mobile').find("th").each(function(i) {
                                var el = $('.table-mobile td:nth-child(' + (i + 1) + ')');
                                if ($(this).text() === " " || $(this).text() === "" || $(this).text() === null || $(this).text() === undefined || $(this).text() === 'none') {
                                    //    						   el.prepend('<span class="table-thead"></span> ');
                                } else {
                                    el.prepend('<span class="table-thead">' + $(this).text() + ':</span> ');
                                }

                                $('.table-thead').hide();
                            });
                            $('.table-mobile').each(function() {
                                var thCount = $(this).find("th").length;
                                var rowGrow = 100 / thCount + '%';
                                //console.log(rowGrow);
                                $(this).find("th, td").css('flex-basis', rowGrow);
                                $(this).find("tr").addClass("shadow-sm p-2 mb-1 bg-white rounded");

                            });

                            function flexTable() {
                                //			   if ($(window).width() < 768) {
                                $(".table-mobile").each(function(i) {
                                    $(this).find('.table-thead').show();
                                    $(this).find('thead').hide();
                                });
                                // window is less than 768px   
                                //			   } else {
                                //				   $(".table").each(function (i) {
                                //					   $(this).find('.table-thead').hide();
                                //				      $(this).find('thead').show();
                                //				   });
                                //			   }
                            }
                            flexTable();
                            //    					window.onresize = function(event) {
                            //    					    flexTable();
                            //    					};
                        }
                    },
                    check_image: function(image) {
                        if (!image) {
                            return "";
                        } else {
                            var url_image = image.link;
                            if (!url_image) {
                                return "";
                            }
                            if (url_image.startsWith("https://somevabe.com/chuyengiatuvan")) {
                                url_image = url_image;
                            } else {
                                url_image = static_url + url_image;
                            }
                            return url_image;
                        }
                    },
                    toInt: function(x) {
                        return parseInt(x) ? parseInt(x) : 0;
                    },
                    convert_khongdau: function(strdata) {
                        var kituA = ["á", "à", "ạ", "ã", "ả", "â", "ấ", "ầ", "ậ", "ẫ", "ă", "ằ", "ắ", "ẳ"],
                            kituE = ["é", "è", "ẹ", "ẻ", "ẽ", "ê", "ế", "ề", "ệ", "ễ", "ể"],
                            kituI = ["í", "ì", "ị", "ỉ", "ĩ"],
                            kituO = ["ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ"],
                            kituU = ["ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ"],
                            kituY = ["ỳ", "ý", "ỵ", "ỷ", "ỹ"];

                        var str2 = strdata.toLowerCase();
                        for (var i = 0; i < kituA.length; i++) {
                            str2 = str2.replace(kituA[i], "a");
                        }
                        for (var i = 0; i < kituE.length; i++) {
                            str2 = str2.replace(kituE[i], "e");
                        }
                        for (var i = 0; i < kituI.length; i++) {
                            str2 = str2.replace(kituI[i], "i");
                        }
                        for (var i = 0; i < kituO.length; i++) {
                            str2 = str2.replace(kituO[i], "o");
                        }
                        for (var i = 0; i < kituU.length; i++) {
                            str2 = str2.replace(kituU[i], "u");
                        }
                        for (var i = 0; i < kituY.length; i++) {
                            str2 = str2.replace(kituY[i], "y");
                        }
                        str2 = str2.replace("đ", "d");
                        // if(upper === true){
                        // 	return str2.toUpperCase();
                        // }
                        return str2;
                    },

                });
                mobileapp.isMobile = true;
                mobileapp.registerScrollToolbar = function(view) {
                    if (!!mobileapp.isMobile) {
                        Backbone.off("window:scroll").on("window:scroll", function(evt) {
                            var toolwrap = view.$el.find(".toolbar-wraper");
                            if (evt.direction === "up") {
                                if ($(window).scrollTop() > 30) {
                                    toolwrap.addClass("autofix fix");
                                } else {
                                    toolwrap.removeClass("autofix fix");
                                }
                            } else if (evt.direction === "down") {
                                toolwrap.removeClass("autofix fix");
                            }
                        });
                    }

                };
                Backbone.history.start();
                var iScrollPos = 0;
                $(window).scroll(function() {
                    var iCurScrollPos = $(this).scrollTop();
                    if (iCurScrollPos > (iScrollPos + 30)) {
                        iScrollPos = iCurScrollPos;
                        Backbone.trigger('window:scroll', { direction: "down" });

                    } else if (iCurScrollPos < (iScrollPos - 30)) {
                        iScrollPos = iCurScrollPos;
                        Backbone.trigger('window:scroll', { direction: "up" });
                    }

                });

            });
    },
    receivedEvent: function() {
        var self = this;
        // OneSignal Initialization
        // Enable to debug issues.
        // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

        // Set your iOS Settings
        //        var iosSettings = {};
        //        iosSettings["kOSSettingsKeyAutoPrompt"] = false;
        //        iosSettings["kOSSettingsKeyInAppLaunchURL"] = true;
        var appkey_onesignal = '267683b8-4b18-4d0c-b1c3-c2a4fe416aed'
        if (navigator.userAgent.match(/(iPad|iPhone|iPod)/i)) {
            appkey_onesignal = '2cbdb907-ec2b-428c-8bd6-5cca37836a85';

        }
        //window.plugins.OneSignal.setLogLevel({logLevel: 6, visualLevel: 4});
        window.plugins.OneSignal
            .startInit(appkey_onesignal)
            .handleNotificationReceived(function(jsonData) {
                alert("Notification received: \n" + JSON.stringify(jsonData));
            })
            .handleNotificationOpened(function(jsonData) {
                alert("Notification opened: \n" + JSON.stringify(jsonData));
                console.log('didOpenRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
            })
            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.InAppAlert)
            //          .iOSSettings(iosSettings)
            .endInit();

        //        if (addedObservers == false) {
        //            addedObservers = true;

        //            window.plugins.OneSignal.addEmailSubscriptionObserver(function(stateChanges) {
        //                console.log("Email subscription state changed: \n" + JSON.stringify(stateChanges, null, 2));
        //            });

        window.plugins.OneSignal.addSubscriptionObserver(function(stateChanges) {
            console.log("Push subscription state changed: " + JSON.stringify(stateChanges, null, 2));
        });

        window.plugins.OneSignal.addPermissionObserver(function(stateChanges) {
            console.log("Push permission state changed: " + JSON.stringify(stateChanges, null, 2));
        });
        //        }
        //Call syncHashedEmail anywhere in your app if you have the user's email.
        //This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        //window.plugins.OneSignal.syncHashedEmail(userEmail);
    }

};

cordovaApp.initialize();
