define(function(require){
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),

        tpl = require('text!app/index/tpl/index.html');
        
    return Gonrin.ModelView.extend({
        template : tpl,
        modelSchema	: {},
        render:function(){
            var self = this;
            self.applyBindings();
        }
    });
})