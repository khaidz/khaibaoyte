define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        tpl = require('text!app/khaibaoyte/khaibao/tpl/model.html'),
        schema = require('json!schema/ToKhaiYTeSchema.json'),
        combodate = require('js/combodate'),
        storejs = require('vendor/store');
    var jsonKhoaPhong = require('json!app/enum/KhoaTrungTam.json');

    return Gonrin.ModelView.extend({
        template: tpl,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "tokhai_yte",
        $selectize: null,
        dataTrieuChung: [],
        dataDichTe: [],
        trieuChungSelected: [],
        dichTeSelected: [],
        check: false,
        tools: [
            {
                name: "defaultgr",
                type: "group",
                groupClass: "toolbar-group w-50",
                buttons: [
                    {
                        name: "back",
                        type: "button",
                        buttonClass: "btn btn-secondary width-sm ml-1",
                        label: "Trở về",
                        visible: function () {
                            return !!this.getApp().getRouter().getParam("id");
                        },
                        command: function () {
                            Backbone.history.history.back();
                        }
                    },
                    {
                        name: "save",
                        type: "button",
                        buttonClass: "btn btn-green width-sm ml-1",
                        label: "Gửi",
                        visible: function () {
                            return !this.getApp().getRouter().getParam("id");
                        },
                        command: function () {
                            var self = this;
                            var loai_khaibao = self.$el.find('input[name="loai_khaibao"]:checked').val();
                            if (!loai_khaibao) {
                                self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                return;
                            } else {
                                self.model.set('loai_khaibao', loai_khaibao);
                            }
                            if (!self.model.get('so_dien_thoai')) {
                                self.getApp().notify({ message: "Chưa nhập số điện thoại" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            if (!self.getApp().validatePhone(self.model.get('so_dien_thoai'))) {
                                self.getApp().notify({ message: "Số điện thoại không hợp lệ" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            if (!self.model.get('ten')) {
                                self.getApp().notify({ message: "Chưa nhập họ và tên" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            
                            if (!!self.$el.find('.day.custom-select').val() && !!self.$el.find('.month.custom-select').val() && !!self.$el.find('.year.custom-select').val() ) {
                                var ngaysinh = self.$el.find('#combodate').combodate('getValue', null).unix();
                                self.model.set('ngaysinh', ngaysinh);
                            }else{
                                self.model.set('ngaysinh', null);

                            }
                            

                            if (!self.model.get('gioi_tinh')) {
                                self.getApp().notify({ message: "Chưa nhập giới tính" }, { type: "danger", delay: 1000 });
                                return;
                            }

                            if (!self.model.get('dia_chi')) {
                                self.getApp().notify({ message: "Chưa nhập địa chỉ" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            if (self.$selectize != null) {
                                if (!!self.$selectize[0].selectize.getValue()) {
                                    self.model.set('khoa_phong', self.$selectize[0].selectize.getValue());
                                };
                            }
                            if(loai_khaibao=='4'){
                                if (!self.model.get('khoa_phong')) {
                                    self.getApp().notify({ message: "Chưa chọn khoa/phòng khám" }, { type: "danger", delay: 1000 });
                                    return;
                                }
                            }else{
                                if (!self.model.get('lydodenvien')) {
                                    self.getApp().notify({ message: "Chưa chọn lý do đến viện" }, { type: "danger", delay: 1000 });
                                    return;
                                }
                            }

                            if (!self.validateTrieuChung()) {
                                self.getApp().notify({ message: "Vui lòng xác nhận các triệu chứng" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            if (!self.validateDichTe()) {
                                self.getApp().notify({ message: "Vui lòng xác nhận các yếu tố dịch tễ" }, { type: "danger", delay: 1000 });
                                return;
                            }

                            self.model.set({ 'trieuchung': self.trieuChungSelected, 'dichte': self.dichTeSelected });
                            if (self.dichTeSelected.length > 0) {
                                self.model.set('coyeuto_dichte', 1);
                            } else {
                                self.model.set('coyeuto_dichte', 0);
                            }

                            
                            if (!!storejs.get('UID-TOKHAI-YTE')) {
                                self.model.set('uid', storejs.get('UID-TOKHAI-YTE'));
                            } else {
                                var uid = self.getApp().default_uuid();
                                self.model.set('uid', uid);
                                storejs.set('UID-TOKHAI-YTE', uid);
                            }
                            self.getApp().showloading();
                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Khai báo y tế thành công");
                                    var path = "tokhai_yte/ketqua?id=" + respose.id;
                                    self.getApp().getRouter().navigate(path);
                                    self.getApp().hideloading();
                                },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });

                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Khai báo không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        }
                    }
                ],
            }],
        uiControl: {
            fields: [
                {
                    field: "gioi_tinh",
                    uicontrol: "combobox",
                    textField: "text",
                    valueField: "value",
                    cssClass: "form-control",
                    dataSource: [
                        { value: 1, text: "Nam" },
                        { value: 2, text: "Nữ" },
                        { value: 3, text: "Giới tính khác" },
                    ],
                    readonly: true,
                    enable: false
                },
                {
                    field: "lydodenvien",
                    uicontrol: "combobox",
                    textField: "text",
                    valueField: "text",
                    cssClass: "form-control",
                    dataSource: [
                        { value: 1, text: "Khám, chữa bệnh" },
                        { value: 2, text: "Liên hệ công tác" },
                        { value: 3, text: "Khác" },
                    ],
                    readonly: true,
                    enable: false
                },
                
                {
                    field: "ngaysinh",
                    uicontrol: "datetimepicker",
                    format: "DD/MM/YYYY",
                    textFormat: "DD/MM/YYYY",
                    extraFormats: ["DDMMYYYY"],
                    parseInputDate: function (val) {
                        return gonrinApp().parseDate(val);
                    },
                    parseOutputDate: function (date) {
                        return date.unix();
                    },
                    disabledComponentButton: true
                },
            ]
        },
        render: function () {

            var self = this;
            var id = this.getApp().getRouter().getParam("id");
            self.loadDataTrieuChung();
            self.loadDataDichTe();
            self.$el.find('#combodate').combodate({
                firstItem: 'name',
                minYear: 1900,
                maxYear: new Date().getFullYear(),
                customClass: 'custom-select',
                smartDays: true

            });
            

            self.$el.find('input.lydodenvien-input').attr('inputmode', 'email');

            self.$selectize = self.$el.find('#selectize-khoaphong').selectize({
                maxItems: 1,
                valueField: 'ten',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                options: jsonKhoaPhong
            });

            if (id) {
                this.model.set('id', id);
                this.model.fetch({
                    success: function (data) {
                        self.applyBindings();
                        if(self.model.get('ngaysinh')){
                            self.$el.find('#combodate').combodate('setValue', new Date(self.model.get('ngaysinh')*1000)); 
                        }
                        if (self.model.get('loai_khaibao') == '1') {
                            self.$el.find('.ma_benh_nhan').show();
                        } else {
                            self.$el.find('.ma_benh_nhan').hide();
                        }
                        if (self.model.get('loai_khaibao') === '4') {
                            self.$el.find('.khoaphong').show();
                            self.$el.find('.lydodenvien').hide();
                        } else {
                            self.$el.find('.khoaphong').hide();
                            self.$el.find('.lydodenvien').show();
                        }
                        
                        self.$el.find('input[name="loai_khaibao"][value="' + self.model.get('loai_khaibao') + '"]').prop('checked', true);

                    },
                    error: function () {
                        self.getApp().notify({ message: "Có lỗi xảy ra. Thử lại sau" }, { type: "danger", delay: 1000 });
                    },
                });
            } else {
                self.$el.find('input[name="loai_khaibao"][value="1"]').prop('checked', true);
                self.model.set({ 'gioi_tinh': 1,'so_dien_thoai':'0'});
                self.applyBindings();
            }

            self.$el.find('.gioitinh input').attr('inputmode', 'none');
            self.$el.find('.lydodenvien input').attr('inputmode', 'none');


            self.$el.find('input[name="loai_khaibao"]').on('change', function () {
                self.resetModel();
                self.$el.find('.day.custom-select').add('.month.custom-select').add('.year.custom-select').val('');

                self.check=false;
                if (self.$el.find('input[name="loai_khaibao"]:checked').val() === '1') {
                    self.$el.find('.ma_benh_nhan').show();
                } else {
                    self.$el.find('.ma_benh_nhan').hide();
                }

                if (self.$el.find('input[name="loai_khaibao"]:checked').val() === '4') {
                    self.$el.find('.khoaphong').show();
                    self.$el.find('.lydodenvien').hide();
                } else {
                    self.$el.find('.khoaphong').hide();
                    self.$el.find('.lydodenvien').show();
                }

            })
            if (self.$el.find('input[name="loai_khaibao"]:checked').val() === '4') {
                self.$el.find('.khoaphong').show();
                self.$el.find('.lydodenvien').hide();
            } else {
                self.$el.find('.khoaphong').hide();
                self.$el.find('.lydodenvien').show();
            }

            self.$el.find('.ma_benh_nhan_ip').on('keyup', function () {
                var text = $(this).val();
                if (!!text && text.trim().length ==9) {
                    $.ajax({
                        url: (self.getApp().serviceURL || "") + '/api/v1/khaibaoyte/benhnhan',
                        type: 'post',
                        data: JSON.stringify({
                            ma_benh_nhan: text
                        }),
                        headers: {
                            'content-type': 'application/json'
                        },
                        dataType: 'json',
                        success: function (result) {
                            var data = result.data;
                            if (data != null) {
                                self.model.set({
                                    'ten': data.ten,
                                    'so_dien_thoai': data.so_dien_thoai,
                                    'ngaysinh': data.ngaysinh,
                                    'gioi_tinh': data.gioi_tinh,
                                    'dia_chi': data.dia_chi,
                                    // 'khoa_phong': data.khoa_phong,
                                    'lydodenvien': data.lydodenvien,
                                    'trieuchung': data.trieuchung,
                                    'dichte': self.dichte,
                                    'ghi_chu': data.ghi_chu
                                })

                                self.check = true;
                                self.$el.find('.day.custom-select').add('.month.custom-select').add('.year.custom-select').val('');

                                if(data.ngaysinh!=null && data.ngaysinh!=undefined){
                                    self.$el.find('#combodate').combodate('setValue', new Date(self.model.get('ngaysinh')*1000));
                                }
                                // if (!!self.model.get('khoa_phong')) {
                                //     self.$selectize[0].selectize.setValue(self.model.get('khoa_phong'));
                                // }

                            }
                        },
                        error: function (xhr, status, error) {

                        }
                    });
                }else if(text==null || text==undefined || text.trim().length==0){
                    self.model.set({
                        'ten': null,
                        'so_dien_thoai': null,
                        'ngaysinh': null,
                        'gioi_tinh': 1,
                        'dia_chi': null,
                        // 'khoa_phong': data.khoa_phong,
                        'lydodenvien': null,
                        'trieuchung': null,
                        'dichte': null,
                        'ghi_chu': null
                    });
                    self.check = false;
                    self.$el.find('.day.custom-select').add('.month.custom-select').add('.year.custom-select').val('');
                }
            })

            self.$el.find('.so_dien_thoai').on('keyup', function () {
                var text = $(this).val();
                var loai_kb = self.$el.find('input[name="loai_khaibao"]:checked').val();
                if (self.check==false && self.getApp().validatePhone(text)) {
                    $.ajax({
                        url: (self.getApp().serviceURL || "") + '/api/v1/khaibaoyte/phone',
                        type: 'post',
                        data: JSON.stringify({
                            dien_thoai: text,
                            loai_kb: loai_kb
                        }),
                        headers: {
                            'content-type': 'application/json'
                        },
                        dataType: 'json',
                        success: function (result) {
                            var data = result.data;
                            if (data != null) {
                                self.model.set({
                                    'ten': data.ten,
                                    'ngaysinh': data.ngaysinh,
                                    'gioi_tinh': data.gioi_tinh,
                                    'dia_chi': data.dia_chi,
                                    // 'khoa_phong': data.khoa_phong,
                                    'lydodenvien': data.lydodenvien,
                                    'ghi_chu': data.ghi_chu
                                })
                                if (self.$el.find('input[name="loai_khaibao"]:checked').val() == '1') {
                                    self.model.set({ 'ma_benh_nhan': data.ma_benh_nhan });
                                }
                                self.$el.find('.day.custom-select').add('.month.custom-select').add('.year.custom-select').val('');

                                if(data.ngaysinh!=null && data.ngaysinh!=undefined){
                                    self.$el.find('#combodate').combodate('setValue', new Date(self.model.get('ngaysinh')*1000));
                                }
                                if (self.$el.find('input[name="loai_khaibao"]:checked').val() == '4') {
                                    self.model.set('khoa_phong', data.khoa_phong);
                                    if(!!self.model.get('khoa_phong')){
                                        self.$selectize[0].selectize.setValue(self.model.get('khoa_phong'));
                                    }
                                }
                            }
                        },
                        error: function (xhr, status, error) {

                        }
                    });
                }
                if(self.check==false  && (text==null || text==undefined || text.trim().length==0)){
                    self.model.set({
                        'ten': null,
                        'so_dien_thoai': null,
                        'ngaysinh': null,
                        'gioi_tinh': 1,
                        'dia_chi': null,
                        // 'khoa_phong': data.khoa_phong,
                        'lydodenvien': null,
                        'trieuchung': null,
                        'dichte': null,
                        'ghi_chu': null
                    });
                    self.$el.find('.day.custom-select').add('.month.custom-select').add('.year.custom-select').val('');

                }
            })

        },

        validateTrieuChung: function () {
            var self = this;
            self.trieuChungSelected = [];
            for (let i = 0; i < self.dataTrieuChung.length; i++) {
                var value = self.$el.find(`input[name="trieuchung_${self.dataTrieuChung[i].id}"]:checked`).val();
                if (self.dataTrieuChung[i].batbuoc == 1 && (value == null || value == undefined)) {
                    return false;
                }
                if (value == "1") self.trieuChungSelected.push(self.dataTrieuChung[i])
            }

            return true;
        },
        validateDichTe: function () {
            var self = this;
            self.dichTeSelected = [];
            for (let i = 0; i < self.dataDichTe.length; i++) {
                var value = self.$el.find(`input[name="dichte_${self.dataDichTe[i].id}"]:checked`).val();
                if (self.dataDichTe[i].batbuoc == 1 && (value == null || value == undefined)) {
                    return false;
                }
                if (value == "1") self.dichTeSelected.push(self.dataDichTe[i])
            }

            return true;
        },
        loadDataTrieuChung: function () {
            var self = this;
            var query = {
                "filters": {
                    "trangthai": { "$eq": 1 }
                },
                "order_by": [{ "field": "thutu_uutien", "direction": "asc" }, { "field": "updated_at", "direction": "asc" }]
            };
            $.ajax({
                url: self.getApp().serviceURL + '/api/v1/trieuchung?q=' + JSON.stringify(query),
                dataType: "json",
                success: function (res) {
                    var objects = res.objects;
                    self.dataTrieuChung = objects;
                    var html = objects.map((value, index) => {
                        return `
                        <tr>
                            <td>${value.ten} ${value.batbuoc == 1 ? '<span class="text-important">(*)</span>' : ''}</td>
                            <td class="text-center">
                                <label class="lb-radio"><input type="radio" id="${value.id}" name="trieuchung_${value.id}" class="radio-trieuchung" value="1"></label>
                            </td>
                            <td class="text-center">
                                <label class="lb-radio"><input type="radio" id="${value.id}" name="trieuchung_${value.id}" class="radio-trieuchung" value="0"></label>
                            </td>
                        </tr>
                        `;
                    }).join('');
                    self.$el.find("#list-trieuchung").html(html);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
            });
        },

        loadDataDichTe: function () {
            var self = this;
            var query = {
                "filters": {
                    "trangthai": { "$eq": 1 }
                },
                "order_by": [{ "field": "thutu_uutien", "direction": "asc" }, { "field": "updated_at", "direction": "asc" }]
            };
            $.ajax({
                url: self.getApp().serviceURL + '/api/v1/dichte?q=' + JSON.stringify(query),
                dataType: "json",
                success: function (res) {
                    var objects = res.objects;
                    self.dataDichTe = objects;
                    var html = objects.map((value, index) => {
                        var ten = value.ten;
                        var text = ten;
                        var tooLong = false;
                        if (!!ten && ten.length > 150) {
                            text = ten.slice(0, 150) + "...";
                            tooLong = true;
                        }

                        self.$el.find('#list-modal').after(`
                        <div id="full-detail-modal-${value.id}" class="modal fade show" tabindex="-1" aria-labelledby="standard-modalLabel" style="display: none; padding-right: 12px;" aria-modal="true" role="dialog">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="standard-modalLabel">Chi tiết</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body" style="white-space: pre-line">${ten}</div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>
                        `);
                        return `
                        <tr>
                            <td>${text}${tooLong ? `<a class="a" data-toggle="modal" data-target="#full-detail-modal-${value.id}">Xem thêm</a>` : ' '}${value.batbuoc == 1 ? '<span class="text-important">(*)</span>' : ''}</td>
                            <td class="text-center">
                                <label class="lb-radio"><input type="radio" id="${value.id}" name="dichte_${value.id}" class="radio-dichte" value="1"></label>
                            </td>
                            <td class="text-center">
                                <label class="lb-radio"><input type="radio" id="${value.id}" name="dichte_${value.id}"class="radio-dichte" value="0"></label>
                            </td>
                        </tr>
                        `;
                    }).join('');
                    self.$el.find("#list-dichte").html(html);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
            });
        },
        resetModel: function () {
            var self = this;
            self.model.set({
                'ma_benh_nhan': '',
                'so_dien_thoai': null,
                'ten': '',
                'ngaysinh': null,
                'gioi_tinh': 1,
                'dia_chi': '',
                'khoa_phong': null,
                'lydodenvien':null,
                'ghi_chu': ''
            })
        }

    });
})