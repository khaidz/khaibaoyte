define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin');

    var template = require('text!app/khaibaoyte/trieuchung/tpl/collection.html'),
        schema = require('json!schema/TrieuChungSchema.json');
    var CustomFilterView = require('app/bases/CustomFilterView');
    var ModelDialogView = require('app/khaibaoyte/trieuchung/view/ModelDialogView');
    return Gonrin.CollectionView.extend({
        template: template,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "trieuchung",
        datatableClass: "table table-hover",
        tools: [{
            name: "defaultgr",
            type: "group",
            groupClass: "toolbar-group",
            buttons: [{
                name: "create",
                type: "button",
                buttonClass: "btn-success btn-sm",
                label: "TRANSLATE:CREATE",
                command: function () {
                    var self = this;
                    var dialogView = new ModelDialogView({});
                    dialogView.dialog({ "size": "normal" });
                    dialogView.on('savedata', function(data){
                        var $col = self.getCollectionElement();
                        $col.data('gonrin').filter();
                    })
                }
            }]
        }],
        uiControl: {
            orderBy: [
                { field: "thutu_uutien", direction: "asc" }
            ],
            fields: [
                { field: "ten", label: "Tên triệu chứng" },
                {
                    field: "thutu_uutien",
                    label: "Thứ tự ưu tiên",
                },
                {
                    field: "batbuoc",
                    label: "Bắt buộc nhập",
                    template: (rowData) => {
                        if (rowData.batbuoc == 0) {
                            return '<div class="text-danger">Không</div>';
                        } else if (rowData.batbuoc == 1) {
                            return '<div class="text-success">Có</div>';
                        }
                        return '';
                    }
                },

                {
                    field: "trangthai",
                    label: "Trạng thái",
                    template: (rowData) => {
                        if (rowData.trangthai == 0) {
                            return '<div class="text-danger">Đang bị ẩn</div>';
                        } else if (rowData.trangthai == 1) {
                            return '<div class="text-success">Đang hoạt động</div>';
                        }
                        return '';
                    }
                },

            ],
            onRowClick: function (event) {
                var self = this;
				var rowData = event.rowData;
				var dialogView = new ModelDialogView({ viewData: { "id": rowData.id } });
                dialogView.dialog({ "size": "normal" });
                dialogView.on('savedata', function(data){
                    var $col = self.getCollectionElement();
                    $col.data('gonrin').filter();
                })
            },
            datatableClass: "table table-mobile",
            selectedTrClass: "",
        },
        render: function () {
            var self = this;
            if (gonrinApp().currentUser === undefined || gonrinApp().currentUser === null) {
                self.getApp().notify({ message: "Hết phiên làm việc, vui lòng đăng nhập lại" }, { type: "danger", delay: 1000 });
                self.getApp().getRouter().navigate("login");
                return false;
            }
           
            self.query = null;
            var filter = new CustomFilterView({
                el: self.$el.find("#grid_search"),
                sessionKey: self.collectionName + "_filter"
            });
            filter.render();
            self.$el.find("#grid_search input").val("");
            self.$el.find("#grid_search input").attr({ "placeholder": "Nhập nội dung tìm kiếm" });
            var filter_query = self.uiControl.filters;
            if (filter_query !== undefined && filter_query !== null && filter_query !== false) {
                self.query = filter_query;
            }
            filter.model.set("text", "");

            self.uiControl.orderBy = [{ "field": "thutu_uutien", "direction": "asc" }];
            if (!filter.isEmptyFilter()) {
                var text = !!filter.model.get("text") ? filter.model.get("text").trim() : "";
                var filters = { "tenkhongdau": { "$likeI": self.getApp().convert_khongdau(text) }};
                var filter1;
                if (self.query !== null && self.query !== undefined) {
                    filter1 = {
                        "$and": [
                            filters, self.query
                        ]
                    };
                } else {
                    filter1 = filters;
                }
                self.uiControl.filters = filter1;
            }
            self.applyBindings();

            filter.on('filterChanged', function (evt) {
                var $col = self.getCollectionElement();
                var text = !!evt.data.text ? evt.data.text.trim() : "";
                if ($col) {
                    if (text !== null) {
                        var filters = { "tenkhongdau": { "$likeI": self.getApp().convert_khongdau(text) }};
                        var filter1;
                        if (self.query !== null && self.query !== undefined) {
                            filter1 = {
                                "$and": [
                                    filters, self.query
                                ]
                            };
                        } else {
                            filter1 = filters;
                        }
                        $col.data('gonrin').filter(filter1);
                    }
                }
                self.applyBindings();
            });
            self.$el.find("table").addClass("table-hover");
            self.$el.find("table").removeClass("table-striped");

            return this;
        },
    });

});