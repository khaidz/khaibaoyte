define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        tpl = require('text!app/khaibaoyte/trieuchung/tpl/model.html'),
        schema = require('json!schema/TrieuChungSchema.json');
    return Gonrin.ModelDialogView.extend({
        template: tpl,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "trieuchung",
        tools: [
            {
                name: "defaultgr",
                type: "group",
                groupClass: "toolbar-group",
                buttons: [
                    {
                        name: "back",
                        type: "button",
                        buttonClass: "btn btn-secondary width-sm",
                        label: "Đóng",
                        command: function () {
                            var self = this;
                            self.close();
                        },
                        visible: function () {
                            // return gonrinApp().currentUser.hasRole("Admin") || gonrinApp().currentUser.hasRole("CoSoKCB");
                            return true;
                        }
                    },
                    {
                        name: "save",
                        type: "button",
                        buttonClass: "btn btn-primary width-sm ml-1",
                        label: "Lưu",
                        command: function () {
                            var self = this;
                            if (!self.model.get('ten')) {
                                self.getApp().notify({ message: "Chưa nhập tên triệu chứng" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            var batbuoc = self.$el.find("#batbuoc").data('gonrin').getValue();
                            var trangthai = self.$el.find("#trangthai").data('gonrin').getValue();

                            if (batbuoc != "1" && batbuoc != "0") {
                                self.getApp().notify({ message: "Lựa chọn không hợp lệ" }, { type: "danger", delay: 1000 });
                                return;
                            }
                            if (trangthai != "1" && trangthai != "0") {
                                self.getApp().notify({ message: "Lựa chọn không hợp lệ" }, { type: "danger", delay: 1000 });
                                return;
                            }

                            self.model.set({ 'batbuoc': batbuoc, 'trangthai': trangthai });


                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Lưu thông tin thành công");
                                    self.trigger('savedata', {});
                                    self.close();
                                },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                            self.getApp().getRouter().navigate("login");
                                        } else if (($.parseJSON(error.xhr.responseText).error_code) === "ERROR_PERMISSION") {
                                            self.getApp().notify({ message: "Bạn không có quyền thực hiện tác vụ này" }, { type: "danger", delay: 1000 });
                                        }

                                        else {
                                            self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                                        }
                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        },
                    },
                    {
                        name: "delete",
                        type: "button",
                        buttonClass: "btn btn-danger width-sm ml-1",
                        label: "Xóa",
                        command: function () {
                            var self = this;
                            self.model.destroy({
								success: function (model, response) {
                                    self.getApp().notify('Xoá dữ liệu thành công');
                                    self.trigger("savedata", {});
                                    self.close();
								},
								error: function (xhr, status, error) {
									try {
										if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
											self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
											self.getApp().getRouter().navigate("login");
										} else {
											self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
										}
									}
									catch (err) {
										self.getApp().notify({ message: "Xóa dữ liệu không thành công" }, { type: "danger", delay: 1000 });
									}
								},
							});
                            
                        },
                        visible: function () {
                            var self = this;
                            var viewData = self.viewData;
                            return !!viewData && !!viewData.id;
                        }
                    },

                ],
            }],
        render: function () {
            var self = this;

            var id = this.getApp().getRouter().getParam("id");
            var viewData = self.viewData;
            if (viewData !== undefined && viewData !== null) {
                id = viewData.id;
            }
            if (id) {
                this.model.set('id', id);
                this.model.fetch({
                    success: function (data) {
                        self.applyBindings();
                        self.$el.find('#batbuoc').data('gonrin').setValue(self.model.get('batbuoc')+"");
                        self.$el.find('#trangthai').data('gonrin').setValue(self.model.get('trangthai')+"");
                    },
                    error: function () {
                        self.getApp().notify(self.getApp().translate("TRANSLATE:error_load_data"));
                    },
                });
            } else {
                self.applyBindings();
            }


            self.$el.find('#batbuoc').combobox({
                textField: "text",
                valueField: "value",
                dataSource: [
                    { text: "Có", value: "1" },
                    { text: "Không", value: "0" },
                ],
                value: "1"
            });

            self.$el.find('#trangthai').combobox({
                textField: "text",
                valueField: "value",
                dataSource: [
                    { text: "Hiển thị", value: "1" },
                    { text: "Ẩn", value: "0" },
                ],
                value: "1"
            });
        },

    });
})