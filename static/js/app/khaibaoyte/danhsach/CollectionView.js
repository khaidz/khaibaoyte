define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin');

    var template = require('text!app/khaibaoyte/danhsach/tpl/collection.html'),
        schema = require('json!schema/ToKhaiYTeSchema.json');
    var CustomFilterView = require('app/bases/CustomFilterView');
    var ModelDialogView = require('app/khaibaoyte/ketqua/ModelDialogView');
    return Gonrin.CollectionView.extend({
        template: template,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "tokhai_yte_search",
        dataTrieuChung: [],
        dataDichTe: [],
        filter_common: [],
        filter1: {
            "$and":{}
        },
        uiControl: {
            fields: [
                {
                    field: "ten", label: "Họ tên", width: 250, template: function (rowData) {
                        if (!!rowData && !!rowData.ten) {
                            if(rowData.ma_benh_nhan){
                                return '<span class="white" >' + rowData.ten + ' (MBN: '+ rowData.ma_benh_nhan+')</span>';
                                
                            }
                            return '<span class="white" >' + rowData.ten + '</span>';
                        }
                        return "";
                    }
                },
                {
                    field: "so_dien_thoai", label: "Số điện thoại", width: 150, template: function (rowData) {
                        if (!!rowData && !!rowData.so_dien_thoai) {
                            return '<span class="white" >' + rowData.so_dien_thoai + '</span>';
                        }
                        return "";
                    }
                },
                {
                    field: "ngay_sinh", label: "Ngày sinh", width: 180, template: function (rowData) {
                        if (!!rowData && !!rowData.ngaysinh) {
                            return gonrinApp().timestampFormat(rowData.ngaysinh * 1000, 'DD/MM/YYYY');
                        }
                        return "";
                    }
                },

                {
                    field: "gioi_tinh", label: "Giới tính", width: 150, template: function (rowData) {
                        if (!!rowData) {
                            if (rowData.gioi_tinh == 1) {
                                return '<i class="text-primary"></i> Nam';
                            } else if (rowData.gioi_tinh == 2) {
                                return '<i class="text-primary"></i> Nữ';
                            } else {
                                return '<i class="text-primary"></i> Giới tính khác';
                            }
                        }
                    }
                },
                {
                    field: "dia_chi", label: "Địa chỉ", width: 250, template: function (rowData) {
                        if (rowData !== undefined && !!rowData.dia_chi) {
                            return rowData.dia_chi;
                        }
                        return "";
                    }
                },
                {
                    field: "thoigian_khaibao", label: "Ngày khai báo", width: 180, template: function (rowData) {
                        if (rowData !== undefined && !!rowData.thoigian_khaibao) {
                            return gonrinApp().timestampFormat(rowData.thoigian_khaibao * 1000, "DD/MM/YYYY HH:mm");
                        }
                        return "";
                    }
                },
                {
                    field: "coyeuto_dichte", label: "Có yếu tố dịch tễ", width: 200, template: function (rowData) {
                        if (!!rowData) {
                            if (rowData.coyeuto_dichte == 1) {
                                return `<span class="red">Có</span>`;
                            }
                        }

                        return `<span class="text-success">Không</span>`;
                    }
                },

            ],
            onRowClick: function (event) {
                var self = this;
                var rowData = event.rowData;
                var ketquaView = new ModelDialogView({ viewData: { "id": rowData.id } });
                ketquaView.dialog({ "size": "large" });
            },
            language: {
                no_records_found: "Chưa có dữ liệu"
            },
            noResultsClass: "alert alert-default no-records-found",
            datatableClass: "table table-mobile",
            selectedTrClass: "",
            rowClass: function (rowData) {
                if (!!rowData) {
                    if (rowData.coyeuto_dichte == 1) {
                        return 'bg-yes';
                    }
                }
            }
        },
        render: function () {
            var self = this;
            if (gonrinApp().currentUser === undefined || gonrinApp().currentUser === null) {
                self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                self.getApp().getRouter().navigate("login");
                return false;
            }


            self.$el.find(".page-title").html("Danh sách khai báo y tế");

            self.filter = new CustomFilterView({
                el: self.$el.find("#grid_search")
            });
            self.filter.render();
            self.uiControl.filters = self.filter1;
            // self.uiControl.orderBy = [{ "field": "thoigian_khaibao", "direction": "desc" }];


            var filter_tungay = self.$el.find('#tungay').datetimepicker({
                textFormat: 'DD/MM/YYYY',
                extraFormats: ['DDMMYYYY'],
                format: "DD/MM/YYYY",
                widgetPositioning: { "vertical": "bottom" },
                parseInputDate: function (val) {
                    var value_date = gonrinApp().parseDate(val);
                    return value_date;
                },
                parseOutputDate: function (date) {
                    return date.unix();
                },
                disabledComponentButton: true
            });

            var filter_denngay = self.$el.find('#denngay').datetimepicker({
                textFormat: 'DD/MM/YYYY',
                extraFormats: ['DDMMYYYY'],
                format: "DD/MM/YYYY",
                widgetPositioning: { "vertical": "bottom" },
                parseInputDate: function (val) {
                    var value_date = gonrinApp().parseDate(val);
                    return value_date;
                },
                parseOutputDate: function (date) {
                    return date.unix();
                },
                disabledComponentButton: true
            });

            filter_tungay.on('change.gonrin', function (e) {
                var $col = self.getCollectionElement();
                self.getFilter();
                var filters = self.filter_common;
                $col.data('gonrin').filter(filters);
            })

            filter_denngay.on('change.gonrin', function (e) {
                var $col = self.getCollectionElement();
                self.getFilter();
                var filters = self.filter_common;
                $col.data('gonrin').filter(filters);
            })

            self.loadDataTrieuChung();
            self.loadDataDichTe();


            self.filter.on('filterChanged', function (evt) {
                var $col = self.getCollectionElement();
                var text = !!evt.data.text ? evt.data.text.trim() : "";
                if ($col) {
                    if (text !== null) {
                        self.getFilter();
                        filters = self.filter_common
                        self.uiControl.filters = filters;
                        $col.data('gonrin').filter(filters);
                    } else {
                        var filters = {
                            "$and": [
                                { "tenkhongdau": { "$likeI": "" } }
                            ]
                        }
                        self.uiControl.filters = filters;
                    }
                }
                self.applyBindings();
            });

            self.$el.find('.btn-xoaboloc').unbind('click').bind('click', function () {
                self.$el.find('#tungay').data('gonrin').setValue(null);
                self.$el.find('#denngay').data('gonrin').setValue(null);
                self.filter.model.set("text", null);
                self.$el.find("#dichte").data('gonrin').setValue([]);
                self.$el.find("#trieuchung").data('gonrin').setValue([]);
                self.getFilter();
                var filters = self.filter_common;
                var $col = self.getCollectionElement();
                $col.data('gonrin').filter(filters);
            })

            
            self.$el.find(".btn-export-excel").bind("click", function(){
                var filter = null;
                if(!Array.isArray(self.filter_common)){
                    filter = {
                        "filters": self.filter_common
                    }
                }
                var url = (self.getApp().serviceURL || "") + '/api/v1/export-tokhaiyte?'+(filter ? "&q=" + JSON.stringify(filter) : "");
                console.log(url)
                // window.location = (self.getApp().serviceURL || "") + '/api/v1/exportvoucher?id=' +id;
                window.open(url);
            });

            this.applyBindings();
        },
        getFilter: function () {
            var self = this;
            var text = !!self.filter.model.get("text") ? self.filter.model.get("text").trim() : "";

            var common = [];

            var dichte = self.$el.find('#dichte').data('gonrin').getValue();
            var trieuchung = self.$el.find('#trieuchung').data('gonrin').getValue();
            
            if(dichte.length> 0){
                common.push({"dichte":dichte});
            }

            if(trieuchung.length> 0){
                common.push({"trieuchung":trieuchung});
            }

            var tungay = self.$el.find('#tungay').data('gonrin').getValue();

            var denngay = self.$el.find('#denngay').data('gonrin').getValue();

            if (tungay != null && tungay != undefined && denngay!=null && denngay !=undefined) {
                common.push({
                    'thoigian_khaibao': { "$gte": moment(gonrinApp().parseDate(tungay)).unix(), "$lte": moment(gonrinApp().parseDate(denngay)).unix()+86399 }
                })
            }
            else if (denngay != null && denngay != undefined) {
                common.push({
                    'thoigian_khaibao': { "$lte": moment(gonrinApp().parseDate(denngay)).unix()+86399 }
                })
            }else if (tungay != null && tungay != undefined){
                common.push({
                    'thoigian_khaibao': { "$gte": moment(gonrinApp().parseDate(tungay)).unix() }
                })
            }

            if(text.trim().length > 0){
                common.push({"text": text})
            }
            

            self.filter_common = {
                "$and": common
            }

        },
        loadDataTrieuChung: function () {
            var self = this;
            var query = {
                "filters": {
                    "trangthai": { "$eq": 1 }
                },
                "order_by": [{ "field": "thutu_uutien", "direction": "desc" }, { "field": "updated_at", "direction": "asc" }]
            };
            $.ajax({
                url: self.getApp().serviceURL + '/api/v1/trieuchung?q=' + JSON.stringify(query),
                dataType: "json",
                success: function (res) {
                    var objects = res.objects;
                    self.dataTrieuChung = objects;
                    self.$el.find('#trieuchung').combobox({
                        textField: "ten",
                        valueField: "id",
                        selectionMode: 'multiple',
                        dataSource: objects
                    })
                    self.$el.find('#trieuchung').on('change.gonrin', function (e) {
                        var $col = self.getCollectionElement();
                        // var text = !!filter.model.get("text") ? filter.model.get("text").trim() : "";
                        self.getFilter();
                        var filters = self.filter_common;
                        $col.data('gonrin').filter(filters);
                    });


                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
            });
        },

        loadDataDichTe: function () {
            var self = this;
            var query = {
                "filters": {
                    "trangthai": { "$eq": 1 }
                },
                "order_by": [{ "field": "thutu_uutien", "direction": "desc" }, { "field": "updated_at", "direction": "asc" }]
            };
            $.ajax({
                url: self.getApp().serviceURL + '/api/v1/dichte?q=' + JSON.stringify(query),
                dataType: "json",
                success: function (res) {
                    var objects = res.objects;
                    self.dataDichTe = objects;
                    self.$el.find('#dichte').combobox({
                        textField: "ten",
                        valueField: "id",
                        selectionMode: 'multiple',
                        dataSource: objects
                    })
                    self.$el.find('#dichte').on('change.gonrin', function (e) {
                        var $col = self.getCollectionElement();
                        // var text = !!filter.model.get("text") ? filter.model.get("text").trim() : "";
                        self.getFilter();
                        var filters = self.filter_common;
                        $col.data('gonrin').filter(filters);
                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
            });
        },

    });

});