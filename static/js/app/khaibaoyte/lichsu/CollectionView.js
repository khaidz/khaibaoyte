define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        storejs = require('vendor/store');

    var template = require('text!app/khaibaoyte/lichsu/tpl/collection.html'),
        schema = require('json!schema/ToKhaiYTeSchema.json');
    var CustomFilterView = require('app/bases/CustomFilterView');

    return Gonrin.CollectionView.extend({
        template: template,
        modelSchema: schema,
        urlPrefix: "/api/v1/khaibaoyte/",
        collectionName: "uid",
        
        tools: [{
            name: "defaultgr",
            type: "group",
            groupClass: "toolbar-group",
            buttons: [{
                name: "create",
                type: "button",
                buttonClass: "btn-success btn-sm",
                label: "Khai báo mới",
                command: function () {
                    var self = this;
                    var path = "tokhai_yte/model";
                    this.getApp().getRouter().navigate(path);
                }
            }]
        }],

        uiControl: {
            fields: [
                {
                    field: "ten", label: "Họ tên", width: 250, template: function (rowData) {
                        if (!!rowData && !!rowData.ten) {
                            return '<span class="text-primary" >' + rowData.ten + '</span>';
                        }
                        return "";
                    }
                },
                {
                    field: "ngaysinh", label: "Ngày sinh", width: 180, template: function (rowData) {
                        if (!!rowData && !!rowData.ngaysinh) {
                            return gonrinApp().timestampFormat(rowData.ngaysinh * 1000, 'DD/MM/YYYY');
                        }
                        return "";
                    }
                },

                {
                    field: "gioi_tinh", label: "Giới tính", width: 150, template: function (rowData) {
                        if (!!rowData) {
                            if (rowData.gioi_tinh == 1) {
                                return '<i class="text-primary"></i> Nam';
                            } else if (rowData.gioi_tinh == 2) {
                                return '<i class="text-primary"></i> Nữ';
                            } else {
                                return '<i class="text-primary"></i> Giới tính khác';
                            }
                        }
                    }
                },
                {
                    field: "thoigian_khaibao", label: "Ngày khai báo", width: 250, template: function (rowData) {
                        if (rowData !== undefined && !!rowData.thoigian_khaibao) {
                            return gonrinApp().format_timestamp_toDDMMYYY(rowData.thoigian_khaibao);
                        }
                        return "";
                    }
                }

            ],
            onRowClick: function(event) {
                if (event.rowId) {
                    var path =  'tokhai_yte/ketqua?id=' + event.rowId;
                    this.getApp().getRouter().navigate(path);
                }
            },
            // onRowClick: function (event) {
            //     if (event.rowId) {
            //         var path = this.collectionName + '/ketqua?id=' + event.rowId;
            //         this.getApp().getRouter().navigate(path);
            //     }
            // },
            language: {
                no_records_found: "Chưa có dữ liệu"
            },
            noResultsClass: "alert alert-default no-records-found",
            datatableClass: "table table-mobile",
            // onRendered: function (e) {
            //     gonrinApp().responsive_table();
            // }
        },
        render: function () {
            var self = this;
            self.$el.find(".page-title").html("Lịch sử khai báo y tế")


            self.uiControl.orderBy = [{ "field": "thoigian_khaibao", "direction": "desc" }];

            var filters = {
                "$and": [
                    {"uid": storejs.get('UID-TOKHAI-YTE')}
                ]
            }
            self.uiControl.filters = filters;

            self.applyBindings();
            return this;
        },

    });

});