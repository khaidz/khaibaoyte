define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        QRCode = require('js/easy.qrcode'),
        tpl = require('text!app/khaibaoyte/ketqua/tpl/model.html'),
        schema = require('json!schema/ToKhaiYTeSchema.json');
    return Gonrin.ModelView.extend({
        template: tpl,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "tokhai_yte",
        
        type_qrcode: 1,
        tools: [
            {
                name: "defaultgr",
                type: "group",
                groupClass: "toolbar-group",
                buttons: [
                    {
                        name: "back",
                        type: "button",
                        buttonClass: "btn text-white font-weight-bold width-sm ml-1 tieptuc d-none",
                        label: "Tiếp tục khai báo",
                        command: function () {
                            var self = this;
                            self.getApp().getRouter().navigate('tokhai_yte/model');
                        }
                    },
                    {
                        name: "back",
                        type: "button",
                        buttonClass: "btn btn-secondary width-sm ml-1 quaylai d-none",
                        label: "Quay lại",
                        command: function () {
                            var self = this;
                            self.getApp().getRouter().navigate('tokhai_yte/collection')
                        }
                    }

                ],
            }],
        uiControl: {

        },
        render: function () {
            var self = this;
            var id = this.getApp().getRouter().getParam("id");
            if (id) {
                this.model.set('id', id);
                this.model.fetch({
                    success: function (data) {
                        self.applyBindings();
                        self.$el.find('.ten').text(self.model.get('ten'));
                        if (!!self.model.get('ma_benh_nhan') && self.model.get('loai_khaibao') == 1) {
                            self.$el.find('.ma_benh_nhan').html(`Mã bệnh nhân: ${self.model.get('ma_benh_nhan')}`);
                        } else if (!self.model.get('ma_benh_nhan') && self.model.get('loai_khaibao') == 1) {
                            self.$el.find('.ma_benh_nhan').html(`Người bệnh`);
                        }
                        else if (self.model.get('loai_khaibao') == 2) {
                            self.$el.find('.ma_benh_nhan').html('Người nhà');
                        } else if (self.model.get('loai_khaibao') == 3) {
                            self.$el.find('.ma_benh_nhan').html('Khai hộ');
                        }
                        else if (self.model.get('loai_khaibao') == 4) {
                            self.$el.find('.ma_benh_nhan').html('Nhân viên y tế');
                        }
                        else if (self.model.get('loai_khaibao') == 5) {
                            self.$el.find('.ma_benh_nhan').html('Khác');
                        }
                        self.$el.find('.ngay_sinh').text(self.getApp().format_timestamp_toDDMMYYY(self.model.get('ngaysinh')));

                        /** Toggle lý do đến viện / Khoa phòng */
                        if (self.model.get('loai_khaibao') == 4) {
                            self.$el.find('.lydodenvien').hide();
                        }else{
                            self.$el.find('.khoaphong').hide();
                        }

                        var gioitinh = self.model.get('gioi_tinh');
                        if (gioitinh == 1) {
                            gioitinh = 'Nam';
                        } else if (gioitinh == 2) {
                            gioitinh = 'Nữ';
                        } else {
                            gioitinh = 'Giới tính khác';
                        }
                        self.$el.find('.gioi_tinh').text(gioitinh);

                        self.$el.find('.dia_chi').text(self.model.get('dia_chi'));

                        self.$el.find('.ngay_khai_bao').text(self.getApp().timestampFormat(self.model.get('thoigian_khaibao') * 1000, "DD/MM/YYYY HH:mm"));
                       
                        var trieuchung =self.model.get('trieuchung');
                        if(trieuchung==null || trieuchung==undefined || (Array.isArray(trieuchung) && trieuchung.length==0)){
                            self.$el.find('.dauhieu').text("Không");
                        }else{
                            var text = trieuchung.map((value, index)=>{
                                return value.ten;
                            }).join(', ');
                            self.$el.find('.dauhieu').html(`<span class="red">${text}</span>`);
                        }

                        var dichte =self.model.get('dichte');
                        if(dichte==null || dichte==undefined || (Array.isArray(dichte) && dichte.length==0)){
                            self.$el.find('.dichte').text("Không");
                        }else{
                            var html = dichte.map((value, index) => {
                                var ten = value.ten;
                                var text = ten;
                                var tooLong = false;
                                if (!!ten && ten.length > 150) {
                                    text = ten.slice(0, 150) + "...";
                                    tooLong = true;
                                }
                                self.$el.find('#list-modal').after(`
                                    <div id="full-detail-modal-${value.id}" class="modal fade show" tabindex="-1" aria-labelledby="standard-modalLabel" style="display: none; padding-right: 12px;" aria-modal="true" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="standard-modalLabel">Chi tiết</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body" style="white-space: pre-line">${ten}</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light" data-dismiss="modal">Đóng</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>`);
                                return `
                                        <div class="red" style="white-space: pre-line">${text}${tooLong ? `<a class="a" data-toggle="modal" data-target="#full-detail-modal-${value.id}">Xem thêm</a>` : ' '}</div>
                                    `;
                            }).join('');

                            self.$el.find('.dichte').html(html);
                            self.$el.find('.ketqua').html(`<span class="text-important">
                            Đề nghị anh/chị đến Trung tâm Bệnh nhiệt đới để được sàng lọc và thăm khám theo quy định.
                            </span>`);
                        }
                        if(self.model.get("ghi_chu")!=null && self.model.get("ghi_chu")!==""){
                            self.$el.find('.ghichu').html(`<span class="red">Ghi chú: `+self.model.get("ghi_chu")+`</span>`);
                        }

                        self.showQrcode(self.type_qrcode);

                        self.$el.find('.type_qrcode').on('click', function () {
                            self.type_qrcode = self.type_qrcode == 1 ? 2 : 1;
                            self.showQrcode(self.type_qrcode);
                        })

                        if (self.getApp().hasRole('user') || self.getApp().hasRole('admin')) {
                            self.$el.find('.quaylai').removeClass('d-none');
                        } else {
                            self.$el.find('.tieptuc').removeClass('d-none');
                        }
                    },
                    error: function () {
                        self.getApp().notify("Get data Eror");
                    },
                });
            } else {
                self.getApp().notify({ message: "Có lỗi xảy ra. Thử lại sau" }, { type: "danger", delay: 1000 });
            }

        },

        capitalizeFirstLetter: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        },
        showQrcode: function(type=1) {
            var self = this;
            document.getElementById("qrcode").innerHTML = '';
            var qrcode = new QRCode(document.getElementById("qrcode"), {
                width: 120,
                height: 120,
                colorDark: "#000000",
                colorLight: "#ffffff",
                text: self.generateTextQrcode(type)
            });
            if(type==1){
                self.$el.find('.type_qrcode').text("QR Y tế");
            }else{
                self.$el.find('.type_qrcode').text("QR Khai báo y tế");
            }
        },
        generateTextQrcode: function (type = 1) {
            var self = this;
            if (type == 1) {
                return encodeURI(gonrinApp().serviceURL + '/#tokhai_yte/ketqua?id=' + this.getApp().getRouter().getParam("id"));
            } else {
                var ten = self.model.get('ten');
                if (!!ten) ten = self.getApp().utf8ToHex(ten);
                var gioitinh = self.model.get('gioi_tinh');
                if (!gioitinh) gioitinh = '';
                var ngaysinh = self.model.get('ngaysinh');
                if (!!ngaysinh) {
                    ngaysinh = self.getApp().format_timestamp_toDDMMYYY(ngaysinh);
                } else {
                    ngaysinh = '';
                }
                var diachi = self.model.get('dia_chi');
                if (!!diachi) {
                    diachi = self.getApp().utf8ToHex(diachi);
                } else {
                    diachi = '';
                }
                return `|${ten}|${ngaysinh}|${gioitinh}|${diachi}|||||||`;

            }


            /*
#     * Theo thứ tự là: số thẻ 0| họ tên 1| ngày sinh 2| giới tinh 3| địa chỉ 4| mã
#     * tỉnh/thành phố và mã cskcb của bệnh viện đăng ký ban đầu 5| giá trị sử dụng từ 6
#     * | bỏ 7| ngày ký 8| bỏ 9| bỏ 10| ngày bắt đầu (liên tục 5 năm từ) 11
#     */
            // DN4010112176796|
            // c490e1bab76e672056c4836e204e616d|
            // 12/12/1989|
            // 1|
            // 5068c6b0e1bb9d6e672044c6b0c6a16e67204ee1bb99692c205175e1baad6e2048c3a020c490c3b46e672c205468c3a06e68207068e1bb912048c3a0204ee1bb99695f43c3944e4720545920544e4848204e474849c38a4e2043e1bba8552c205048c3815420545249e1bb824e2056c38020c490e1baa6552054c6af2043c3944e47204e4748e1bb862043414f2048c380204ee1bb9849|
            // 01 - 028|
            // 01/11/2019|
            // -|11/11/2019
            // |01050112176796|-
            // |4|
            //  01/11/2024|
            //  4cd72d1dbd8512ea-7102|$

        }

    });
})