define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        tpl = require('text!app/quanlynguoidung/tpl/model.html'),
        schema = require('json!schema/UserSchema.json');
    var AttachFileVIew = require("app/view/AttachFile/AttachFileVIew");
    return Gonrin.ModelView.extend({
        template: tpl,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "user",
        tools: [
            {
                name: "defaultgr",
                type: "group",
                groupClass: "toolbar-group",
                buttons: [
                    {
						name: "back",
						type: "button",
						buttonClass: "btn btn-secondary width-sm",
						label: "TRANSLATE:BACK",
						command: function(){
							var self = this;
							Backbone.history.history.back();
						},
						visible:function(){
                            // return gonrinApp().currentUser.hasRole("Admin") || gonrinApp().currentUser.hasRole("CoSoKCB");
                            return true;
						}
					},
                    {
                        name: "save",
                        type: "button",
                        buttonClass: "btn btn-primary width-sm ml-1",
                        label: "Lưu",
                        command: function () {
                            var self = this;
                            var validate = self.validate();
                            if (validate === false) {
                                return;
                            }
                            self.model.set('roles',self.$el.find("#roles").data('gonrin').getValue());
                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Lưu thông tin thành công");
                                    if(self.getApp().hasRole('admin')){
                                        var path = "quan_ly_nguoi_dung/collection";
                                        self.getApp().getRouter().navigate(path);
                                    }
                                                                    },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                            self.getApp().getRouter().navigate("login");
                                        } else if (($.parseJSON(error.xhr.responseText).error_code) === "ERROR_PERMISSION") {
                                            self.getApp().notify({ message: "Bạn không có quyền thực hiện tác vụ này" }, { type: "danger", delay: 1000 });
                                        } 
                                        
                                        else {
                                            self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                                        }
                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        },
                    },
                    {
                        name : "khoataikhoan",
                        type : "button",
                        buttonClass : "btn btn-danger with-sm ml-1 d-none btn-khoa-tai-khoan",
                        label : "Khóa",
                        visible: function(){
                            return gonrinApp().hasRole('admin');
                        },
                        command : function(){
                            var self =this;
                            var validate = self.validate();
                            if (validate === false) {
                                return;
                            }
                            self.model.set('active', 0);
                            self.model.set('roles',self.$el.find("#roles").data('gonrin').getValue());
                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Tài khoản đã bị khóa");
                                    self.getApp().getRouter().refresh();
                                },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                            self.getApp().getRouter().navigate("login");
                                        } else {
                                            self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                                        }
                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        }
                    },
                    {
                        name : "motaikhoan",
                        type : "button",
                        buttonClass : "btn btn-success btn-sm ml-1 d-none btn-mo-tai-khoan",
                        label : "Mở khóa",
                        visible: function(){
                            return gonrinApp().hasRole('admin');
                        },
                        command : function(){
                            var self =this;
                            var validate = self.validate();
                            if (validate === false) {
                                return;
                            }
                            self.model.set('active', 1);
                            self.model.set('roles',self.$el.find("#roles").data('gonrin').getValue());
                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Tài khoản đã được kích hoạt");
                                    self.getApp().getRouter().refresh();
                                },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                            self.getApp().getRouter().navigate("login");
                                        } else {
                                            self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                                        }
                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        }
                    }
                    
                ],
            }],
        render: function () {
            var self = this;
            
            var id = self.getApp().getRouter().getParam("id");
            if (id) {
                this.model.set('id', id);
                this.model.fetch({
                    success: function (data) {
                        self.applyBindings();
                        if(self.model.get('active')==1){
                            self.$el.find('.btn-khoa-tai-khoan').removeClass('d-none');
                        }else{
                            self.$el.find('.btn-mo-tai-khoan').removeClass('d-none');
                        }
                        var roles = self.model.get('roles');
                        if(!!roles && Array.isArray(roles)){
                            roles.map((value, index)=>{
                                if(value.name=='user'){
                                    self.$el.find("#roles").data('gonrin').setValue("user");
                                }else{
                                    self.$el.find("#roles").data('gonrin').setValue("admin");
                                }
                            })
                        }
                        
                    },
                    error: function () {
                        self.getApp().notify(self.getApp().translate("TRANSLATE:error_load_data"));
                    },
                });
                self.$el.find('.no_id').html(`Mật khẩu mới:`);
                
            } else {
                self.applyBindings();
            }
            if(self.getApp().hasRole('admin')){
                self.$el.find('.vaitro').removeClass('d-none');
            }

            self.$el.find('#roles').combobox({
                textField: "value",
                valueField: "name",
                dataSource: [
                    { name: "user", value: "User" },
                    { name: "admin", value: "Admin" },
                ],
            });
            self.$el.find('#roles').on('change.gonrin', function(e){
            });
        },
        
        validate: function() {
            var self = this;
            var phone = self.model.get('phone');
            var email = self.model.get('email');
            var fullname = self.model.get('fullname');
            var id = self.model.get("id");
            var password = self.model.get("password");
            
            if (phone == null || phone == '' || phone == undefined) {
                self.getApp().notify({ message: "Số điện thoại không được bỏ trống" }, { type: "danger", delay: 1000 });
                return false;
            }
            if (!self.getApp().validatePhone(phone)) {
                self.getApp().notify({ message: "Số điện thoại không hợp lệ" }, { type: "danger", delay: 1000 });
                return false;
            }
            if (email != null && email != '' && !self.getApp().validateEmail(email)) {
                self.getApp().notify({ message: "Email không hợp lệ" }, { type: "danger", delay: 1000 });
                return false;
            }
            if (!id && !password) {
                self.getApp().notify({message: "Vui lòng nhập mật khẩu"}, {type: 'danger', delay: 1000});
                return false;
            }
            if(!self.$el.find("#roles").data('gonrin').getValue()){
                self.getApp().notify({message: "Chưa chọn vai trò"}, {type: 'danger', delay: 1000});
                return false;
            }
            
            return true;
        }

    });
})