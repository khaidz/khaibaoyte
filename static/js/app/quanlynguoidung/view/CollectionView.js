define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin');

    var template = require('text!app/quanlynguoidung/tpl/collection.html'),
        schema = require('json!schema/UserSchema.json');
    var CustomFilterView = require('app/bases/CustomFilterView');
    return Gonrin.CollectionView.extend({
        template: template,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "user",
        datatableClass: "table table-hover",
        tools: [{
            name: "defaultgr",
            type: "group",
            groupClass: "toolbar-group",
            buttons: [{
                name: "create",
                type: "button",
                buttonClass: "btn-success btn-sm",
                label: "TRANSLATE:CREATE",
                command: function () {
                    var self = this;
                    var path = "quan_ly_nguoi_dung/model";
                    this.getApp().getRouter().navigate(path);
                }
            }]
        }],
        uiControl: {
            orderBy: [
                { field: "id", direction: "asc" }
            ],
            fields: [
                { field: "fullname", label: "Họ và tên" },
                { field: "email", label: "Email" },
                { field: "phone", label: "Số điện thoại" },

                {
                    field: "active",
                    label: "Trạng thái",
                    template: (rowData) => {
                        if (rowData.active == 0) {
                            return '<div class="text-danger">Đang bị khóa</div>';
                        } else if (rowData.active == 1) {
                            return '<div class="text-success">Đang hoạt động</div>';
                        }
                        return '';
                    }
                },

            ],
            onRowClick: function (event) {
                var self = this;
                if (event.rowId) {
                    var path = "quan_ly_nguoi_dung/model?id=" + event.rowId;
                    self.getApp().getRouter().navigate(path);
                }
            },
            datatableClass: "table table-mobile",
            // onRendered: function(e) {
            //     gonrinApp().responsive_table();
            // }
        },
        render: function () {
            var self = this;
            self.query = null;
            var filter = new CustomFilterView({
                el: self.$el.find("#grid_search"),
                sessionKey: self.collectionName + "_filter"
            });
            filter.render();
            self.$el.find("#grid_search input").val("");
            self.$el.find("#grid_search input").attr({ "placeholder": "Nhập nội dung tìm kiếm" });
            var filter_query = self.uiControl.filters;
            if (filter_query !== undefined && filter_query !== null && filter_query !== false) {
                self.query = filter_query;
            }
            filter.model.set("text", "");

            // self.uiControl.orderBy = [{ "field": "id", "direction": "desc" }];
            if (!filter.isEmptyFilter()) {
                var text = !!filter.model.get("text") ? filter.model.get("text").trim() : "";
                var filters = {
                    "$or": [
                        { "unsigned_name": { "$likeI": self.getApp().convert_khongdau(text) } },
                        { "email": { "$likeI": text } },
                        { "phone": { "$likeI": text } }
                    ]
                };
                var filter1;
                if (self.query !== null && self.query !== undefined) {
                    filter1 = {
                        "$and": [
                            filters, self.query
                        ]
                    };
                } else {
                    filter1 = filters;
                }
                self.uiControl.filters = filter1;
            }
            self.applyBindings();

            filter.on('filterChanged', function (evt) {
                var $col = self.getCollectionElement();
                var text = !!evt.data.text ? evt.data.text.trim() : "";
                if ($col) {
                    if (text !== null) {
                        var filters = {
                            "$or": [
                                { "unsigned_name": { "$likeI": self.getApp().convert_khongdau(text) } },
                                { "email": { "$likeI": text } },
                                { "phone": { "$likeI": text } }
                            ]
                        };
                        var filter1;
                        if (self.query !== null && self.query !== undefined) {
                            filter1 = {
                                "$and": [
                                    filters, self.query
                                ]
                            };
                        } else {
                            filter1 = filters;
                        }
                        $col.data('gonrin').filter(filter1);
                    }
                }
                self.applyBindings();
            });
            self.$el.find("table").addClass("table-hover");
            self.$el.find("table").removeClass("table-striped");

            return this;
        },
    });

});