define(function (require) {
    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        tpl = require('text!app/thongtincanhan/tpl/model.html'),
        schema = require('json!schema/ProfileUserSchema.json');
    var AttachFileVIew = require("app/view/AttachFile/AttachFileVIew");
    return Gonrin.ModelView.extend({
        template: tpl,
        modelSchema: schema,
        urlPrefix: "/api/v1/",
        collectionName: "profile",
        tools: [
            {
                name: "defaultgr",
                type: "group",
                groupClass: "toolbar-group",
                buttons: [
                    {
                        name: "save",
                        type: "button",
                        buttonClass: "btn btn-dark text-white px-4 py-2 font-weight-bold mr-2",
                        label: "TRỞ VỀ",
                        command: function(){
                            var self = this;
                            self.getApp().getRouter().navigate('chungchihanhnghe/model');
                        }
                    },
                    {
                        name: "save",
                        type: "button",
                        buttonClass: "btn btn-primary text-white px-4 py-2 font-weight-bold",
                        label: "LƯU THÔNG TIN",
                        command: function () {
                            var self = this;
                            var phone = self.model.get('dienthoai');
                            var email = self.model.get('email');
                            var ho = self.model.get('ho');
                            var ten = self.model.get('ten');
                            var ngaysinh = self.model.get('ngaysinh');
                            var gioitinh = self.model.get('gioitinh');
                            var macongdan = self.model.get('macongdan');

                            if(!self.model.get('avatar_url')){
                                self.getApp().notify({ message: "Chưa cập nhật avatar" }, { type: "danger", delay: 1000 });
                                return false;
                            }

                            if (phone == null || phone == '' || phone == undefined) {
                                self.getApp().notify({ message: "Số điện thoại không được bỏ trống" }, { type: "danger", delay: 1000 });
                                return false;
                            }
                            if (!self.getApp().validatePhone(phone)) {
                                self.getApp().notify({ message: "Số điện thoại không hợp lệ" }, { type: "danger", delay: 1000 });
                                return false;
                            }
                            if (email != null && email != '' && !self.getApp().validateEmail(email)) {
                                self.getApp().notify({ message: "Email không hợp lệ" }, { type: "danger", delay: 1000 });
                                return false;
                            }
                            if (ho === null || ho === '' || ho === undefined ||
                                ten === null || ten === '' || ten === undefined ||
                                macongdan === null || macongdan === '' || macongdan === undefined ||
                                ngaysinh===null || gioitinh===null
                            ) {
                                self.getApp().notify({ message: "Vui lòng nhập đầy đủ thông tin" }, { type: "danger", delay: 1000 });
                                return false;
                            }
                            self.model.save(null, {
                                success: function (model, respose, options) {
                                    self.getApp().notify("Lưu thông tin thành công");
                                    self.getApp().currentUser = respose;
                                    // self.getApp().getRouter().refresh();

                                },
                                error: function (xhr, status, error) {
                                    self.getApp().hideloading();
                                    try {
                                        if (($.parseJSON(error.xhr.responseText).error_code) === "SESSION_EXPIRED") {
                                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                                            self.getApp().getRouter().navigate("login");
                                        } else {
                                            self.getApp().notify({ message: $.parseJSON(error.xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                                        }
                                    }
                                    catch (err) {
                                        self.getApp().notify({ message: "Lưu thông tin không thành công" }, { type: "danger", delay: 1000 });
                                    }
                                }
                            });
                        }
                    }
                ],
            }],
        uiControl: {
            fields: [
                {
                    field: "ngaysinh",
                    uicontrol: "datetimepicker",
                    format: "DD/MM/YYYY",
                    textFormat: "DD/MM/YYYY",
                    extraFormats: ["DDMMYYYY"],
                    parseInputDate: function (val) {
                        return gonrinApp().parseDate(val);
                    },
                    parseOutputDate: function (date) {
                        return date.unix();
                    },
                    disabledComponentButton: true
                },
                {
                    field: "ngaycap",
                    uicontrol: "datetimepicker",
                    format: "DD/MM/YYYY",
                    textFormat: "DD/MM/YYYY",
                    extraFormats: ["DDMMYYYY"],
                    parseInputDate: function (val) {
                        return gonrinApp().parseDate(val);
                    },
                    parseOutputDate: function (date) {
                        return date.unix();
                    },
                    disabledComponentButton: true
                },

                {
                    field: "gioitinh",
                    uicontrol: "combobox",
                    textField: "text",
                    valueField: "value",
                    cssClass: "form-control",
                    dataSource: [
                        { value: 1, text: "Nam" },
                        { value: 2, text: "Nữ" },
                        { value: 3, text: "Giới tính khác" },
                    ],
                },
                {
                    field: "tinhthanh_thuongtru",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "tinhthanh_thuongtru_id"
                },
                {
                    field: "quanhuyen_thuongtru",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "quanhuyen_thuongtru_id"
                },
                {
                    field: "xaphuong_thuongtru",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "xaphuong_thuongtru_id"
                },
                {
                    field: "tinhthanh",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "tinhthanh_id"
                },
                {
                    field: "quanhuyen",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "quanhuyen_id"
                },
                {
                    field: "xaphuong",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "xaphuong_id"
                },
                {
                    field: "vanbang",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "ma_van_bang"
                },
                {
                    field: "noi_tot_nghiep",
                    uicontrol: "ref",
                    textField: "ten",
                    foreignRemoteField: "id",
                    foreignField: "ma_noi_tot_nghiep"
                }

            ]
        },
        render: function () {
            var self = this;
            self.register_attach_file('btn-change-avatar');
            var $selectize1 = self.$el.find('#selectize-tinhthanh-thuongtru').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {

                    var url = (self.getApp().serviceURL || "") + '/api/v1/tinhthanh?page=1&results_per_page=100';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize1[0].selectize.setValue(self.model.get('tinhthanh_thuongtru_id'));
                        }
                    });
                },
                onChange: function (value) {

                    if (value != self.model.get('tinhthanh_thuongtru_id')) {
                        var tinhthanh_thuongtru = $selectize1[0].selectize.options[value];
                        self.model.set('tinhthanh_thuongtru_id', value)
                        self.model.set('tinhthanh_thuongtru', tinhthanh_thuongtru)
                        self.model.set('quanhuyen_thuongtru_id', null)
                        self.model.set('quanhuyen_thuongtru', null)
                        self.model.set('xaphuong_thuongtru_id', null)
                        self.model.set('xaphuong_thuongtru', null)
                        $selectize2[0].selectize.setValue('');
                        $selectize3[0].selectize.setValue('');
                        $.ajax({
                            url: (self.getApp().serviceURL || "") + '/api/v1/quanhuyen?page=1&results_per_page=100&q={"filters":{"tinhthanh_id":{"$eq":"' + self.model.get('tinhthanh_thuongtru_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}',
                            method: 'get',
                            success: function (res) {
                                $selectize2[0].selectize.clearOptions();
                                $selectize2[0].selectize.addOption(res.objects);
                            }
                        })
                    }

                }
            });

            var $selectize2 = self.$el.find('#selectize-quanhuyen-thuongtru').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/quanhuyen?page=1&results_per_page=100&q={"filters":{"tinhthanh_id":{"$eq":"' + self.model.get('tinhthanh_thuongtru_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize2[0].selectize.setValue(self.model.get('quanhuyen_thuongtru_id'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('quanhuyen_thuongtru_id') !== value) {
                        var quanhuyen_thuongtru = $selectize2[0].selectize.options[value];
                        self.model.set('quanhuyen_thuongtru_id', value)
                        self.model.set('quanhuyen_thuongtru', quanhuyen_thuongtru)
                        self.model.set('xaphuong_thuongtru_id', null)
                        self.model.set('xaphuong_thuongtru', null)
                        $selectize3[0].selectize.setValue('');
                        $.ajax({
                            url: (self.getApp().serviceURL || "") + '/api/v1/xaphuong?page=1&results_per_page=100&q={"filters":{"quanhuyen_id":{"$eq":"' + self.model.get('quanhuyen_thuongtru_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}',
                            method: 'get',
                            success: function (res) {
                                $selectize3[0].selectize.clearOptions();
                                $selectize3[0].selectize.addOption(res.objects);
                            }
                        })
                    }
                }
            });

            var $selectize3 = self.$el.find('#selectize-xaphuong-thuongtru').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/xaphuong?page=1&results_per_page=100&q={"filters":{"quanhuyen_id":{"$eq":"' + self.model.get('quanhuyen_thuongtru_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize3[0].selectize.setValue(self.model.get('xaphuong_thuongtru_id'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('xaphuong_thuongtru_id') !== value) {
                        var xaphuong_thuongtru = $selectize3[0].selectize.options[value];
                        self.model.set('xaphuong_thuongtru_id', value)
                        self.model.set('xaphuong_thuongtru', xaphuong_thuongtru)
                    }

                }
            });

            var $selectize4 = self.$el.find('#selectize-tinhthanh').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/tinhthanh?page=1&results_per_page=100';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize4[0].selectize.setValue(self.model.get('tinhthanh_id'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('tinhthanh_id') !== value) {
                        var tinhthanh = $selectize4[0].selectize.options[value];
                        if (tinhthanh) delete tinhthanh.$order;
                        self.model.set('tinhthanh_id', value)
                        self.model.set('tinhthanh', tinhthanh)
                        self.model.set('quanhuyen_id', null)
                        self.model.set('quanhuyen', null)
                        self.model.set('xaphuong_id', null)
                        self.model.set('xaphuong', null)
                        $selectize5[0].selectize.setValue('');
                        $selectize6[0].selectize.setValue('');
                        $.ajax({
                            url: (self.getApp().serviceURL || "") + '/api/v1/quanhuyen?page=1&results_per_page=100&q={"filters":{"tinhthanh_id":{"$eq":"' + self.model.get('tinhthanh_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}',
                            method: 'get',
                            success: function (res) {
                                $selectize5[0].selectize.clearOptions();
                                $selectize5[0].selectize.addOption(res.objects);
                            }
                        })
                    }
                }
            });

            var $selectize5 = self.$el.find('#selectize-quanhuyen').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/quanhuyen?page=1&results_per_page=100&q={"filters":{"tinhthanh_id":{"$eq":"' + self.model.get('tinhthanh_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize5[0].selectize.setValue(self.model.get('quanhuyen_id'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('quanhuyen_id') !== value) {
                        var quanhuyen = $selectize5[0].selectize.options[value];
                        if (quanhuyen) delete quanhuyen.$order;
                        self.model.set('quanhuyen_id', value)
                        self.model.set('quanhuyen', quanhuyen)
                        self.model.set('xaphuong_id', null)
                        self.model.set('xaphuong', null)
                        $selectize6[0].selectize.setValue('');
                        $.ajax({
                            url: (self.getApp().serviceURL || "") + '/api/v1/xaphuong?page=1&results_per_page=100&q={"filters":{"quanhuyen_id":{"$eq":"' + self.model.get('quanhuyen_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}',
                            method: 'get',
                            success: function (res) {
                                $selectize6[0].selectize.clearOptions();
                                $selectize6[0].selectize.addOption(res.objects);
                            }
                        })
                    }
                }
            });

            var $selectize6 = self.$el.find('#selectize-xaphuong').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/xaphuong?page=1&results_per_page=100&q={"filters":{"quanhuyen_id":{"$eq":"' + self.model.get('quanhuyen_id') + '"}},"order_by":[{"field":"ma","direction":"asc"}]}';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize6[0].selectize.setValue(self.model.get('xaphuong_id'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('xaphuong_id') != value) {
                        var xaphuong = $selectize6[0].selectize.options[value];
                        if (xaphuong) delete xaphuong.$order;
                        self.model.set('xaphuong_id', value)
                        self.model.set('xaphuong', xaphuong)
                    }
                }
            });

            var $selectize7 = self.$el.find('#selectize-vanbang').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/vanbang_chuyenmon?page=1&results_per_page=100';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize7[0].selectize.setValue(self.model.get('ma_van_bang'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('ma_van_bang') != value) {
                        var vanbang = $selectize7[0].selectize.options[value];
                        if (vanbang) delete vanbang.$order;

                        self.model.set('ma_van_bang', value)
                        self.model.set('ten_van_bang', vanbang.ten);
                        self.model.set('vanbang', vanbang)
                    }
                }
            });

            var $selectize8 = self.$el.find('#selectize-noitotnghiep').selectize({
                maxItems: 1,
                valueField: 'id',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/noitotnghiep?page=1&results_per_page=100';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            callback(res.objects);
                            $selectize8[0].selectize.setValue(self.model.get('ma_noi_tot_nghiep'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('ma_noi_tot_nghiep') != value) {
                        var noitotnghiep = $selectize8[0].selectize.options[value];
                        if (noitotnghiep) delete noitotnghiep.$order;

                        self.model.set('ma_noi_tot_nghiep', value)
                        self.model.set('noi_tot_nghiep', noitotnghiep);
                    }
                }
            });

            var $selectize9 = self.$el.find('#selectize-tinhthanh-capcmnd').selectize({
                maxItems: 1,
                valueField: 'ten',
                labelField: 'ten',
                searchField: ['ten', 'tenkhongdau'],
                preload: true,
                sortField: {
                    field: 'ten',
                    direction: 'asc'
                },
                load: function (query, callback) {
                    var url = (self.getApp().serviceURL || "") + '/api/v1/tinhthanh?page=1&results_per_page=100';
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        error: function () {
                            callback();
                        },
                        success: function (res) {
                            var obj = res.objects;
                            obj.push({ten: "Cục cảnh sát ĐKQL cư trú và QLQG về dân cư"})
                            obj.push({ten: "Cục trưởng Cục Cảnh sát quản lý hành chính về trật tự xã hội"})
                            // obj.addOption({value: "123", text: "123"})
                            callback(obj);
                            // callback({value: "12", text: "123"})
                            $selectize9[0].selectize.setValue(self.model.get('noicap'));
                        }
                    });
                },
                onChange: function (value) {
                    if (self.model.get('noicap') != value) {
                        self.model.set('noicap', value);
                    } 
                }
            });


            var current_user = gonrinApp().currentUser;
            var id = null;
            if (current_user != null && current_user != undefined) {
                id = current_user.id;
            }
            if (id) {
                this.model.set('id', id);
                this.model.fetch({
                    success: function (data) {
                        self.applyBindings();
                        if (!!self.model.get('avatar_url')) {
                            self.$el.find('#avatar-img').attr('src', self.getApp().check_image(self.model.get('avatar_url')));
                        } else {
                            self.$el.find('#avatar-img').attr('src', self.getApp().staticURL + 'images_template/default-forum-user.png');
                        }
                    },
                    error: function () {
                        self.getApp().notify(self.getApp().translate("TRANSLATE:error_load_data"));
                    },
                });
            } else {
                self.applyBindings();
            }
        },
        register_attach_file: function (class_file = null) {
            var self = this;
            self.$el.find("." + class_file).unbind("click").bind("click", function () {
                var attachImage = new AttachFileVIew();
                attachImage.render();
                attachImage.on("success", (event) => {
                    var file = event.data;
                    var file_type = file.type;
                    if (!!file && !!file_type && (file_type.toLowerCase().includes("jpeg") || file_type.toLowerCase().includes("jpg") || file_type.toLowerCase().includes("png"))) {
                        self.getApp().notify("Tải file thành công!");
                        self.model.set('avatar_url', file.link);

                        self.$el.find('#avatar-img').attr('src', self.getApp().check_image(self.model.get('avatar_url')));
                    }
                });
            });
        },

    });
})