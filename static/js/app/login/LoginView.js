define(function (require) {

    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        storejs = require('vendor/store'),

        tpl = require('text!app/login/tpl/login.html'),
        tpl_mobile = require('text!app/login/tpl/login_mobile.html'),
        template = _.template(tpl);
    var template_mobile = _.template(tpl_mobile);
    return Gonrin.View.extend({
        render: function () {
            var self = this;
            storejs.set('X-USER-TOKEN', '');
            if (!!gonrinApp().currentUser) {
                gonrinApp().currentUser = null;
            }

            this.$el.html(template());
            $('.account-pages').addClass('bg-gradient min-vh-100');
            this.$el.find(".btn-login").unbind("click").bind("click", function () {
                gonrinApp().check_permission_notify();
                self.processLogin();
                return false;
            });

            self.$el.find("[data-password]").on('click', function () {
                if ($(this).attr('data-password') == "false") {
                    $(this).siblings("input").attr("type", "text");
                    $(this).attr('data-password', 'true');
                    $(this).addClass("show-password");
                } else {
                    $(this).siblings("input").attr("type", "password");
                    $(this).attr('data-password', 'false');
                    $(this).removeClass("show-password");
                }
            });
            
            self.$el.find(".input-login").unbind("keyup").bind("keyup", function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    self.$el.find(".btn-login").click();
                }
            });
            return this;
        },
        processLogin: function () {
            var self = this;
            var username = this.$('#username').val();
            var password = this.$('#password').val();
            // var qrcode = this.getApp().getRouter().getParam("qr");
            if (username === undefined || username === "" || password === undefined || password === "") {
                self.getApp().notify({ message: "Vui lòng nhập tài khoản và mật khẩu" }, { type: "danger", delay: 2000 });

                return;
            } else {
                username = username.toLowerCase();
            }
            var data = JSON.stringify({
                data: username,
                password: password
            });

            self.getApp().showloading();
            var url_login = self.getApp().serviceURL + '/api/v1/login';
            $.ajax({
                url: url_login,
                type: 'POST',
                data: data,
                headers: {
                    'content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                beforeSend: function () {
                    $("#loading").removeClass("d-none");
                },
                dataType: 'json',
                success: function (data) {
                    self.getApp().hideloading();
                    if (data.active === 0 || data.active === '0') {
                        self.getApp().notify({ message: "Tài khoản của bạn chưa kích hoạt" }, { type: "danger", delay: 5000 });
                        gonrinApp().getRouter().navigate('login');

                    } else if (data.active === 1 || data.active === '1') {
                        $.ajaxSetup({
                            headers: {
                                'X-USER-TOKEN': data.token
                            }
                        });
                        storejs.set('X-USER-TOKEN', data.token);
                        self.getApp().postLogin(data);
                        self.getApp().notify("Đăng nhập thành công.");
                    } else {
                        self.getApp().notify({ message: "Tài khoản của bạn đã bị khóa" }, { type: "danger", delay: 5000 });
                        self.$el.find('[name=username]').val("");
                        self.$el.find('[name=password]').val("");
                    }

                },
                error: function (xhr, status, error) {
                    try {
                        if (($.parseJSON(xhr.responseText).error_code) === "SESSION_EXPIRED") {
                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                            self.getApp().getRouter().navigate("login");
                        } else {
                            self.getApp().notify({ message: $.parseJSON(xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                        }
                    } catch (err) {
                        self.getApp().notify({ message: "Có lỗi xảy ra, vui lòng thử lại sau" }, { type: "danger", delay: 1000 });
                    }
                },
                complete: function () {
                    self.getApp().hideloading();
                    return false;
                }
            });
        },

    });

});