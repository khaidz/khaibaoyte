define(function (require) {

    "use strict";
    var $                   = require('jquery'),
        _                   = require('underscore'),
        Gonrin            	= require('gonrin'),
//        storejs				= require('store'),
        tpl                 = require('text!app/bases/tpl/forgotpassword.html'),
        template = _.template(tpl);

    return Gonrin.View.extend({
        render: function () {
            var self = this;
            this.$el.html(template());
            $('.account-pages').addClass('bg-gradient min-vh-100');
            $('#contaner-template').add('.account-pages').addClass('h-100');
            this.$el.find("#button-forgot-password").unbind("click").bind("click", function(){
                self.processForgotPass();
            });
           
            
          
            return this;
        },

       	processForgotPass: function(){
            var self = this;
            var phone = this.$('#phone').val();
            if(phone ===undefined || phone ===""){
                self.getApp().notify("Yêu cầu nhập số điện thoại");
                return false;
            }
       		var data = JSON.stringify({
                   phone: phone,
       		});
       		$.ajax({
       		    url: (self.getApp().serviceURL || "") + '/api/resetpw',
       		    type: 'post',
       		    data: data,
	       		headers: {
	    		    	'content-type': 'application/json'
	    		    },
       		    dataType: 'json',
       		    success: function (data) {
                   
                    gonrinApp().notify("Gửi mã xác nhận thành công, vui lòng kiểm tra tin nhắn điện thoại");
                    
       		    	self.getApp().getRouter().navigate("confirm-changepass?uid="+data.id);
       		    },
       		    error: function(xhr, status, error) {
                    try {
                        if (($.parseJSON(xhr.responseText).error_code) === "SESSION_EXPIRED") {
                            self.getApp().notify("Hết phiên làm việc, vui lòng đăng nhập lại!");
                            self.getApp().getRouter().navigate("login");
                        } else {
                            self.getApp().notify({ message: $.parseJSON(xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                        }
                    } catch (err) {
                        self.getApp().notify({ message: "Có lỗi xảy ra, vui lòng thử lại sau" }, { type: "danger", delay: 1000 });
                    }
                }
       		});
       	},

    });

});