define(function(require) {
    "use strict";
    return [{
            "text": "Quản lý Tin tức",
            "icon": "fa fa-newspaper",
            "type": "category",
            "visible": function() {
                return false;
                // return (this.userHasRole("admin"));
            },
            "entries": [{
                    "text": "Quản lý chuyên mục",
                    "type": "view",
                    "icon": "mdi mdi-book-open",
                    "collectionName": "category",
                    "route": "categoryadmin/collection",
                    "$ref": "app/view/CategoryPost/CategoryAdminCollectionView",
                    "visible": function() {
                        return (this.userHasRole("admin"));
                    },
                },
                {
                    "text": "Quản lý Bài viết",
                    "type": "view",
                    "icon": "mdi mdi-billboard",
                    "collectionName": "post",
                    "route": "postadmin/collection",
                    "$ref": "app/view/CategoryPost/PostAdminCollectionView",
                    "visible": function() {
                        return (this.userHasRole("editor") || this.userHasRole("admin") || this.userHasRole("manager"));
                    },
                },
            ]
        }, 
       
        {
            "text": "Danh Mục",
            "icon": "ri-book-line",
            "type": "category",
            "visible": function() {
                return this.userHasRole("admin");
            },
            "entries": [
                {
                    "text": "Danh mục cơ sở y tế",
                    "type": "view",
                    "collectionName": "cosoyte",
                    "route": "cosoyte/collection",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "text": "Dân Tộc",
                    "type": "view",
                    "collectionName": "dantoc",
                    "route": "dantoc/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/DanToc/CollectionView",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type": "view",
                    "collectionName": "dantoc",
                    "route": "dantoc/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/DanToc/ModelView",
                    "visible": false
                },
                {
                    "text": "Quốc Gia",
                    "type": "view",
                    "collectionName": "quocgia",
                    "route": "quocgia/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/QuocGia/CollectionView",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type": "view",
                    "collectionName": "quocgia",
                    "route": "quocgia/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/QuocGia/ModelView",
                    "visible": false
                },
                {
                    "text": "Tỉnh Thành",
                    "type": "view",
                    "collectionName": "tinhthanh",
                    "route": "tinhthanh/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/TinhThanh/CollectionView",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type": "view",
                    "collectionName": "tinhthanh",
                    "route": "tinhthanh/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/TinhThanh/ModelView",
                    "visible": false
                },
                {
                    "text": "Quận Huyện",
                    "type": "view",
                    "collectionName": "quanhuyen",
                    "route": "quanhuyen/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/QuanHuyen/CollectionView",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type": "view",
                    "collectionName": "quanhuyen",
                    "route": "quanhuyen/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/QuanHuyen/ModelView",
                    "visible": false
                },
                {
                    "text": "Xã Phường",
                    "type": "view",
                    "collectionName": "xaphuong",
                    "route": "xaphuong/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/XaPhuong/CollectionView",
                    "visible": function() {
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type": "view",
                    "collectionName": "xaphuong",
                    "route": "xaphuong/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/XaPhuong/ModelView",
                    "visible": false
                },
                {
                    "text":"Tuyến đơn vị",
                    "type":"view",
                    "collectionName":"tuyendonvi",
                    "route":"tuyendonvi/collection",
                    "$ref": "app/danhmuc/danhmuc_dungchung/TuyenDonVi/CollectionView",
                    "visible": function(){
                        return this.userHasRole("admin");
                    }
                },
                {
                    "type":"view",
                    "collectionName":"tuyendonvi",
                    "route":"tuyendonvi/model",
                    "$ref": "app/danhmuc/danhmuc_dungchung/TuyenDonVi/ModelView",
                    "visible":  false
                },
            ]
        },
        {
            "text": "Quản lý lịch đặt hẹn",
            "icon": "fa fa-clock",
            "type": "view",
            "collectionName": "healthrecord",
            "route": "",
        },
        {
            "text": "Báo cáo - thống kê",
            "icon": "mdi mdi-view-dashboard",
            "type": "view",
            "collectionName": "healthrecord",
            "route": "",
        },
    ];

});