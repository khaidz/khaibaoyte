define(function(require) {
    "use strict";
    return [
        {
            "text": "Dân Tộc",
            "type": "view",
            "collectionName": "dantoc",
            "route": "dantoc/collection",
            "$ref": "app/danhmuc/danhmuc_dungchung/DanToc/CollectionView",
            "visible": function() {
                return this.userHasRole("admin");
            }
        },
        {
            "type": "view",
            "collectionName": "dantoc",
            "route": "dantoc/model",
            "$ref": "app/danhmuc/danhmuc_dungchung/DanToc/ModelView",
            "visible": false
        },
        {
            "text": "Quốc Gia",
            "type": "view",
            "collectionName": "quocgia",
            "route": "quocgia/collection",
            "$ref": "app/danhmuc/danhmuc_dungchung/QuocGia/CollectionView",
        },
        {
            "type": "view",
            "collectionName": "quocgia",
            "route": "quocgia/model",
            "$ref": "app/danhmuc/danhmuc_dungchung/QuocGia/ModelView",
        },
        {
            "text": "Tỉnh Thành",
            "type": "view",
            "collectionName": "tinhthanh",
            "route": "tinhthanh/collection",
            "$ref": "app/danhmuc/danhmuc_dungchung/TinhThanh/CollectionView",
        },
        {
            "type": "view",
            "collectionName": "tinhthanh",
            "route": "tinhthanh/model",
            "$ref": "app/danhmuc/danhmuc_dungchung/TinhThanh/ModelView",
        },
        {
            "text": "Quận Huyện",
            "type": "view",
            "collectionName": "quanhuyen",
            "route": "quanhuyen/collection",
            "$ref": "app/danhmuc/danhmuc_dungchung/QuanHuyen/CollectionView",
        },
        {
            "type": "view",
            "collectionName": "quanhuyen",
            "route": "quanhuyen/model",
            "$ref": "app/danhmuc/danhmuc_dungchung/QuanHuyen/ModelView",
        },
        {
            "text": "Xã Phường",
            "type": "view",
            "collectionName": "xaphuong",
            "route": "xaphuong/collection",
            "$ref": "app/danhmuc/danhmuc_dungchung/XaPhuong/CollectionView",
        },
        {
            "type": "view",
            "collectionName": "xaphuong",
            "route": "xaphuong/model",
            "$ref": "app/danhmuc/danhmuc_dungchung/XaPhuong/ModelView",
        },
        {
            "text": "Thôn Xóm",
            "type": "view",
            "collectionName": "thonxom",
            "route": "thonxom/collection",
            "$ref": "app/danhmuc/danhmuc_donvi/ThonXom/CollectionView",
        },
        {
            "type": "view",
            "collectionName": "thonxom",
            "route": "thonxom/model",
            "$ref": "app/danhmuc/danhmuc_donvi/ThonXom/ModelView",
        },
        {
            "route" : "quan_ly_nguoi_dung/collection",
            "$ref" : "app/quanlynguoidung/view/CollectionView",
            "visible": function() {
                return false;
            }
        },
        {
            "route" : "quan_ly_nguoi_dung/model",
            "$ref" : "app/quanlynguoidung/view/ModelView"
        },
        {
            "route" : "tokhai_yte/collection",
            "$ref" : "app/khaibaoyte/danhsach/CollectionView"
        },
        {
            "route" : "tokhai_yte/model",
            "$ref" : "app/khaibaoyte/khaibao/ModelView"
        },
        {
            "route" : "tokhai_yte/ketqua",
            "$ref" : "app/khaibaoyte/ketqua/ModelView"
        },
        {
            "route" : "tokhai_yte/lichsu",
            "$ref" : "app/khaibaoyte/lichsu/CollectionView"
        },
        {
            "route" : "tokhai_yte/trieuchung/collection",
            "$ref" : "app/khaibaoyte/trieuchung/view/CollectionView"
        },
        {
            "route" : "tokhai_yte/dichte/collection",
            "$ref" : "app/khaibaoyte/dichte/view/CollectionView"
        }
        
    ];

});