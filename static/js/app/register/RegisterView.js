define(function (require) {

    "use strict";
    var $ = require('jquery'),
        _ = require('underscore'),
        Gonrin = require('gonrin'),
        tpl = require('text!app/register/tpl/register.html'),
        template = _.template(tpl)
    return Gonrin.View.extend({
        
        render: function () {
            var self = this;
            this.$el.html(template());
            $('.account-pages').addClass('bg-gradient min-vh-100');
            this.$el.find("#btn-register").on("click", function () {
                self.processRegister();
                return false;
            });
            this.$el.find('input[name=role]').on('change', function(){
                $(this).parent().addClass("type-selected").removeClass("type-no-select");
                $(this).parent().siblings().addClass("type-no-select").removeClass("type-selected");
            })
            

            return this;
        },
        processRegister: function () {
            var self = this;
            var phone = this.$('#phone_number').val(),
                password = this.$('#password').val().trim(),
                confirm_password = this.$("#confirm_password").val(),
                role = this.$('input[name=role]:checked').val();
            if(role!=='duocsi' && role!=='tochuc'){
                self.getApp().notify({ message: "Lựa chọn không hợp lệ" }, { type: "danger", delay: 2000 });
                return;
            }    
            if (phone === undefined || phone === "" || phone===null) {
                self.getApp().notify({ message: "Vui lòng nhập số điện thoại" }, { type: "danger", delay: 2000 });
                return;
            }
            if(!self.getApp().validatePhone(phone)){
                self.getApp().notify({ message: "Số điện thoại không hợp lệ" }, { type: "danger", delay: 2000 });
                return;
            }
            if (password === undefined || password === ""  || password===null) {
                self.getApp().notify({ message: "Vui lòng nhập mật khẩu" }, { type: "danger", delay: 2000 });
                return;
            }
            if(password.length < 3){
                self.getApp().notify({ message: "Mật khẩu quá ngắn" }, { type: "danger", delay: 2000 });
                return;
            }

            if (password !== confirm_password) {
                self.getApp().notify({ message: "Mật khẩu không khớp" }, { type: "danger", delay: 2000 });
                return;
            }
            var data = JSON.stringify({
                role: role,
                phone : phone,
                password: password
            });

            self.getApp().showloading();
            var path = self.getApp().serviceURL + '/api/v1/register';
            $.ajax({
                url: path,
                type: 'POST',
                data: data,
                headers: {
                    'content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                beforeSend: function () {
                    $("#loading").removeClass("d-none");
                },
                dataType: 'json',
                success: function (data) {
                    self.getApp().getRouter().navigate('confirm-active?uid=' + data.id);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    try {
                        self.getApp().notify({ message: $.parseJSON(xhr.responseText).error_message }, { type: "danger", delay: 1000 });
                    } catch (err) {
                        self.getApp().notify({ message: "Có lỗi xảy ra, vui lòng thử lại sau" }, { type: "danger", delay: 1000 });
                    }
                },
                complete: function () {
                    self.getApp().hideloading();
                    return false;
                }
            });
        },

    });

});