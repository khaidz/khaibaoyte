# -*- coding: utf-8 -*-
""" Module for managing tasks through a simple cli interface. """
# Libraries

import sys
from os.path import abspath, dirname
sys.path.insert(0, dirname(abspath(__file__)))
import os
from sqlalchemy import create_engine, or_
from sqlalchemy.sql import table, column, select, update, insert

from manager import Manager
from sqlalchemy.inspection import inspect
from application import run_app
from application.server import app
from application.database import init_database, db

from application.extensions import auth, init_extensions

from gatco_restapi.helpers import to_dict
from gatco.response import text
from gatco_restapi import  ProcessingException

from application.controllers.helpers.helper_common import generator_salt, convert_text_khongdau, default_uuid
from application.models.model_danhmuc import QuocGia, TinhThanh, QuanHuyen, XaPhuong, DanToc, TuyenDonVi
import json
from application.models.model_donvi import *
import xlrd
from application.controllers.helpers.helper_common import *

manager = Manager()

@manager.command
def create_test_models():
    # from application.config.development import Config
    from application.config.production import Config
    app.config.from_object(Config)
    init_database(app)
    init_extensions(app)

    salt = str(generator_salt())
    role_admin = db.session.query(Role).filter(Role.name == 'admin').first()
    if role_admin is not None:
        # role_admin = Role(name='admin',description='Admin')
        # db.session.add(role_admin)
        # db.session.commit()
        user1 = User(email='admin', password=auth.encrypt_password("123456abcA", salt), active=1, salt = salt)
        user1.roles.append(role_admin)
        db.session.add(user1)
        db.session.commit()


notdict = ['_created_at','_updated_at','_deleted','_deleted_at','_etag']



@manager.command
def generate_schema(path = None, exclude = None, prettyprint = True):
    """ Generate javascript schema"""
    exclude_list = None
    if path is None:
        print("Path is required")
        return
    
    if exclude is not None:
        exclude_list = exclude.split(",")
        
    for cls in [cls for cls in db.Model._decl_class_registry.values() if isinstance(cls, type) and issubclass(cls, db.Model)]:
        classname = cls.__name__
        if (exclude_list is not None) and (classname in exclude_list):
            continue
        schema = {}
        for col in cls.__table__.c:
            col_type = str(col.type)
            if col.name in notdict:
                continue
            schema_type = ''
            if 'DECIMAL' in col_type:
                schema_type = 'number'
            if col_type in ['INTEGER','SMALLINT', 'FLOAT', 'BIGINT' ]:
                schema_type = 'number'
            if col_type == 'DATETIME':
                schema_type = 'datetime'
            if col_type == 'DATE':
                schema_type = 'datetime'
            if 'VARCHAR' in col_type:
                schema_type = 'string'
            if col_type in ['VARCHAR', 'UUID', 'TEXT']:
                schema_type = 'string'
            if col_type in ['JSON', 'JSONB']:
                schema_type = 'json'
            if 'BOOLEAN' in col_type:
                schema_type = 'boolean'
            
            schema[col.name] = {"type": schema_type}
            
            if col.primary_key:
                schema[col.name]["primary"] = True
            if (not col.nullable) and (not col.primary_key):
                schema[col.name]["required"] = True
                
            if hasattr(col.type, "length") and (col.type.length is not None):
                schema[col.name]["length"] = col.type.length
            if (col.default is not None) and (col.default.arg is not None) and (not callable(col.default.arg)):
                schema[col.name]["default"] = col.default.arg
        relations = inspect(cls).relationships
        for rel in relations:
            if rel.direction.name in ['MANYTOMANY', 'ONETOMANY']:
                schema[rel.key] = {"type": "list"}
            if rel.direction.name in ['MANYTOONE']:
                schema[rel.key] = {"type": "dict"}
            
        if prettyprint:
            with open(path + '/' + classname + 'Schema.json', 'w') as outfile:
                ujson.dump(schema,  outfile, indent=4,)
        else:
            with open(path + '/' + classname + 'Schema.json', 'w') as outfile:
                ujson.dump(schema,  outfile,)

@manager.command
def init_danhmuc(): 
    # from application.config.development import Config
    from application.config.production import Config
    app.config.from_object(Config)
    init_database(app)

    print("add quoc gia==========")
    add_quoc_gia()

    print("add danh muc tinh thanh=============")
    add_danhmuc_tinhthanh()
    
    print("add danh muc quan huyen=============")
    add_danhmuc_quanhuyen()
    
    print("add danh muc xa phuong=============")
    add_danhmuc_xaphuong()
     
    print("add danh muc dan toc=============")
    add_danhmuc_dantoc()

    print("add role==========")
    add_role()

    print("add tuyendonvi====================")
    add_danhmuc_tuyendonvi()

    
    # print("migrate ten khong dau======")
    # migrate_tenkhongdau()

@manager.command
def add_quoc_gia():   

    check_quocgia = db.session.query(QuocGia).filter(QuocGia.ma == 'VN').first()
    if check_quocgia is None:
        quocgias = QuocGia( ma = "VN", ten = "Việt Nam")
        db.session.add(quocgias)
        db.session.flush() 
        db.session.commit()

@manager.command
def add_danhmuc_tinhthanh():
    
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
        json_url_dstinhthanh = os.path.join(SITE_ROOT, "application/data", "ThongTinTinhThanh.json")
        print("danhmuc====", json_url_dstinhthanh)
        data_dstinhthanh = ujson.load(open(json_url_dstinhthanh))
        # print("danhmuc1231====", data_dstinhthanh)
        for item_dstinhthanh in data_dstinhthanh:
            tinhthanh_filter = db.session.query(TinhThanh).filter(TinhThanh.ma == item_dstinhthanh["matinhthanh"]).first()
            if tinhthanh_filter is None:
                quocgia_filter = db.session.query(QuocGia).filter(QuocGia.ma == 'VN').first()
                tinhthanh_filter = TinhThanh(ten = item_dstinhthanh["tentinhthanh"], ma = item_dstinhthanh["matinhthanh"], quocgia_id = quocgia_filter.id)
                tinhthanh_filter.tenkhongdau = convert_text_khongdau(item_dstinhthanh["tentinhthanh"])
                db.session.add(tinhthanh_filter)
                db.session.commit()
    except Exception as e:
        print("TINH THANH ERROR",e)

@manager.command
def add_danhmuc_quanhuyen():
   
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
        json_url_dsquanhuyen = os.path.join(SITE_ROOT, "application/data", "ThongTinTinhThanh.json")
        data_dsquanhuyen = ujson.load(open(json_url_dsquanhuyen))
        for item_dsquanhuyen in data_dsquanhuyen:
            quanhuyen_filter = db.session.query(QuanHuyen).filter(QuanHuyen.ma == item_dsquanhuyen["maquanhuyen"]).first()
            if quanhuyen_filter is None:
                tinhthanh_filter = db.session.query(TinhThanh).filter(TinhThanh.ma == item_dsquanhuyen["matinhthanh"]).first()
                quanhuyen_filter = QuanHuyen(ten = item_dsquanhuyen["tenquanhuyen"], ma = item_dsquanhuyen["maquanhuyen"], tinhthanh_id = tinhthanh_filter.id)
                quanhuyen_filter.tenkhongdau = convert_text_khongdau(item_dsquanhuyen["tenquanhuyen"])
                db.session.add(quanhuyen_filter)
        db.session.commit()
    except Exception as e:
        print("QUAN HUYEN ERROR", e)

@manager.command
def add_danhmuc_xaphuong():
   
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
        json_url_dsxaphuong = os.path.join(SITE_ROOT, "application/data", "ThongTinTinhThanh.json")
        data_dsxaphuong = ujson.load(open(json_url_dsxaphuong))
        for item_dsxaphuong in data_dsxaphuong:
            xaphuong_filter = db.session.query(XaPhuong).filter(XaPhuong.ma == item_dsxaphuong["maxaphuong"]).first()
            if xaphuong_filter is None:
                quanhuyen_filter = db.session.query(QuanHuyen).filter(QuanHuyen.ma == item_dsxaphuong["maquanhuyen"]).first()
                xaphuong_filter = XaPhuong(ten = item_dsxaphuong["tenxaphuong"], ma = item_dsxaphuong["maxaphuong"], quanhuyen_id = quanhuyen_filter.id)
                xaphuong_filter.tenkhongdau = convert_text_khongdau(item_dsxaphuong["tenxaphuong"])
                db.session.add(xaphuong_filter)
        db.session.commit()
    except Exception as e:
        print("XA PHUONG ERROR", e)

@manager.command
def add_danhmuc_dantoc():
   
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))    
        json_url_dantoc = os.path.join(SITE_ROOT, "application/data", "DanTocEnum.json")
        data_dantoc = ujson.load(open(json_url_dantoc))
        for item_dantoc in data_dantoc:
            check_dantoc = db.session.query(DanToc).filter(DanToc.ma == str(item_dantoc["value"])).first()
            if check_dantoc is None:
                dantoc = DanToc(ma = str(item_dantoc["value"]), ten = item_dantoc["text"])
                dantoc.tenkhongdau = convert_text_khongdau(item_dantoc["text"])
                db.session.add(dantoc)
        db.session.commit()
    except Exception as e:
        print("DAN TOC ERROR", e)

@manager.command
def add_danhmuc_tuyendonvi():
    
    try:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))    
        json_url_tuyendonvi = os.path.join(SITE_ROOT, "application/data", "TuyenDonViEnum.json")
        data_tuyendonvi = ujson.load(open(json_url_tuyendonvi))
        for item_tuyendonvi in data_tuyendonvi:
            check_dantoc = db.session.query(TuyenDonVi).filter(TuyenDonVi.ma == str(item_tuyendonvi["value"])).first()
            if check_dantoc is None:
                tuyendonvi = TuyenDonVi(id = str(item_tuyendonvi["value"]), ma = str(item_tuyendonvi["value"]), ten = item_tuyendonvi["text"])
                db.session.add(tuyendonvi)
        db.session.commit()
    except Exception as e:
        print("TUYEN DON VI ERROR", e)

@manager.command
def add_role():
    role_admin = db.session.query(Role).filter(Role.name == 'admin').first()
    if role_admin is None:
        role = Role(name='admin',description='Admin')
        db.session.add(role)
        db.session.commit()

    role_user= db.session.query(Role).filter(Role.name == 'user').first()
    if role_user is None:
        role = Role(name = 'user',description='User')
        db.session.add(role)
        db.session.commit()

@manager.command
def migrate_tenkhongdau():
    # from application.config.development import Config
    from application.config.production import Config
    app.config.from_object(Config)
    init_database(app)

    list_item = db.session.query(TinhThanh).all()
    for item in list_item:
        if item is not None and item.ten is not None:
            item.tenkhongdau = convert_text_khongdau(item.ten)
    db.session.commit()

    list_item = db.session.query(QuanHuyen).all()
    for item in list_item:
        if item is not None and item.ten is not None:
            item.tenkhongdau = convert_text_khongdau(item.ten)
    db.session.commit()

    list_item = db.session.query(XaPhuong).all()
    for item in list_item:
        if item is not None and item.ten is not None:
            item.tenkhongdau = convert_text_khongdau(item.ten)
    db.session.commit()

    list_item = db.session.query(User).all()
    for item in list_item:
        if item is not None and item.name is not None:
            item.tenkhongdau = convert_text_khongdau(item.name)
    db.session.commit()

    list_item = db.session.query(DonVi).all()
    for item in list_item:
        if item is not None and item.name is not None:
            item.unsigned_name = convert_text_khongdau(item.name)
    db.session.commit()

    list_item = db.session.query(DanToc).all()
    for item in list_item:
        if item is not None and item.ten is not None:
            item.tenkhongdau = convert_text_khongdau(item.ten)
    db.session.commit()



def convert_columexcel_to_string(value):
    # print("value", value)
    if isinstance(value,str):
        return value.strip()
    if isinstance(value, float):
        return str(int(value)).strip()
    if isinstance(value,int):
        return str(value).strip()

@manager.command
def convert_excel_to_json_khoaphong():
    # from application.config.development import Config
    from application.config.production import Config
    app.config.from_object(Config)
    init_database(app)
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))    
    path = os.path.join(SITE_ROOT, "application/data", "Phong_ban.xls")
    print(path)
    if(os.path.exists(path) is False):
        print('Không tìm thấy file')
        return
    loc = (path) 
    wb = xlrd.open_workbook(loc) 
    wb_datemode = wb.datemode
    sheet = wb.sheet_by_index(0) 
    sheet.cell_value(0, 0) 
    count =0
    result=[]
    for i in range(sheet.nrows):
        if i == 0:
            continue
        id = str(sheet.cell_value(i,0)).strip()
        ten = str(sheet.cell_value(i,1)).strip()
        tenkhongdau = convert_text_khongdau(ten)
        result.append(to_dict({
            'id': id,
            'ten':ten,
            'tenkhongdau':tenkhongdau
        }))
    print(result)
    print('Xong!!')   
    return


@manager.command
def run():
    """ Starts server on port 12009. """
    run_app(host="0.0.0.0", port=12009, mode="production")

@manager.command
def rundev():
    """ Starts server on port 12009. """
    run_app(host="0.0.0.0", port=12009, mode="development")
    
if __name__ == '__main__':
    manager.main()
