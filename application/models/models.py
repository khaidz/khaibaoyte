""" Module represents a User. """

from sqlalchemy import (
    func, Column, Table, String, Integer, SmallInteger,BigInteger,
    DateTime, Date, Boolean, Float,
    Index, ForeignKey,UniqueConstraint, event, __version__
)
from sqlalchemy import or_,and_

from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.collections import attribute_mapped_collection


import uuid
from datetime import datetime

from gatco_restapi import ProcessingException
from application.database import db
from application.database.model import CommonModel, default_uuid
from sqlalchemy.dialects.postgresql import UUID, JSONB
from application.models.model_danhmuc import QuocGia, TinhThanh, QuanHuyen, XaPhuong, ThonXom


def default_uuid():
    return str(uuid.uuid4())


class Notify(CommonModel):
    __tablename__ = 'notify'
    id = db.Column(String, primary_key=True, default=default_uuid)
    title = db.Column(String, index=True)
    content = db.Column(String)
    type = db.Column(String(20))  # text/image/video
    url = db.Column(String)
    iteminfo = db.Column(JSONB())
    notify_condition = db.Column(JSONB())
    
class NotifyUser(CommonModel):
    __tablename__ = 'notify_user'
    id = db.Column(String, primary_key=True, default=default_uuid)
    user_id = db.Column(String)
    notify_id = db.Column(String, ForeignKey('notify.id'), nullable=True)
    notify = db.relationship('Notify')
    notify_at = db.Column(BigInteger())
    read_at = db.Column(BigInteger())


class Category(CommonModel):
    __tablename__ = 'category'
    id = db.Column(String, primary_key=True, default=default_uuid)
    name = db.Column(String)
    tenkhongdau = db.Column(String)
    description = db.Column(String)
    status = db.Column(Integer, default=0)
    priority = db.Column(Integer())

class Post(CommonModel):
    __tablename__ = 'post'
    id = db.Column(String, primary_key=True, default=default_uuid)
    user_id = db.Column(String, index=True, nullable=False)
    category_id = db.Column(String, ForeignKey('category.id', onupdate='CASCADE', ondelete='SET NULL'), nullable=True)
    category = relationship('Category')
    title = db.Column(String)
    description = db.Column(String)
    content = db.Column(String)
    image = db.Column(JSONB())
    status = db.Column(Integer, default=0)
    approvedtime = db.Column(BigInteger())
    approvedby = db.Column(String())
    #add
    priority = db.Column(Integer())
    tags_age = db.Column(JSONB())
    type_show = db.Column(JSONB())
    show = db.Column(SmallInteger(), default=1)
