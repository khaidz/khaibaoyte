from sqlalchemy import (
    Column, String, Integer,SmallInteger, BigInteger, DateTime, Date, Boolean, DECIMAL, Text, ForeignKey, UniqueConstraint, JSON, Index, Float
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.collections import attribute_mapped_collection

from sqlalchemy.orm import *
from application.database import db
from application.database.model import CommonModel
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy import or_,and_
import uuid
from application.models.model_danhmuc import TuyenDonVi

def default_uuid():
    return str(uuid.uuid4())
    


roles_users = db.Table('roles_users',
                       db.Column('user_id', String(), db.ForeignKey('userinfo.id')),
                       db.Column('role_id', String(), db.ForeignKey('role.id')))


class User(CommonModel):
    __tablename__ = 'userinfo'
    id = db.Column(String(), primary_key=True, default=default_uuid)
    fullname = db.Column(String)
    unsigned_name = db.Column(String)
    accountName  = db.Column(String(255), index=True, unique=True)
    email = db.Column(String(255), index=True, unique=True)
    phone = db.Column(String(), index=True, unique=True)
    password = db.Column(String(255))
    salt = db.Column(String())
    type_user = db.Column(SmallInteger())

    active = db.Column(SmallInteger(), default=0) 
    #0- chua active, 1- active, >1 Khoa
    confirmed_at = db.Column(DateTime())
    type_confirm = db.Column(SmallInteger())
    #0 - email, 1- sms
    roles = relationship("Role", secondary=roles_users)
    donvi_id = db.Column(String, ForeignKey('donvi.id'), index=True, nullable=True)
    donvi = relationship('DonVi')
    
    # Methods
    def __repr__(self):
        """ Show user object info. """
        return '<User: {}>'.format(self.id)


    def has_role(self, role):
        if isinstance(role, str):
            return role in (role.name for role in self.roles)
        else:
            return role in self.roles
   

        

class Role(CommonModel):
    __tablename__ = 'role'
    id = db.Column(String(), primary_key=True,default=default_uuid)
    name = db.Column(String(200), unique=True)
    description = db.Column(String)
    active = db.Column(SmallInteger(), default=1) 

    
class DonVi(CommonModel):
    __tablename__ = 'donvi'
    id = db.Column(String, primary_key=True, default=default_uuid)
    ma_coso = db.Column(String(255), nullable=True)
    ten_coso = db.Column(String(255), nullable=True)
    ten = db.Column(String(255), nullable=False)
    sogiayphep = db.Column(String)
    ngaycapphep = db.Column(BigInteger)
    sodienthoai = db.Column(String(63))
    email = db.Column(String(255))
    website = db.Column(String)
    loaihinh_hoatdong = db.Column(String)
    lich_lamviec = db.Column(Float)
    gioithieu = db.Column(String)
    nguoidaidien = db.Column(String)
    nguoidaidien_dienthoai = db.Column(String)
    thumbnail_url = db.Column(String)
    banner_url = db.Column(String)
    xaphuong_id = db.Column(String, ForeignKey('xaphuong.id'), nullable=True)
    xaphuong = relationship('XaPhuong') 
    quanhuyen_id = db.Column(String, ForeignKey('quanhuyen.id'), nullable=True)
    quanhuyen = relationship('QuanHuyen')  
    tinhthanh_id = db.Column(String, ForeignKey('tinhthanh.id'), nullable=True)
    tinhthanh = relationship('TinhThanh', viewonly=True)
    quocgia_id = db.Column(String, ForeignKey('quocgia.id'), nullable=True)
    quocgia = relationship('QuocGia')
    diachi = db.Column(String)
    danh_sach_co_so = db.Column(JSONB)
    active = db.Column(Boolean(), default=False)
    tenkhongdau = db.Column(String())
    users = relationship('User', viewonly=True)
    

    def __repr__(self):
        return "DonVi(id=%r, ten=%r)" % (
                    self.id,
                    self.ten
                )
    def __todict__(self):
        return {"id":str(self.id), "ma": self.ma,"text": self.ten,"ten": self.ten}

    def __toid__(self):
        return self.id

    def dump(self, _indent=0):
        obj = self.__todict__()
        return obj
 


Index('user_uq_phone', User.phone, unique=True, postgresql_where=(and_(User.phone.isnot(None),User.phone !='')))
Index('user_uq_email', User.email, unique=True, postgresql_where=(and_(User.email.isnot(None),User.email !='')))