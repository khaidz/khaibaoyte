from sqlalchemy import (
    Column, String, Integer,SmallInteger, BigInteger,DateTime, Date, Boolean, DECIMAL, Text, ForeignKey, UniqueConstraint, JSON
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.collections import attribute_mapped_collection

from sqlalchemy.orm import *
from sqlalchemy.dialects.postgresql import UUID, JSONB
from application.database import db
from application.database.model import CommonModel
import uuid

def default_uuid():
    return str(uuid.uuid4())
    
class TrieuChung(CommonModel):
    __tablename__ = 'trieuchung'
    id = db.Column(String, primary_key=True, default=default_uuid)
    ten = ten = db.Column(String())
    tenkhongdau = db.Column(String)
    thutu_uutien = db.Column(SmallInteger, default=0)
    batbuoc = db.Column(SmallInteger, default=1)    #0: Khong, 1 co
    trangthai = db.Column(SmallInteger, default=1) # 0: disable, 1 active

class DichTe(CommonModel):
    __tablename__ = 'dichte'
    id = db.Column(String, primary_key=True, default=default_uuid)
    ten = ten = db.Column(String())
    tenkhongdau = db.Column(String)
    thutu_uutien = db.Column(SmallInteger, default=0)
    batbuoc = db.Column(SmallInteger, default=1)    #0: Khong, 1 co
    trangthai = db.Column(SmallInteger, default=1) # 0: disable, 1 active

class ToKhaiYTe(CommonModel):
    __tablename__ = 'tokhai_yte'
    id = db.Column(String, primary_key=True, default=default_uuid)
    uid = db.Column(String) #Luu vao storage
    stt = db.Column(String(255), nullable=False, unique=True)
    loai_khaibao = db.Column(SmallInteger) # 1: Benh nhan, 2: Nguoi nha, 3: Khai ho 
    ma_benh_nhan = db.Column(String)
    so_dien_thoai = db.Column(String)
    ten = db.Column(String())
    tenkhongdau = db.Column(String)
    gioi_tinh = db.Column(SmallInteger) #1: nam, 2 Nu, 3 Khac
    ngaysinh = db.Column(BigInteger)
    dia_chi = db.Column(String)
    khoa_phong = db.Column(String)
    trieuchung = db.Column(JSONB)
    dichte = db.Column(JSONB)
    coyeuto_dichte=db.Column(SmallInteger)
    lydodenvien = db.Column(String)
    ghi_chu = db.Column(String)
    sothe_bhyt = db.Column(String)
    bhyt_full = db.Column(String)
    thoigian_khaibao = db.Column(BigInteger)

class SoThuTu(CommonModel):
    __tablename__ = 'sothutu'
    id = db.Column(String, primary_key=True,default=default_uuid)
    type = db.Column(String, index=True)
    sothutu = db.Column(Integer)