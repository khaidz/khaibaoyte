from application.database import redisdb,db
from application.server import app
from gatco.response import json,text, html
from application.extensions import jinja
from application.extensions import apimanager
import asyncio
import hashlib
import binascii
from application.client import HTTPClient 
import ujson
import uuid
from sqlalchemy import func
import time
from application.controllers.helpers.helper_common import *            
from application.extensions import auth
from sqlalchemy import or_, and_
from gatco_restapi.helpers import to_dict
from application.models.model_donvi import User, Role, DonVi
from sanic import response


def deny_func(request=None, **kw):
    return json({'error_code':"ERROR_PERMISSION_DENY", 'error_message':'Permission denied'}, status=520)

@app.route('/')
async def index(request):
    return jinja.render('index.html', request)


@app.route('/landingpage')
async def index(request):
    return jinja.render('landingpage.html', request)


@app.route('/privacy')
async def contact(request):
#     return jinja.render('admin/index.html', request)
    return jinja.render('dieukhoan.html', request)

@app.route('/api/v1/current_user')
async def check_current_user(request):
    uid = current_uid(request)
    print("check_current_user.uid===",uid)
    data = await get_current_user(request,uid)
    if data is None:
        return json({"error_code": "SESSION_EXPIRED", "error_msg": "Hết phiên làm việc, vui lòng đăng nhập lại!"},status=520)
    else:
        return json(data,status=200)

@app.route('/login')
async def login(request):    
    return response.redirect('/#login')

# register    
@app.route('/api/v1/login', methods=["POST"])
async def do_login(request):
    if not check_content_json(request):
        return json({"error_message":"content type is not application-json", "error_code":"PARAM_ERROR"}, status=520)
    
    param = request.json
    if "data" not in param or param['data'] is None or "password" not in param \
        or (param['password'] is None) or (len(param['data']) == 0) or (len(param['password']) == 0):
        return json({"error_message":"Tham số không hợp lệ", "error_code":"PARAM_ERROR"}, status=520)
    
    data = param['data']
    password = param['password']
    user = db.session.query(User).filter(or_(User.phone == data, User.email == data)).first()
    if user is None or user.deleted == True:
        return json({"error_code":"LOGIN_FAILED", "error_message":u"Tài khoản không tồn tại"}, status=520)
    elif user.active !=1:
        return json({"error_code":"LOGIN_FAILED", "error_message":u"Tài khoản đang bị khóa"}, status=520)
    else:
        if auth.verify_password(password, user.password, user.salt):
            result = response_current_user(user)
            return json(result, status=200)
        else:
            return json({"error_code":"LOGIN_FAILED", "error_message":u"Mật khẩu không đúng"}, status=520)

@app.route('/api/v1/register', methods=["POST"])
async def register(request):
    if request.method == 'POST':
        data = request.json
        password = data.get('password', None)
        name = data.get('name', None)
        email = data.get('email', None)
        phone = data.get('phone', None)
        type_confirm = data.get('type_confirm', None)

        if name is None or  name.strip() == '':
            return json({"error_code": "REGISTER_FAILED", "error_message": "Họ và tên không được để trống!"},status=520)
        if password is None or  password.strip() == '':
            return json({"error_code": "REGISTER_FAILED", "error_message": "Mật khẩu không hợp lệ!"},status=520)
        
        if email is not None and email.strip() != '':
            check_email = db.session.query(User).filter(and_(User.email == email, User.deleted == False)).first()
            if check_email is not None:
                return json({"error_code": "REGISTER_FAILED", "error_message": "Email đã tồn tại trong hệ thống"},status=520)
        else:
            email = None

        if phone is not None and phone.strip() == '':
            phone = None
        
        if phone is not None:
            check_phone = db.session.query(User).filter(and_(User.phone == phone, User.deleted == False)).first()
            if check_phone is not None:
                return json({"error_code": "REGISTER_FAILED", "error_message": "Số điện thoại đã tồn tại trong hệ thống"},status=520)
        if phone is None and email is None:
            return json({"error_code": "REGISTER_FAILED", "error_message": "Email hoặc số điện thoại không được để trống"},status=520)

        
        salt = str(generator_salt())
        role3 = db.session.query(Role).filter(Role.name == 'nguoidan').first()
        user = User(email=email, name=name, password=auth.encrypt_password(password,salt), active=0, salt = salt)
        user.phone = phone
        user.roles.append(role3)
        user.unsigned_name = convert_text_khongdau(name)
        user.type_confirm = type_confirm
        db.session.add(user)
        db.session.commit()
        result = response_current_user(user)
        await send_active_account(request,to_dict(user))
        return json(result, status=200)
        
    return json({"error_code": "REGISTER_FAILED", "error_message": "Method not found"},status=520)

@app.route('/api/v1/register/resend-code', methods=["POST"])
async def register_resend_code(request):
    data = request.json
    uid = data.get('uid', None)
    
    user = db.session.query(User).filter(and_(User.id == uid, User.deleted == False)).first()
    if user is None:
        return json({"error_code": "ACTIVE_FAILED", "error_message": "Tham số không hợp lệ"},status=520)
    await send_active_account(request,to_dict(user))
    return json({"error_message":"Gửi mã xác nhận thành công"}, status=200)

@app.route('/api/v1/register/active', methods=["POST"])
async def register_active(request):
    data = request.json
    uid = data.get('uid', None)
    active = data.get('active', None)
    check_token = redisdb.get("session-active-account:"+str(uid))
    if check_token is not None:
        str_active = check_token.decode('utf8')

        if active != str_active:
            return json({"error_code": "ACTIVE_FAILED", "error_message": "Mã số không hợp lệ"},status=520)
        else:
            user = db.session.query(User).filter(and_(User.id == uid, User.deleted == False)).first()
            if user is None:
                return json({"error_code": "ACTIVE_FAILED", "error_message": "Tham số không hợp lệ"},status=520)
            user.active = 1
            db.session.commit()
            result = response_current_user(user)
            return json(result, status=200)
    else:
        return json({"error_code": "ACTIVE_FAILED", "error_message": "Mã số hết hạn sử dụng, vui lòng thử lại"},status=520)


# 5c0df310-2202-4801-b24e-4834db57406f
# logout      
@app.route('/logout', methods=['GET'])    
async def logout2(request):
    return logout(request)

@app.route('/api/v1/logout', methods=['GET'])
async def logout1(request):
    return logout(request)

def logout(request):
    token = request.headers.get("X-USER-TOKEN", None)
    if token is not None:
        redisdb.delete("sessions:" + token)
    
    return json({"error_message": "successful!"})
    #else:
    #    return json({"error_code":"LOGOUT_FAILED","error_message": "Token is not found in header"})

# change password
@app.route('/api/v1/user/changepw', methods=['POST'])
async def changepassword(request):
    error_msg = None
    params = request.json
    # password = params['password']
    password = params['password']
    cfpassword = params['confirm_password']
    uid_current = current_uid(request)
    if uid_current is None:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết hạn phiên làm việc"}, status=520)
    
    if((error_msg is None)):
        if(password != cfpassword ) :
            error_msg = u"Xin mời nhập lại mật khẩu!"
    salt = generator_salt()
    print('\n\n\n===', salt)
    user = db.session.query(User).filter(User.id == uid_current).first()
    if user is None:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết hạn phiên làm việc"}, status=520)  
    newpassword = auth.encrypt_password(str(password), str(salt))
    user.password = newpassword
    user.salt = salt
    db.session.commit()
    return json({"error_code": "OK", "error_message": "successfilly"},status=200) 

    if request.method == 'POST':
        uid_current = current_uid(request)
        if uid_current is None:
            return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết phiên làm việc, vui lòng đăng nhập lại"}, status=520)
        user = db.session.query(User).filter(or_(User.id == uid_current)).first()
        
        if user is None:
            return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết hạn phiên làm việc"}, status=520)
        
        return json({"error_code": "Ok", "error_message": "successfully", "data": response_current_user(user)},status=200)



async def preprocess_create_user(data, Model, **kw):
    param = data
    user = User()
    phone = param["phone"]
    if param["phone"] is None and param["email"] is None:
        return json({"error_message":"Tham số không hợp lệ.", "error_code":"PARAM_ERROR"}, status=520)
        
    if phone is not None and (phone[0] == 0  or phone[0] == '0'):
        check_phone = db.session.query(User).filter(User.deleted == False).filter(User.phone == phone).first()
        if check_phone is not None:
            return json({"error_message":"Số điện thoại đã tồn tại, vui lòng nhập lại", "error_code":"PARAM_ERROR"}, status=520)
    if ("email" in param) and (param["email"] is not None) and (len(param["email"])>0):
        check_email = db.session.query(User).filter(User.deleted == False).filter(User.email == param["email"]).first()
        if check_email is not None:
            return json({"error_message":"Email đã tồn tại trong hệ thống, vui lòng chọn email khác", "error_code":"PARAM_ERROR"}, status=520)
    
    user.id = default_uuid()
    # data['id'] = user.id
    user.email = param["email"]
    user.phone = phone
    user.fullname = param["fullname"]
    if(param["fullname"] is not None):
        user.unsigned_name = convert_text_khongdau(param["fullname"])
    user.active = 1
    
    if 'roles' in param:
        role = db.session.query(Role).filter(Role.name == param["roles"]).first()
        if role is None:
            return json({"error_code":"ERROR_PARAM","error_message":"Tham số không hợp lệ"},status=520)
        roles_dict = []
        user.roles = []
        roles_dict.append(role)
        user.roles = roles_dict 

    salt = generator_salt()
    if("password" in param and param["password"] is not None and param["password"] !=""):
        newpassword = auth.encrypt_password(str(param['password']), str(salt))
        user.password = newpassword
        user.salt = salt
    del param['password']
    db.session.add(user)
    db.session.commit()
    return json({}, status=200)


async def preprocess_update_user(data, Model, **kw):
    param = data
    if ("id" not in param or param['id'] is None):
        return json({"error_message":"Tham số không hợp lệ", "error_code":"PARAM_ERROR"}, status=520)

    user = db.session.query(User).filter(and_(User.id == param["id"],User.deleted == False)).first()
    if user is None:
        return json({
                "error_code": "USER_NOT_FOUND",
                "error_message":"Không tìm thấy tài khoản"
            }, status=520)

    
    phone = param["phone"]
    if param["phone"] is None and param["email"] is None:
        return json({"error_message":"Tham số không hợp lệ.", "error_code":"PARAM_ERROR"}, status=520)
        
    if phone is not None and phone!=user.phone and (phone[0] == 0  or phone[0] == '0'):
        check_phone = db.session.query(User).filter(User.deleted == False).filter(User.phone == phone).first()
        if check_phone is not None:
            return json({"error_message":"Số điện thoại đã tồn tại, vui lòng nhập lại", "error_code":"PARAM_ERROR"}, status=520)
    if ("email" in param) and (param["email"] is not None) and param["email"]!=user.email and (len(param["email"])>0):
        check_email = db.session.query(User).filter(User.deleted == False).filter(User.email == param["email"]).first()
        if check_email is not None:
            return json({"error_message":"Email đã tồn tại trong hệ thống, vui lòng chọn email khác", "error_code":"PARAM_ERROR"}, status=520)
    
    user.email = param["email"]
    user.phone = phone
    user.fullname = param["fullname"]
    if(param["fullname"] is not None):
        user.unsigned_name = convert_text_khongdau(param["fullname"])
    
    if 'roles' in param:
        role = db.session.query(Role).filter(Role.name == param["roles"]).first()
        if role is None:
            return json({"error_code":"ERROR_PARAM","error_message":"Tham số không hợp lệ"},status=520)
        roles_dict = []
        user.roles = []
        roles_dict.append(role)
        user.roles = roles_dict 
    
    salt = generator_salt()
    if("password" in param and param["password"] is not None and param["password"] !=""):
        newpassword = auth.encrypt_password(str(param['password']), str(salt))
        user.password = newpassword
        user.salt = salt
    if "password" in param:
        del param['password']
    if "active" in param:
        user.active = param['active']
    user.deleted = param['deleted']
    user.deleted_by = param['deleted_by']
    db.session.commit()
    return json({}, status=200)


apimanager.create_api(User,
    methods=['GET', 'POST', 'DELETE', 'PUT'],
    url_prefix='/api/v1',
    preprocess=dict(GET_SINGLE=[validate_user], GET_MANY=[validate_admin], POST=[validate_admin, preprocess_create_user], PUT_SINGLE=[validate_user, preprocess_update_user], DELETE_SINGLE=[validate_admin]),
    exclude_columns= ["password","salt"],
    collection_name='user')

apimanager.create_api(Role,
    methods=['GET', 'POST', 'DELETE', 'PUT'],
    url_prefix='/api/v1',
    preprocess=dict(GET_SINGLE=[validate_user], GET_MANY=[validate_user], POST=[validate_admin], PUT_SINGLE=[validate_admin], DELETE_SINGLE=[validate_admin]),
    collection_name='role')
