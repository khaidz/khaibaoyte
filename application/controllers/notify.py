import string, time
import random
import uuid
import base64, re
import binascii
import aiohttp
import ujson
import hashlib

from math import floor
import asyncio


from gatco.response import json, text, html
from application.extensions import apimanager
from application.models.model_donvi import User, DonVi
from application.models.models import  NotifyUser, Notify
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from application.database import db, redisdb
from application.server import app
from sqlalchemy import or_, and_, desc, asc
from gatco_restapi.helpers import to_dict
from application.extensions import  jinja
from application.controllers.helpers.helper_notify import send_notify_single,\
    send_firebase_notify

from application.controllers.helpers.helper_common import *
from application.controllers.helpers.EmailClient import  send_email, send_mail_process
from sqlalchemy.orm.attributes import flag_modified


async def apply_user_filter(request=None, search_params=None, **kw):
    uid_current = current_uid(request)
    if uid_current is not None:
        list_user_notify = db.session.query(NotifyUser).filter(NotifyUser.user_id == uid_current).order_by(desc(NotifyUser.updated_at)).limit(100)
        if list_user_notify is None or list_user_notify:
            return json({"objects":[]})
        arr_uid = []
        for uid in list_user_notify:
            arr_uid.append(uid)
        
        search_params["filters"] = {"id": {"$in": arr_uid}}
    else:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết phiên làm việc, vui lòng đăng nhập lại"}, status=520)

@app.route('/api/v1/notify/read', methods=['POST', 'GET'])
async def read_notify(request):
    uid_current = current_uid(request)
    if uid_current is None:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết phiên làm việc, vui lòng đăng nhập lại"}, status=520)
    else:
        if request.method == 'GET':
            arrNotify = []
            notify_user = db.session.query(NotifyUser).\
            filter(NotifyUser.user_id == uid_current).order_by(desc(NotifyUser.created_at)).limit(300)
            if notify_user is not None:
                for nf in notify_user:
                    notify = to_dict(nf.notify)
                    notify["read_at"] = nf.read_at
                    arrNotify.append(notify)
                    if nf.read_at is None:
                        nf.read_at = floor(time.time())
            # db.session.commit()
            return json({"objects": arrNotify})
        elif request.method == 'POST':
            params = request.json
            id = params.get('id', None)
            if id is None:
                return json({"error_code": "PARAM_ERROR", "error_message": "Không tìm thấy thông báo1. Vui lòng thử lại sau."}, status=520)
            notify_info = db.session.query(NotifyUser).filter(NotifyUser.notify_id == id).first()
            if notify_info is None:
                return json({"error_code": "PARAM_ERROR", "error_message": "Không tìm thấy thông báo. Vui lòng thử lại sau."}, status=520)
            notify_info.read_at = floor(time.time())
            updated_at = notify_info.updated_at
            notify_info.updated_at = updated_at
            db.session.commit()
            return json({"error_message": "successful"}, status=200)

@app.route('/api/v1/notify/check', methods=['GET'])
async def check_notify(request):
    uid_current = current_uid(request)
    if uid_current is None:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết phiên làm việc, vui lòng đăng nhập lại"}, status=520)
    else:
        count = 0
        notify_user = db.session.query(NotifyUser).\
        filter(and_(NotifyUser.user_id == uid_current, NotifyUser.read_at == None)).count()
        if notify_user is not None:
            count = notify_user
        check_all =db.session.query(NotifyUser, Notify).filter(and_(NotifyUser.user_id == uid_current, NotifyUser.notify_id == Notify.id)).\
            filter(NotifyUser.read_at.is_(None)).order_by(desc(NotifyUser.updated_at)).all()
        
        check_new = None
        noti_user = None

        for item, notify in check_all:
            if (check_new is None):
                check_new = to_dict(notify)
                noti_user = item

        if noti_user is not None and noti_user.read_at is None:
            notify_info = db.session.query(NotifyUser).filter(NotifyUser.id ==noti_user.id).first()
            if notify_info is not None:
                notify_info.read_at = floor(time.time())
                # db.session.commit()
        return json({"error_message": "successful", "data": count, "notify": to_dict(check_new)})

@app.route('/api/v1/notify/send', methods=['POST'])
async def send_notify(request):
    data = request.json
    id = data.get('id', None)
    title = data.get('title', None)
    content = data.get('content', None)
    notify_type= data.get('notify_type')
    url=data.get('url')
    iteminfo = data.get('iteminfo')
    await send_notify_single(str(id), title,content, notify_type ,url, iteminfo)
    return json([], status=200)

@app.route('/api/v1/email/send', methods=['POST'])
async def send_email_lienket(request):
    data = request.json
    email = data.get('email')
    name = data.get('name')
    result = data.get('result')
    time = data.get('time')
    subject = 'Liên kết chứng chỉ hành nghề dược'
    mailbody = jinja.render_string('email/xacnhan-lienket.html',request, name=name, result = result, time =time)
    
    scheduler = AsyncIOScheduler()
    scheduler.add_job(send_mail_process,args=[subject,email, mailbody])
    scheduler.start()
    return json({"error_message": "Gửi email thành công"}, status=200)


apimanager.create_api(Notify, max_results_per_page=1000000,
                         methods=['GET', 'POST', 'DELETE', 'PUT'],
                         url_prefix='/api/v1',
                         preprocess=dict(GET_SINGLE=[], GET_MANY=[], POST=[], PUT_SINGLE=[]),
                         postprocess=dict(POST=[]),
                         collection_name='notify')

apimanager.create_api(NotifyUser, max_results_per_page=1000000,
                         methods=['GET', 'POST', 'DELETE', 'PUT'],
                         url_prefix='/api/v1',
                         preprocess=dict(GET_SINGLE=[], GET_MANY=[apply_user_filter], POST=[], PUT_SINGLE=[]),
                         postprocess=dict(POST=[]),
                         collection_name='notify_user')


@app.route('/api/v1/set_notify_token', methods=['POST'])
async def set_notify_token(request):
    uid_current = current_uid(request)
    if uid_current is None:
        return json({
            "error_code": "USER_NOT_LOGIN",
            "error_message": None
        }, status=520)

    data = request.json
    token = data.get("data", None)
    type_notify = data.get("type_notify", None)
    if(type_notify is not None and type_notify == "web"):
        redisdb.set("notify_token_web:" + str(uid_current), token)
    else:
        redisdb.set("notify_token:" + str(uid_current), token)
    return json({})


@app.route('/api/v1/test_notify', methods=['POST'])
async def test_notify(request):
 
    data = request.json
    uid_current = data.get("uid",None)
    firebase_token = redisdb.get("notify_token:" + str(uid_current))
    if firebase_token is not None:
        firebase_token = firebase_token.decode('utf8')


        await send_firebase_notify([firebase_token], data.get("content", ""), data)

        return json({})
    else:
        return json({"error_code": "KEY_NOT_SET", "error_message": ""}, status=520)

