# import asyncio
import aiohttp
import hashlib
import ujson

from application.extensions import apimanager
from application.server import app
from gatco.response import json, text, html, file
from application.database import redisdb,db 
from application.controllers.helpers.helper_common import *
from sqlalchemy import or_, and_, desc, asc
import uuid
import time
from application.database.model import default_uuid
from application.models.model_khaibaoyte import ToKhaiYTe, TrieuChung, DichTe
import xlrender
from datetime import date
import datetime
import ast

@app.route('/api/v1/export-tokhaiyte' , methods = ["POST", "GET"])
async def export_khaibaoyte(request):
    filename= "danhsachtokhaiyte"
    os.environ['TZ'] = 'UTC-7'
    time.tzset()
    date_object = date.today()
    str_time = datetime.datetime.fromtimestamp(time.time()).strftime("%d/%m/%Y %H:%M")
    headerobj = {
    "ngayxuat": str_time
    }
    deffile = "exceltpl/danhsachtokhaiyte.json"
    template = "exceltpl/danhsachtokhaiyte.xlsx"
    render = xlrender.xlrender(template,deffile)
    render.put_area("header", ujson.dumps(headerobj))
    render.put_area("table-header")
    count = 1
    # Get list ds khai bao 
    query = request.args.get('q', None)
    check_ds_tokhaiyte = []
    if query is not None:
        x = ast.literal_eval(query)

        dichte = ''
        trieuchung = ''
        text = ''
        thoigian_khaibao = ''

        for item in x['filters']['$and']:
            if 'dichte' in item:
                dichte = item['dichte']
            if 'trieuchung' in item:
                trieuchung = item['trieuchung']
            if 'text' in item:
                text = item['text']
            if 'thoigian_khaibao' in item:
                thoigian_khaibao = item['thoigian_khaibao']

        filter_query= db.session.query(ToKhaiYTe).order_by(desc(ToKhaiYTe.thoigian_khaibao))
        if(text!=''):
            filter_query = filter_query.filter(or_(
                ToKhaiYTe.tenkhongdau.ilike('%{}%'.format(text)),
                ToKhaiYTe.so_dien_thoai.ilike('%{}%'.format(text)),
                ToKhaiYTe.ma_benh_nhan.ilike('%{}%'.format(text))
            ))
        if thoigian_khaibao!='':
            gte = None
            lte = None
            if '$gte' in thoigian_khaibao:
                gte = thoigian_khaibao['$gte']
            if '$lte' in thoigian_khaibao:
                lte = thoigian_khaibao['$lte']
            if gte is not None and lte is not None:
                filter_query = filter_query.filter(and_(ToKhaiYTe.thoigian_khaibao>=gte, ToKhaiYTe.thoigian_khaibao<=lte))
            elif gte is not None and lte is None:
                filter_query = filter_query.filter(ToKhaiYTe.thoigian_khaibao>=gte)
            elif gte is None and lte is not None:
                filter_query = filter_query.filter(ToKhaiYTe.thoigian_khaibao<=lte)

        arr_filter_dichte = []
        if dichte!='':
            for id_dichte in dichte:
                obj = {"id":id_dichte}
                arr_filter_dichte.append(obj)
                filter_query = filter_query.filter(ToKhaiYTe.dichte.contains(arr_filter_dichte))

        arr_filter_trieuchung = []
        if trieuchung!='':
            for id_trieuchung in trieuchung:
                obj = {"id":id_trieuchung}
                arr_filter_trieuchung.append(obj)
                filter_query = filter_query.filter(ToKhaiYTe.trieuchung.contains(arr_filter_trieuchung))
        check_ds_tokhaiyte = filter_query.all()
    else:
        check_ds_tokhaiyte = db.session.query(ToKhaiYTe).all()
    # Ket thuc get list

    if check_ds_tokhaiyte is not None and isinstance(check_ds_tokhaiyte, list):
        for tokhai in check_ds_tokhaiyte:
            obj_tmp = dict()
            obj_tmp["stt"] = count
            count+=1
            obj_tmp["ma_benh_nhan"] = tokhai.ma_benh_nhan
            obj_tmp["lydodenvien"] = tokhai.lydodenvien
            obj_tmp["khoa_phong"] = tokhai.khoa_phong
            obj_tmp["ten"] = tokhai.ten
            if tokhai.ngaysinh is not None:
                ngaysinh = datetime.datetime.fromtimestamp(tokhai.ngaysinh).strftime('%d/%m/%Y')
                obj_tmp["ngaysinh"] = ngaysinh
            if tokhai.gioi_tinh is not None:
                if tokhai.gioi_tinh == 1:
                    obj_tmp["gioi_tinh"] = 'Nam'
                elif tokhai.gioi_tinh==2:
                    obj_tmp["gioi_tinh"] = 'Nữ'
                else:
                    obj_tmp["gioi_tinh"] = 'Khác'
                    
            if tokhai.loai_khaibao is not None:
                if tokhai.loai_khaibao == 1:
                    obj_tmp["loai_khaibao"] = 'Bệnh nhân'
                elif tokhai.loai_khaibao==2:
                    obj_tmp["loai_khaibao"] = 'Người nhà'
                elif tokhai.loai_khaibao==3:
                    obj_tmp["loai_khaibao"] = 'Khai hộ'
                elif tokhai.loai_khaibao==4:
                    obj_tmp["loai_khaibao"] = 'Nhân viên y tế'
                elif tokhai.loai_khaibao==5:
                    obj_tmp["loai_khaibao"] = 'Khác'

            obj_tmp["dia_chi"] = tokhai.dia_chi
            obj_tmp["so_dien_thoai"] = tokhai.so_dien_thoai
            if tokhai.thoigian_khaibao is not None:
                thoigian_khaibao = datetime.datetime.fromtimestamp(tokhai.thoigian_khaibao).strftime('%d/%m/%Y %H:%M')
                obj_tmp["thoigian_khaibao"] = thoigian_khaibao
            trieuchung = tokhai.trieuchung
            if trieuchung is not None and isinstance(trieuchung, list):
                list_trieuchung = []
                for tc in trieuchung:
                    list_trieuchung.append(tc['ten'])
                obj_tmp["trieuchung"] = ', '.join(list_trieuchung)

            dichte = tokhai.dichte
            if dichte is not None and isinstance(dichte, list):
                list_dichte = []
                for dt in dichte:
                    list_dichte.append(dt['ten'][:150])
                obj_tmp["dichte"] = ', '.join(list_dichte)

            obj_tmp["ghi_chu"] = tokhai.ghi_chu
            
            render.put_area("table-content", ujson.dumps(obj_tmp))
        render.save(filename)
        return await file(filename, mime_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                filename= filename + '.xlsx')
    else:
        return json({"error_message" : "Không tìm thấy danh sách.", "error_code" : "PARAM_NOT_EXISTS"}, status=520)   
