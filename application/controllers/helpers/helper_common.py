#from gatco.exceptions import ServerError
from application.server import app
from gatco.response import json
from application.database import redisdb, db
from application.client import HTTPClient 
from application.models.models import *

import aiosmtplib
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from email.mime.text import MIMEText
from email.header import Header

import random
import os
import re
import binascii
import asyncio
import aiohttp
import hashlib
import ujson
import uuid
import string
from datetime import datetime,timezone
from math import floor
import time
from gatco_restapi.helpers import to_dict
from application.models.model_donvi import User
from sqlalchemy import or_, and_, desc, asc
from sqlalchemy import update
from application.models.model_khaibaoyte import SoThuTu
from application.models.model_donvi import DonVi
def hash_value(value):
    return hashlib.md5(value.encode('utf-8')).hexdigest()

async def pre_post_insert_donvi(request=None, data=None, Model=None, **kw):
    uid = current_uid(request)
    if uid is None:
        return json({'error_code':'SESSION_EXPIRED', 'error_message':'Hết phiên làm việc, vui lòng đăng nhập lại'}, status=520)
    else:
        user = db.session.query(User).filter(and_(User.id == uid,User.deleted == False)).first()
        if user is not None and user.donvi_id is not None:
            data["created_by"] = user.id
            data["updated_by"] = user.id
        if 'ten' in data and 'tenkhongdau' in data:
            data['tenkhongdau'] = convert_text_khongdau(data['ten'])
        if "ten_dichvu" in data and "ten_dichvu_khongdau" in data:
            data['ten_dichvu_khongdau'] = convert_text_khongdau(data['ten_dichvu'])
        if "ten_vattu" in data and "tenkhongdau" in data:
            data['tenkhongdau'] = convert_text_khongdau(data['ten_vattu'])
        if "donvi_id" in data:
            if user.has_role("admin_donvi") or user.has_role("canbo"):
                if "donvi" in data:
                    data["donvi"] = user.donvi
                if "donvi_id" in data:
                    data["donvi_id"] = user.donvi_id
            else:
                return json({'error_code':'PERMISSION_DENY', 'error_message':'Cần đăng nhập tài khoản đơn vị để thực hiện chức năng này'}, status=520)

async def pre_put_insert_tenkhongdau(request=None, data=None, Model=None, **kw):
    uid = current_uid(request)
    if uid is None:
        return json({'error_code':'SESSION_EXPIRED', 'error_message':'Hết phiên làm việc, vui lòng đăng nhập lại'}, status=520)
    else:
        user = db.session.query(User).filter(and_(User.id == uid,User.deleted == False)).first()
        if 'ten' in data and 'tenkhongdau' in data:
            data['tenkhongdau'] = convert_text_khongdau(data['ten'])
        if "ten_dichvu" in data and "ten_dichvu_khongdau" in data:
            data['ten_dichvu_khongdau'] = convert_text_khongdau(data['ten_dichvu'])
        if "ten_vattu" in data and "tenkhongdau" in data:
            data['tenkhongdau'] = convert_text_khongdau(data['ten_vattu'])

async def pre_getmany_donvi(search_params=None,request=None, **kw):
    uid = current_uid(request)
    if uid is None:
        return json({'error_code':'SESSION_EXPIRED', 'error_message':'Hết phiên làm việc, vui lòng đăng nhập lại'}, status=520)
    else:
        user = db.session.query(User).filter(and_(User.id == uid, User.deleted == False)).first()
        if user is not None and user.donvi_id is not None:
            
            if user.has_role("admin_donvi") or user.has_role("canbo"):
                if "filters" in search_params and search_params["filters"] != "":
                    search_params["filters"] = {"$and":[search_params["filters"], {"donvi_id":{"$eq": user.donvi_id}},{"deleted":{"$eq": False}}]}
                else:
                    search_params["filters"] = {"$and":[{"donvi_id":{"$eq": user.donvi_id}},{"deleted":{"$eq": False}}]}

async def pre_getmany_deleted(search_params=None,request=None, **kw):
    if "filters" in search_params and search_params["filters"] != "":
        search_params["filters"] = {"$and":[search_params["filters"], {"deleted":{"$eq": False}}]}
    else:
        search_params["filters"] = {"deleted":{"$eq": False}}




async def pre_delete(request=None, instance_id=None, Model=None, **kw):
    uid = current_uid(request)
    if uid is None:
        return json({'error_code':'SESSION_EXPIRED', 'error_message':'Hết phiên làm việc, vui lòng đăng nhập lại'}, status=520)
    else:
        user = db.session.query(User).filter(and_(User.id == uid,User.deleted == False)).first()
        if user is not None:
            record = db.session.query(Model).filter(Model.id == instance_id).first()
            if record is not None:
                record.deleted = True
                db.session.commit()
                return json(to_dict(record), status=200)
            else:
                return json({'error_code':'NOT_FOUND', 'error_message':'Không tìm thấy dữ liệu tương ứng'}, status=520)
        else:
            return json({'error_code':'SESSION_EXPIRED', 'error_message':'Hết phiên làm việc, vui lòng đăng nhập lại'}, status=520)


def convert_timestamp_to_utctimestamp(value):
#     dtobj_utc = datetime.utcfromtimestamp(value)
    dtobj_utc = None
    try:
        
        dtobj_utc = datetime.utcfromtimestamp(int(value))
    except:
        try:
            dtobj_utc = datetime.strptime(value, '%Y-%m-%d')
        except:
            return None
    date_utc = datetime(dtobj_utc.year, dtobj_utc.month, dtobj_utc.day)
    return int(date_utc.replace(tzinfo=timezone.utc).timestamp())

def convert_timestamp_to_string(value, format):
    dtobj_utc = None
    try:
        dtobj_utc = datetime.fromtimestamp(int(value))
    except:
        try:
            dtobj_utc = datetime.strptime(value)
        except:
            return None
    return dtobj_utc.strftime(format)

def convert_datetime_to_timestamp(value, formatdate):
    result = None
    if value is None:
        result = None
    else:
        try:
            validate_ngaysinh = int(value)
            result = validate_ngaysinh
        except:
            for format in ['%d-%m-%Y','%Y-%m-%d','%Y-%m-%dT%H:%M:%S','%d/%m/%Y']:
                try:
                    value = datetime.strptime(value, format)
                    date_utc = datetime(value.year, value.month, value.day)
                    result = int(date_utc.replace(tzinfo=timezone.utc).timestamp())
                    break
                except:
                    continue
    return result

def check_content_json(request):
    ret = False
    try:
        content_type = request.headers.get('Content-Type', "")
        ret = content_type.startswith('application/json')
    except:
        pass
    return ret


def valid_phone_number(phone_number):
    if phone_number is None:
        return False
    if phone_number.isdigit() and len(phone_number)>=8 and len(phone_number)<=12 and phone_number.startswith("0"):
        return True
    return False

def convert_text_khongdau(text):
    if text is None:
        return None
    kituA=["á","à","ạ","ã","ả","â","ấ","ầ","ậ","ẫ","ă","ặ","ằ","ắ","ẳ"]
    kituE=["é","è","ẹ","ẻ","ẽ","ê","ế","ề","ệ","ễ","ể"]
    kituI=["í","ì","ị","ỉ","ĩ"]
    kituO=["ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ"]
    kituU=["ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ"]
    kituY=["ỳ","ý","ỵ","ỷ","ỹ"]

    ten = text.lower()
    for i in ten:
        if i in kituA:
            ten = ten.replace(i,"a")
        elif i in kituE:
            ten = ten.replace(i,"e")
        elif i in kituI:
            ten = ten.replace(i,"i")
        elif i in kituO:
            ten = ten.replace(i,"o")
        elif i in kituU:
            ten = ten.replace(i,"u")
        elif i in kituY:
            ten = ten.replace(i,"y")
        elif i=="đ":
            ten = ten.replace(i,"d")
    return ten


def password_generator(size = 8, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generator_salt():
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(24))

def current_uid(request):
    user_token = request.headers.get("X-USER-TOKEN", None)
    if user_token is None:
        return None
    uid = redisdb.get("sessions:" + user_token)
    if uid is not None:
        p = redisdb.pipeline()
        p.set("sessions:" + user_token, uid)
        p.expire("sessions:" + user_token, app.config.get('SESSION_EXPIRED', 86400))
        p.execute()
        return uid.decode('utf8')
    
    return None


def generate_user_token(uid):
    token =  binascii.hexlify(uuid.uuid4().bytes).decode()
    p = redisdb.pipeline()
    p.set("sessions:" + token, uid)
    p.expire("sessions:" + token, app.config.get('SESSION_EXPIRE_TIME', 86400))
    p.execute()
    return token

def current_user(request):
    user_token = request.headers.get("X-USER-TOKEN", None)
    if user_token is None:
        return None
    uid = redisdb.get("sessions:" + user_token)
    if uid is not None:
        p = redisdb.pipeline()
        p.set("sessions:" + user_token, uid)
        p.expire("sessions:" + user_token, app.config.get('SESSION_EXPIRED', 86400))
        p.execute()
        currentUser = db.session.query(User).filter(and_(User.id == uid.decode('utf8'),User.deleted == False)).first()
        if (currentUser is not None):
            return currentUser
    return None  

async def get_current_user(request, userId):
    if userId is not None:
        user = db.session.query(User).filter(and_(or_(User.id ==userId,User.phone == userId, User.email == userId)),User.active==1).first()
        if user is None:
            return None
        user_token = request.headers.get("X-USER-TOKEN", None)
        return response_current_user(user, user_token)
    return None

def response_current_user(user, token=None):
    id = user.id
    token = generate_user_token(id, token)
    response = to_dict(user)
    donvi =db.session.query(DonVi).filter(DonVi.id == user.donvi_id).first()
    if donvi is not None:
        response['donvi'] = to_dict(donvi)
    else:
        response['donvi']= None
    response.pop('password', None)
    response.pop('salt', None)
    response['token'] = token
    roles = []
    if user.roles is not None:
        for role in user.roles:
            roles.append(role.name)
    response["roles"] = roles
    return response


def generate_user_token(uid_user, token):
    if token is None or token == "":
        token =  binascii.hexlify(uuid.uuid4().bytes).decode()
    p1 = redisdb.pipeline()
    p1.set("sessions:" + token, uid_user)
    p1.expire("sessions:" + token, app.config.get('SESSION_EXPIRE_TIME', 86400))
    p1.execute()
    return token


async def hasRole(request, role):
    uid = current_uid(request)
    if uid is None:
        return False
    else:
        currentUser = await get_current_user(request, uid)
        if currentUser is not None and role in currentUser["roles"]:
            return True
        else:
            return False

async def validate_admin(request, **kw):
    uid = current_uid(request)
    if uid is None:
        return {'error_code':'SESSION_EXPIRED', 'error_message':'Session Expired!'}
    else:
        user = db.session.query(User).filter(and_(User.id == uid,User.deleted == False)).first()
        roles = user.roles
        list_role = []
        for role in roles:
            list_role.append(role.name)
        if 'admin' not in list_role:
            return json({"error_code": "ERROR_PERMISSION", "error_msg": "Permission deny!"},status=403)


def validate_user(request, **kw):
    uid = current_uid(request)
    if uid is None:
        return json({'error_code':'SESSION_EXPIRED', 'error_message':'Session Expired!'}, status=403)

def deny_request(request, **kw):
    return json({'error_code':'ERROR_PERMISSION', 'error_message':'Permission deny!'}, status=403)



async def check_user(uid):    
    url = app.config.get("USER_STORE_URL", "http://127.0.0.1:9018/api/v1/") + str('checkuser')
    data_key = {'data':uid}
    headers = {'content-type': 'application/json'}
    response = await HTTPClient.post(url, data=data_key, headers=headers)
    if(response is not None and "error_code" not in response):
        return response
    return None


async def check_permission_public(uid):
    result = {}
    result["read"] = False
    result["write"] = False
    current_time = int(time.time())
    so_public = db.session.query(PermissionSoPublic).filter(PermissionSoPublic.id == uid).first()
    if so_public is None or so_public.type == -1:
        return result
    else:
        if so_public.type == 0 or current_time < so_public.expired_time:
            if so_public.roles == "write":
                result["write"] = True
                result["read"] = True
            elif  so_public.roles == "read":
                result["read"] = True
            
    return result

def validate_email(email):
    return re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)

def default_uuid():
    return str(uuid.uuid4())

async def send_mail(subject, recipient, body):
    # subject = "Vụ sức khỏe Bà mẹ và Trẻ em cấp QRcode"
    # data = "Vụ sức khỏe Bà mẹ và Trẻ em cấp QRcode cho đơn vị " + donvi_name + "   \nThời gian cấp: " + thoigiancap +"  \nĐường dẫn tải file: " + url
    # await send_mail(subject, email, data)

    host = app.config.get('MAIL_SERVER_HOST')
    port = app.config.get('MAIL_SERVER_PORT')
    user = app.config.get('MAIL_SERVER_USER')
    password = app.config.get('MAIL_SERVER_PASSWORD')

    loop = asyncio.get_event_loop()

    #server = aiosmtplib.SMTP(host, port, loop=loop, use_tls=False, use_ssl=True)
    server = aiosmtplib.SMTP(hostname=host, port=port, loop=loop, use_tls=False)
    await server.connect()

    await server.starttls()
    await server.login(user, password)

    async def send_a_message():
        # message = MIMEText(body)
        # message = MIMEText(body, _charset='utf-8')
        message = MIMEText(body, "html")
        message['From'] = app.config.get('MAIL_SERVER_USER')
        print("..............", message['From'])
        #message['To'] = ','.join(new_obj.get('email_to'))
        message['To'] = recipient
        # message['Subject'] = Header(subject, "utf-8")
        message['Subject'] = Header(subject.encode('utf-8'), 'UTF-8').encode()
        await server.send_message(message)

    await send_a_message()

def generate_sothutu(type_stt):
    check = db.session.query(SoThuTu).filter(SoThuTu.type == type_stt).first()
    if check is None:
        stt = SoThuTu()
        stt.id = default_uuid()
        stt.type = type_stt
        stt.sothutu = 1
        db.session.add(stt)
        db.session.commit()
        return 1
    stt = update(SoThuTu).returning(SoThuTu.sothutu).where(and_(SoThuTu.type == type_stt)).values(sothutu= (SoThuTu.sothutu +1))
    result = db.session.execute(stt)
    db.session.commit()
    for row in result:
        return row[0]