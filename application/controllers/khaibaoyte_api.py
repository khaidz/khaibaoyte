
from application.extensions import apimanager
from gatco_restapi.helpers import to_dict
from application.server import app
from sqlalchemy import or_
from gatco.response import json
from datetime import datetime
import ujson
import asyncio
import aiohttp
import time
from application.models.model_khaibaoyte import ToKhaiYTe, SoThuTu, TrieuChung, DichTe
from application.controllers.helpers.helper_common import validate_user, convert_text_khongdau, validate_admin, pre_delete,  pre_put_insert_tenkhongdau, generate_sothutu,current_uid
from application.database import db
from math import floor, ceil
from sqlalchemy import or_, and_, desc, asc
import ast

async def preprocess_tenkhongdau(request=None, data=None, Model=None, **kw):
    tenkhongdau =''
    if 'ten' in data and data['ten'] is not None:
        tenkhongdau = convert_text_khongdau(data['ten'])
    data['tenkhongdau'] = tenkhongdau


async def preprocess_khaibaoyte(request=None, data=None, Model=None, **kw):
    tenkhongdau =''
    if 'ten' in data and data['ten'] is not None:
        data['ten'] = (data['ten']).upper()
        tenkhongdau = convert_text_khongdau(data['ten'])
    data['tenkhongdau'] = tenkhongdau

    stt = generate_sothutu('KhaiBao_YTe')
    data['stt'] = stt
    data['thoigian_khaibao'] = floor(time.time())
    
@app.route('/api/v1/khaibaoyte/phone', methods=['POST'])
async def get_info_tokhai_yte_phone(request):
    dien_thoai = request.json.get('dien_thoai', None)
    loai_kb = request.json.get('loai_kb', None)
    if loai_kb=='1':
        tokhai_yte = db.session.query(ToKhaiYTe).filter(ToKhaiYTe.so_dien_thoai == dien_thoai, ToKhaiYTe.loai_khaibao==1).order_by(desc(ToKhaiYTe.thoigian_khaibao)).first()
    else:
        tokhai_yte = db.session.query(ToKhaiYTe).filter(ToKhaiYTe.so_dien_thoai == dien_thoai, loai_kb!=1).order_by(desc(ToKhaiYTe.thoigian_khaibao)).first()
    if tokhai_yte is not None:
        data = to_dict(tokhai_yte)
    else:
        data = None
    return json({"error_code":"OK","error_message":"successful", "data":data},status=200)

@app.route('/api/v1/khaibaoyte/benhnhan', methods=['POST'])
async def get_info_tokhai_yte_benhnhan(request):
    ma_benh_nhan = request.json.get('ma_benh_nhan', None)
    tokhai_yte = db.session.query(ToKhaiYTe).filter(ToKhaiYTe.ma_benh_nhan == ma_benh_nhan).order_by(desc(ToKhaiYTe.thoigian_khaibao)).first()
    if tokhai_yte is not None:
        data = to_dict(tokhai_yte)
    else:
        data = None
    return json({"error_code":"OK","error_message":"successful", "data":data},status=200)

@app.route('/api/v1/khaibaoyte/uid', methods=['GET'])
async def get_info_tokhai_yte_uid(request):
    query = request.args.get('q', None)
    if query is None:
        return json({"page": 1,"total_pages":0,"num_results":0,"objects":[]},status=200)
    page = request.args.get('page')
    results_per_page = request.args.get('results_per_page')
    if page is None:
        page = 1
    if results_per_page is None:
        results_per_page = 15

    x = ast.literal_eval(query)
    uid=''
    for item in x['filters']['$and']:
        if 'uid' in item:
            uid = item['uid']
    filter_query= db.session.query(ToKhaiYTe).filter(ToKhaiYTe.uid==uid)

    objects = []
    total_pages = 1
    number_result = 0
    filter_query = filter_query.order_by(desc(ToKhaiYTe.thoigian_khaibao))
    result_all = filter_query.count()
    result_limit = filter_query.offset(int(int(page)-1)*int(results_per_page)).limit(int(results_per_page)).all()
    danhsach_tokhai = []
    for obj in result_limit:
        obj = to_dict(obj)
        tokhai = {
            'id': obj['id'],
            'ten': obj['ten'],
            'ngaysinh': obj['ngaysinh'],
            'gioi_tinh': obj['gioi_tinh'],
            'thoigian_khaibao': obj['thoigian_khaibao']
        }
        objects.append(tokhai)

    number_result = result_all
    total_pages = ceil(number_result/int(results_per_page))
    
    return json({"page": int(int(page)),"total_pages":total_pages,"num_results":number_result,"objects":objects},status=200)
   

# @app.route('/api/v1/khaibaoyte/uid', methods=['POST'])
# async def get_info_tokhai_yte_phone(request):
#     uid = request.json.get('uid', None)
#     tokhai_yte = db.session.query(ToKhaiYTe).filter(ToKhaiYTe.uid == uid).order_by(desc(ToKhaiYTe.thoigian_khaibao)).all()
#     if tokhai_yte is not None:
#         data = to_dict(tokhai_yte)
#     else:
#         data = None
#     return json({"error_code":"OK","error_message":"successful", "data":data},status=200)    

@app.route('/api/v1/tokhai_yte_search', methods=['GET'])
async def get_list_tokhai_yte_search(request):
    uid_current = current_uid(request)
    if uid_current is None:
        return json({"error_code": "SESSION_EXPIRED", "error_message": "Hết phiên làm việc, vui lòng đăng nhập lại"}, status=520)

    query = request.args.get('q', None)
    
    if query is None:
        return json({"page": 1,"total_pages":0,"num_results":0,"objects":[]},status=200)
    page = request.args.get('page')
    results_per_page = request.args.get('results_per_page')

    x = ast.literal_eval(query)

    dichte = ''
    trieuchung = ''
    text = ''
    thoigian_khaibao = ''

    for item in x['filters']['$and']:
        if 'dichte' in item:
            dichte = item['dichte']
        if 'trieuchung' in item:
            trieuchung = item['trieuchung']
        if 'text' in item:
            text = item['text']
        if 'thoigian_khaibao' in item:
            thoigian_khaibao = item['thoigian_khaibao']

    filter_query= db.session.query(ToKhaiYTe).order_by(desc(ToKhaiYTe.thoigian_khaibao))
    if(text!=''):
        filter_query = filter_query.filter(or_(
            ToKhaiYTe.tenkhongdau.ilike('%{}%'.format(text)),
            ToKhaiYTe.so_dien_thoai.ilike('%{}%'.format(text)),
            ToKhaiYTe.ma_benh_nhan.ilike('%{}%'.format(text))
        ))
    if thoigian_khaibao!='':
        gte = None
        lte = None
        if '$gte' in thoigian_khaibao:
            gte = thoigian_khaibao['$gte']
        if '$lte' in thoigian_khaibao:
            lte = thoigian_khaibao['$lte']
        if gte is not None and lte is not None:
            filter_query = filter_query.filter(and_(ToKhaiYTe.thoigian_khaibao>=gte, ToKhaiYTe.thoigian_khaibao<=lte))
        elif gte is not None and lte is None:
            filter_query = filter_query.filter(ToKhaiYTe.thoigian_khaibao>=gte)
        elif gte is None and lte is not None:
            filter_query = filter_query.filter(ToKhaiYTe.thoigian_khaibao<=lte)

    arr_filter_dichte = []
    if dichte!='':
        for id_dichte in dichte:
            obj = {"id":id_dichte}
            arr_filter_dichte.append(obj)
            filter_query = filter_query.filter(ToKhaiYTe.dichte.contains(arr_filter_dichte))

    arr_filter_trieuchung = []
    if trieuchung!='':
        for id_trieuchung in trieuchung:
            obj = {"id":id_trieuchung}
            arr_filter_trieuchung.append(obj)
            filter_query = filter_query.filter(ToKhaiYTe.trieuchung.contains(arr_filter_trieuchung))

    objects = []
    total_pages = 1
    number_result = 0

    result_limit = filter_query.offset(int(int(page)-1)*int(results_per_page)).limit(int(results_per_page)).all()
    
    for obj in result_limit:
        objects.append(to_dict(obj))

    number_result = filter_query.count()
    total_pages = ceil(number_result/int(results_per_page))
    
    return json({"page": int(int(page)),"total_pages":total_pages,"num_results":number_result,"objects":objects},status=200)

apimanager.create_api(ToKhaiYTe,
    methods=['GET', 'POST', 'DELETE', 'PUT'],
    url_prefix='/api/v1',
    preprocess=dict(GET_SINGLE=[], GET_MANY=[validate_user], POST=[preprocess_khaibaoyte], PUT_SINGLE=[preprocess_khaibaoyte], DELETE_SINGLE=[validate_user]),
    postprocess=dict(POST=[],PUT_SINGLE=[]),
    collection_name='tokhai_yte')

apimanager.create_api(TrieuChung,
    methods=['GET', 'POST', 'DELETE', 'PUT'],
    url_prefix='/api/v1',
    preprocess=dict(GET_SINGLE=[], GET_MANY=[], POST=[preprocess_tenkhongdau, validate_user], PUT_SINGLE=[preprocess_tenkhongdau,validate_user], DELETE_SINGLE=[validate_user]),
    postprocess=dict(POST=[],PUT_SINGLE=[]),
    collection_name='trieuchung')

apimanager.create_api(DichTe,
    methods=['GET', 'POST', 'DELETE', 'PUT'],
    url_prefix='/api/v1',
    preprocess=dict(GET_SINGLE=[], GET_MANY=[], POST=[preprocess_tenkhongdau,validate_user], PUT_SINGLE=[preprocess_tenkhongdau,validate_user], DELETE_SINGLE=[validate_user]),
    postprocess=dict(POST=[],PUT_SINGLE=[]),
    collection_name='dichte')

